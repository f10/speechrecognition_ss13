<?xml version="1.0" encoding="UTF-8"?>

<section xml:id="gradle" xmlns="http://docbook.org/ns/docbook"
	xmlns:xlink="http://www.w3.org/1999/xlink">
	<title>Gradle</title>
	<para>
	Um unabhängig von einer bestimmten Entwicklungsumgebung (bisher war Eclipse
	Voraussetzung für die Entwicklung des Projektes) oder einem bestimmten
	Servlet-Container, wie z.B. Tomcat, arbeiten zu können, wird seit dem
	SS2013 Gradle für das Projekt SpeechRecognition verwendet.
	Gradle ist ein auf der Java-Plattform basierendes Build-Management- und
	Automatisierungs-Tool. Es vereinigt
	dabei Konzepte von Apache Ant und Apache Maven. Für die Projektbeschreibung
	wird aber kein XML sondern eine
	auf der Sprache Groovy basierende Domain Specific Language (DSL)
	verwendet. Gradle-Build-Skripte sind somit
	direkt ausführbarer Programmcode.

	Gradle kombiniert die Flexibilität von Ant-Tasks - sowohl Ant-Tasks als auch
	ganze Ant-Projekte sind "first class citizens" in Gradle -
	mit dem (transitiven) Dependency-Management und dem "Convention over
	Configuration"-Paradigma von Maven.

	Dieser Abschnitt soll lediglich eine kurze Einführung zur Projektspezifischen
	Verwendung von Gradle darstellen.

	Eine übersicht zu Gradle findet man unter
	<ulink url="http://www.gradle.org/overview" />.

	Zur weiteren Verwendung von Gradle sei auf den
	<ulink url="http://www.gradle.org/docs/current/userguide/userguide.html"> Gradle User Guide</ulink>
	verwiesen.
	</para>
	<section xml:id="gradle-installation">
		<title>Installation</title>
		<para>
			Gradle kann unter folgender Quelle bezogen werden:
			<ulink url="http://www.gradle.org/downloads" />
			.
			Nachdem entpacken sollte die Umgebungsvariable
			<envar>$GRADLE_HOME</envar>
			auf das Hauptverzeichnis gesetzt werden, sowie
			das bin-Verzeichnis zum Pfad/Path, also zur Umgebungsvariable
			<envar>$PATH</envar>
			hinzugefügt werden, damit der gradle-Befehl
			auf der Kommandozeile verfügbar ist. Unter Unix (z.b. Linux und Mac OS)
			hierzu in der
			<emphasis>/etc/bash.bashrc</emphasis>
			und der
			<emphasis>/etc/profile</emphasis>
			(Systemweit) bzw.
			<emphasis>~/.basrc</emphasis>
			und
			<emphasis>~/.profile</emphasis>
			(benutzerspezifisch) folgenden Eintrag einfügen:
	
			<programlisting>
export GRADLE_HOME=/<replaceable>PATH_TO_GRADLE</replaceable>
export PATH=$GRADLE_HOME/bin:$PATH
			</programlisting>
			<replaceable>PATH_TO_GRADLE</replaceable> muss durch den absoluten Pfad zum Gradle 
			Hauptverzeichnis ersetzt werden.
			</para>
	</section>
	

	<section xml:id="gradle-build">
		<title>Build Script build.gradle</title>
		<para>
		Gradle basiert auf zwei Hauptkonzepten: Projekte und
		Tasks (vgl. <xref linkend="gradle-tasks">Tasks</xref>)
		.
		Das Gradle Build Script
		<emphasis>build.gradle</emphasis>
		befindet sich im Projekt-Hauptverzeichnis.
		Siehe auch
		<ulink
			url="http://www.gradle.org/docs/current/userguide/tutorial_using_tasks.html">Chapter 6. Build Script Basics</ulink>
		im <ulink url="http://www.gradle.org/docs/current/userguide/userguide.html"> Gradle User Guide</ulink>.
	</para>
	</section>

	<section xml:id="gradle-project-structure">
		<title>Projektstruktur</title>
	<para>
	    
	Gradle übernimmt im wesentlichen die von Maven bekannten Konventionen für die
	Projekt-Struktur:

	<itemizedlist>
		<listitem>src/main/java - Java Source-Files</listitem>
		<listitem>src/main/resources - Java Resources</listitem>
		<listitem>src/test/java - Java Unit Test Sources</listitem>
		<listitem>src/test/resources - Java Resourcen für Tests </listitem>
		<listitem>...</listitem>
	</itemizedlist>
	<figure>
		<title>Projektstruktur in Eclipse</title>
		<mediaobject>
			<imageobject>
				<imagedata scale="33" align="center" fileref="images/projectstructure.png">
				</imagedata>
			</imageobject>
		</mediaobject>
	</figure>
	Dies hat - im Gegensatz zu selbst definierten Projektstrukturen bei Ant - den
	Vorteil, dass sich Entwickler die mit der Konvention vertraut sind sofort mit der Projektstruktur zurechtfinden.
	
	Hierfür wurde ein entsprechendes Refactoring vorgenommen um die alte Verzeichnisstruktur umzuwandeln.
	Die in Eclipse verfügbaren Refactoring-Funktionen erwiesen sich hierbei als sehr nützlich.
	</para>
	</section>
	
	<section xml:id="gradle-tasks">
		<title>Gradle Tasks</title>
		<para>
		Für eine Übersicht aller Tasks eines Gradle-Builds, einfach auf der
		Kommandozeile folgenden Gradle-Befehl eingeben:
		<programlisting>
~/git/SpeechRecognition_SS13$ gradle tasks
		</programlisting>
		Output (Auschnitt):
		<screen>
	 		   <![CDATA[ 
------------------------------------
All tasks runnable from root project
------------------------------------
Build tasks
-----------
assemble - Assembles the outputs of this 
           project.
build - Assembles and tests this project.
buildDependents - Assembles and tests 
                  this project and all 
                  projects that depend on it.
buildNeeded - Assembles and tests this
              project and all projects 
              it depends on.
clean - Deletes the build directory.
jar - Assembles a jar archive containing
      the main classes.
war - Generates a war archive with all
      the compiled classes, the web-app
      content and the libraries.
...

DocBook tasks
-------------
buildDocs - Builds all documention

To see all tasks and more detail, 
run with --all.

BUILD SUCCESSFUL

Total time: 13.673 secs

	 		]]></screen>

	</para>
	</section>

	<section xml:id="gradle-plugins">
		<title>Plugins</title>
	<para>
	    Für sich genommen ist Gradle ein generisches Build-System, dass man
		z.B. auch für C++ oder Latex verwenden kann.
		Für spezielle tasks, z.B. das bauen eine Java-Archivs oder Web-Archivs
		existieren spezielle Plugins, wie z.B. "java" oder "war", oder
		auch 'jdocbook' zum übersetzen dieser Dokumentation im Docbook-Format.
		Folgende Plugins werden in
		<emphasis>build.gradle</emphasis>
		eingebunden und einige davon in den folgenden Abschnitten erläutert.
		<programlisting>
apply plugin: 'java'
apply plugin: 'war'
apply plugin: 'jetty'
apply plugin: 'eclipse-wtp'
apply plugin: 'jdocbook'
		</programlisting>
</para>
	</section>
	<section xml:id="eclipse-gradle">
		<title>Eclipse-Plugin für Gradle</title>
		<para>Um ein Gradle-Projekt in Eclipse zu verwenden kann das
		"eclipse"-Plugin eingebunden werden.
		Durch Ausführen von
		<programlisting>
gradle eclipse
		</programlisting>
		werden die Eclipse-Settings-Files erzeugt und das Projekt lässt sich
		anschliessend in Eclipse importieren.
		<note>
			<para>
				Es empiehlt sich die Eclipse-Settings-Files (.classpath, .settings-Verzeichnis, etc.) 
				nicht ins Versionskontrollsystem (git, svn, etc.) zu übernehmen, da diese einerseits
				plattformabhängig sind und andererseits von den Einstellungen des
				Users abhängen und es dadurch in einem verteilten Entwickler-Team zu
				ständigen Konflikten bei Commits kommt.
			</para>
		</note>
	</para>
	</section>

	<section xml:id="eclipse-wtp">
		<title>Eclipse WTP / Tomcat</title>
		<para>
		Mit dem Eclipse Web Tools Project lassen sich Java Web und
		Enterprise-Archive (WARs und EARs) direkt aus der IDE
		heraus in einem konfigurierten ServletContainer wie z.B. Tomcat/Jetty/etc.
		oder Application-Server wie z.B. JBoss/WebSphere/etc.
		deployen. Um entsprechende Settings-Files für Eclipse zu erzeugen reicht es
		oben erwähnten Task
		<code>gradle eclipse</code>
		auszuführen. Dabei wird der abhängige Task
		<code>eclipseWTP</code>
		ausgeführt.
	</para>
	</section>

	<section xml:id="gradle-eclipse">
		<title>Gradle-Plugin für Eclipse</title>
		<para>
			Mit Hilfe des Gradle-Plugins für Eclipse lässt sich gradle direkt aus
			Eclipse heraus aufrufen und man ist nicht auf
			das Terminal angewiesen. Das
			Gradle Plugin für Eclipse lässt sich über den
			Eclipse-Marketplace installieren.
		</para>
		<figure>
			<title>Gradle Plugin in Eclipse</title>
			<mediaobject>
				<imageobject>
					<imagedata scale="33" align="center" fileref="images/gradle_structure.png">
					</imagedata>
				</imageobject>
			</mediaobject>
		</figure>
	</section>
	
	<section xml:id="netbeans">
		<title>Netbeans und andere IDEs</title>
	<para>
		Sowohl Netbeans (<link url="http://plugins.netbeans.org/plugin/44510/gradle-support"> Plugin</link>)
		als auch IDEA bieten eine Gradle-Integration, die von den Authoren aber nicht 
		evaluiert wurden.
		Weitere Information siehe unter <citation>NetBeansAndOthers</citation>.
	</para>
	</section>


	<section xml:id="war">
		<title>Web Archive (war)</title>
		<para>
		Das "war"-Plugin für Gradle bietet Tasks zum erzeugen eines Java
		Web-Archivs (WAR), dass in einem Servlet-Container Deployed werden
		kann. Das war lässt sich mit folgendem Gradle-Kommando erzeugen:
		<programlisting>
gradle war		
		</programlisting>
		</para>
	</section>

	<section xml:id="tests">
		<title>Tests/Test Driven Devolopment (TDD)</title>
		<para>
			Ähnlich wie Maven ist das ausführen von Unit-/Integration-Tests während des Builds
			integraler bestandteil des Java-Plugins für Gradle.
			Unit-Tests befinden sich der Konvention nach in <code>src/test/java</code>,
			Test-Resourcen in <code>src/test/resources</code> (vgl. <xref linkend="unit-test-folders"/>).
			Schlagen die Tests fehl, so schlägt der gesamte Build fehl. 
			Es unterstützt damit das Test-Driven-Development, bei dem zu jedem Modul zuerst (oder Parallel)
			 der (Unit-) Test geschrieben wird, was den Vertrauen in den Code bestärken soll.
			Unit-Tests zeigen darüber hinaus exemplarisch wie der Code funktioniert und können 
			(evtl. weitreichende) Folgen von Modifikationen im Code aufdecken, die sonst 
			auf den ersten Blick nicht ersichtlich wären.
		</para>
			
		<para>
			Die Tests lassen sich mit folgendem Gradle-Kommando ausführen:
		<programlisting>
gradle test		
		</programlisting>
		</para>
	
		<para>
			<figure id="unit-test-folders">
				<title>Unit-Test Verzeichnis</title>
				<mediaobject>
					<imageobject>
						<imagedata scale="33" align="center" fileref="images/test_folder_structure.png">
						</imagedata>
					</imageobject>
				</mediaobject>
			</figure>
		</para>
	</section>

	<section xml:id="jetty">
		<title>Jetty</title>
		<para>
		Das Jetty-Plugin für Gradle bietet Tasks um das Projekt direkt auf
		einem eingebetteten Jetty-Servlet-Container zu deployen und zu
		starten.
		Dazu muss der folgender Gradle-Task ausgeführt werden:
		
		<programlisting>
gradle -Djava.library.path=./lib jettyRun
		</programlisting>

		<note>
			<para>
    			Damit die Spotify-Andbindung funktioniert muss sich, wie in <xref linkend="libspotify"/>
    			beschrieben, die Library libspotify im Linker-Pfad befinden.
			</para>	
		</note> 
		
		Dies hat den Vorteil (vgl. <xref linkend="quickstart"/>) dass man ausser Gradle keine
		zusätzliche Software installieren muss um das Projekt zu Entwickeln.
		Es steht einem frei ob man lieber VI(M), Emacs, Eclipse, NetBeans, IDEA, etc. als Editor nutzen möchte.
	</para>
	</section>

	<section xml:id="docbook">
		<title>DocBook/JDocBook</title>
		<para>
			DocBook wird in zahlreichen Open Source Projekten, wie z.B. Linux, PHP, KDE
			uvm. (siehe auch
			<ulink url="http://wiki.docbook.org/WhoUsesDocBook" />
			) verwendet und ist wesentlich leichtgewichtiger als z.B. LaTex. Es eignet sich primär 
			für technische Dokumentationen.
			Auch diese Dokumentation wurde mit Docbook erstellt.
			Für die Integration in den Build wird das JDocbook-Plugin verwendet.
			</para>
			
			<para>
			Um die Dokumentation zu übersetzen muss lediglich folgendes Gradle-Kommando ausgeführt werden.

			<programlisting>
gradle buildDocs
			</programlisting>
			
			Installation zusätzlicher Software ist nicht nötig, da Gradle alle benötigten Software-Pakete 
			(hier DocBook sowie abhängige Libs und Tools) über das Dependency Management herunterlädt.
		</para>
		<para>
			<figure>
				<title>Docbook HTML Browser Ausgabe</title>
				<mediaobject>
					<imageobject>
						<imagedata scale="15" align="center" fileref="images/docbook_html.png">
						</imagedata>
					</imageobject>
				</mediaobject>
			</figure>
		</para>
	</section>
	
	<section xml:id="javadoc">
		<title>Javadoc</title>
		<para>
			Zum generieren der Javadoc API genügt es folgendes Gradle Kommando im Terminal einzugeben:
		</para>
			
		<para>

			<programlisting>
gradle javadoc
			</programlisting>
			
		</para>

	</section>
	
</section>