/* 
 * Copyright 2013 Berlin Institute of Technology (TU Berlin), 
 * Faculty IV (CS), Department for Open Communication Systems (OKS).
 * 
 * This file is part of SpeechRecognition.
 * SpeechRecognition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SpeechRecognition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SpeechRecognition.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.tub.oks.infotainment.main.states;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.tub.oks.infotainment.main.UserInterface;
import de.tub.oks.infotainment.main.Messages;

/**
 * Handles UI for Twitter state
 */
public class TwitterUi implements UserInterface {

	/**
	 * Logger for this class
	 */
	private static final Logger logger = LoggerFactory.getLogger(TwitterUi.class);

	private static final String twitterImage = "<img src='images/menu/twitter.png' width='398' height='253' alt='CD-Player'>"; //$NON-NLS-1$
	private static final String recordingImage = "<img src='images/menu/record.gif' width='25' height='25' alt='recording...'>"; //$NON-NLS-1$

	private boolean changeCanvasToWebBrowser = true;

	private String posting = ""; //$NON-NLS-1$
	private String link = "";

	enum UIState {
		AUTH, CHOOSE, DICTATE, DICTATE_REVIEW
	};

	private UIState currentState = UIState.AUTH;

	public void setPosting(String posting) {
		this.posting = posting;
	}

	public void setAuthenticationState(String link) {
		this.link = link;
		currentState = UIState.AUTH;
	}

	public void setDefault() {
		changeCanvasToWebBrowser = true;
		posting = ""; //$NON-NLS-1$
		currentState = UIState.CHOOSE;
	}

	public void setDictation() {
		currentState = UIState.DICTATE;
	}

	public void setDicationReview() {
		currentState = UIState.DICTATE_REVIEW;
	}

	@Override
	public String getMenu() {
		String menu = ""; //$NON-NLS-1$
		switch (currentState) {
		case AUTH:
			menu = "<table><tr>" //$NON-NLS-1$
					+ "<td>" + Messages.getString("Social.PleaseAuthenticate") + "</td>" //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
					+ "</tr></table>"; //$NON-NLS-1$
			break;
		case DICTATE:
			menu = "<table><tr>" //$NON-NLS-1$
					+ "<td>" + Messages.getString("Social.FinishFinish") + "</td>" //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
					+ "<td>" + Messages.getString("Social.Cancel") + "</td>" //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
					+ "</tr></table>"; //$NON-NLS-1$
			break;
		case DICTATE_REVIEW:
			menu = "<table><tr>" //$NON-NLS-1$
					+ "<td>" + Messages.getString("Social.Accept") + "</td>" //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
					+ "<td>" + Messages.getString("Social.Abort") + "</td>" //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
					+ "</tr></table>"; //$NON-NLS-1$
			break;
		case CHOOSE:
		default:
			menu = "<table><tr>" + "<td>" + Messages.getString("Social.Post") + "</td>" + "<td>" //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
					+ Messages.getString("Social.AllUpdates") + "</td>" + "<td>" + Messages.getString("Social.UnreadUpdates") + "</td>" //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
					+ "</tr></table>"; //$NON-NLS-1$
		}
		return menu;
	}

	@Override
	public String getFooter() {
		String menu = ""; //$NON-NLS-1$
		switch (currentState) {
		case CHOOSE:
			menu = "<table><tr>" + "<td>" + Messages.getString("Social.Logout") + "</td>" + "<td>" + Messages.getString("TwitterUi.Help") + "</td>" + "<td>" + Messages.getString("TwitterUi.Back") //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$ //$NON-NLS-8$ //$NON-NLS-9$
					+ "</td>" + "</tr></table>"; //$NON-NLS-1$ //$NON-NLS-2$
			break;
		case DICTATE:
		case DICTATE_REVIEW:
		case AUTH:
		default:
			menu = "<table><tr>" + "<td>" + Messages.getString("TwitterUi.Help") + "</td>" + "<td>" + Messages.getString("TwitterUi.Back") //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$
					+ "</td>" + "</tr></table>"; //$NON-NLS-1$ //$NON-NLS-2$
			break;

		}
		return menu;
	}

	@Override
	public String getResult() {
		String returnResult = ""; //$NON-NLS-1$
		switch (currentState) {
		case DICTATE:
			if (posting.isEmpty()) {
				returnResult = Messages.getString("Social.PleaseDictate"); //$NON-NLS-1$
				break;
			}
		case DICTATE_REVIEW:
			returnResult = posting;
			break;
		case AUTH:
			returnResult = "<a href=\"" + link + "\">" + Messages.getString("TwitterUi.TwitterAuthentication") + "</a><br>"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
			break;
		case CHOOSE:
		default:
			returnResult = Messages.getString("Social.PleaseMakeASelection"); //$NON-NLS-1$
			break;
		}
		return returnResult;

	}

	@Override
	public String getServerStatus() {
		String status = ""; //$NON-NLS-1$
		switch (currentState) {
		case DICTATE:
			status = recordingImage;
			break;
		case AUTH:
		case CHOOSE:
		case DICTATE_REVIEW:
		default:
			status = Messages.getString("TwitterUi.Twitter"); //$NON-NLS-1$; //$NON-NLS-1$
			break;

		}
		return status;
	}

	@Override
	public String getCanvas() {
		return twitterImage;
	}

	@Override
	public boolean isCanvasChanged() {
		return changeCanvasToWebBrowser;
	}

	@Override
	public void setCanvasChanged(boolean changed) {
		changeCanvasToWebBrowser = changed;
	}

	@Override
	public String getResultsToShow() {
		return null;
	}

	@Override
	public String getMapResults() {
		return null;
	}

	@Override
	public String getLatFrom() {
		return null;
	}

	@Override
	public String getLngFrom() {
		return null;
	}

	@Override
	public String getLatTo() {
		return null;
	}

	@Override
	public String getLngTo() {
		return null;
	}

	@Override
	public void writeToConsole() {
		if (!posting.isEmpty()) {
			logger.debug("writeToConsole() - {}", posting);
		}
		else {
			logger.debug("writeToConsole() - {}", Messages.getString("Social.PleaseMakeASelection")); //$NON-NLS-1$
		}
	}

	@Override
	public Boolean isIncreasedResult() {
		return false;
	}

}
