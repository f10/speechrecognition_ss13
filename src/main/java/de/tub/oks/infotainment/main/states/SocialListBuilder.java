/* 
 * Copyright 2013 Berlin Institute of Technology (TU Berlin), 
 * Faculty IV (CS), Department for Open Communication Systems (OKS).
 * 
 * This file is part of SpeechRecognition.
 * SpeechRecognition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SpeechRecognition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SpeechRecognition.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.tub.oks.infotainment.main.states;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import de.tub.oks.infotainment.main.RecognizedStateMachineState;
import de.tub.oks.infotainment.main.UserInterface;
import de.tub.oks.infotainment.main.Messages;
import de.tub.oks.infotainment.main.RecognizingStateMachine;
import de.tub.oks.infotainment.main.SettingsAndEnvironment;
import de.tub.oks.infotainment.remoteservice.socialservice.SocialServicePost;

/**
 * handles rendering of social posts and enables navigation...
 * 
 */
public class SocialListBuilder implements RecognizedStateMachineState {

	/**
	 * Logger for this class
	 */
	private static final Logger logger = LoggerFactory.getLogger(SocialListBuilder.class);

	private SettingsAndEnvironment misc = SettingsAndEnvironment.getInstance();

	private boolean finished = true;

	private RecognizingStateMachine machine;

	private List<SocialServicePost> posts;

	private RecognizedStateMachineState lastState;

	private SocialListBuilderUi ui = new SocialListBuilderUi();

	public SocialListBuilder(RecognizingStateMachine machine, List<SocialServicePost> posts, RecognizedStateMachineState lastState) {
		this.machine = machine;
		this.posts = posts;
		this.lastState = lastState;
	}

	// builds the List to Browser and CMD
	private int showResultList(int linesToShow, int i, int next, List<SocialServicePost> posts) {
		int resultsToShow = 0;

		if (misc.debug >= 1)
			logger.debug("showResultList(int, int, int, List<SocialServicePost>) - {}", Messages.getString("Global.HashlineLong")); //$NON-NLS-1$

		String printHelper = ""; //$NON-NLS-1$

		for (i = 0; i < linesToShow; i++) {
			if (i + next < posts.size()) {
				String user = posts.get(i + next).getUsername();
				String text = posts.get(i + next).getText();
				printHelper += i + 1 + ") " + user + "\n"; //$NON-NLS-1$ //$NON-NLS-2$
				printHelper += text + "\n"; //$NON-NLS-1$

				logger.debug("showResultList(int, int, int, List<SocialServicePost>) - {}", printHelper);
				resultsToShow = i + 1;
			}
			else
				logger.debug("showResultList(int, int, int, List<SocialServicePost>) - {}", ""); // F�lle die Liste auf, wenn der Array
			// leer ist
		}

		// aufrunden der Seitenzahl
		int noOfPages10 = (posts.size() * 10) / linesToShow; // java Trick:
																// int/int
																// wird
																// automatisch
																// gerundet,
																// daher *10
		int noOfPagesRounded10 = (posts.size() / linesToShow) * 10;
		int noOfPagesRounded = noOfPagesRounded10 / 10;
		if (noOfPages10 - noOfPagesRounded10 > 0)
			noOfPagesRounded++;

		if (misc.debug >= 1)
			logger.debug(
					"showResultList(int, int, int, List<SocialServicePost>) - {}", Messages.getString("Global.HashlineLong") + "\n" + Messages.getString("Output.Previous") + "||" + Messages.getString("Output.Next") + "||" + Messages.getString("Output.Back") + "||" + Messages.getString("ListBuilder.Page") + (next + 4) / 4 + "/" + noOfPagesRounded + Messages.getString("Global.HashlineLong"));//$NON-NLS-1$

		ui.printResultToWebBrowser = printHelper;

		ui.resultsToShow = resultsToShow;
		ui.setHeadMenu(((i + next) / linesToShow), noOfPagesRounded);

		return i;
	}

	@Override
	public void stateStarted() {
		machine.setGrammar("sociallistbuilder"); //$NON-NLS-1$
		i = showResultList(linesToShow, 0, next, posts);
		machine.say(Messages.getString("ListBuilder.NavigateAndRead")); //$NON-NLS-1$	
		machine.listen();
	}

	@Override
	public void stateFinished() {

	}

	private int linesToShow = 2;
	private int next = 0;
	private int i = 0;

	@Override
	public void handleActionWords(String words) {
		finished = false;

		if (words.matches(Messages.getString("ListBuilder.recNext"))) { //$NON-NLS-1$
			if (next <= posts.size() - i - 1) {
				machine.say(Messages.getString("Output.ListingNextResults")); //$NON-NLS-1$
				next = next + linesToShow;
			}
			else
				machine.say(Messages.getString("Output.NoMoreResults")); //$NON-NLS-1$
		}
		else if (words.matches(Messages.getString("ListBuilder.recPrevious"))) { //$NON-NLS-1$ //$NON-NLS-2$
			if (next >= 1) {
				machine.say(Messages.getString("Output.ListingPreviousResults")); //$NON-NLS-1$
				next = next - linesToShow;
			}
			else
				machine.say(Messages.getString("Output.NotPossible")); //$NON-NLS-1$
		}
		else if (words.matches(Messages.getString("Global.recRead"))) { //$NON-NLS-1$ //$NON-NLS-2$
			readCurrent(linesToShow, 0, next, posts);
		}
		else if (words.matches(Messages.getString("Global.recBack"))) { //$NON-NLS-1$
			machine.setState(lastState);
		}
		else { // Did not understand what the User said
			machine.say(Messages.getString("Output.DidNotUnderstand") + Messages.getString("Output.PleaseRepeatYourRequest")); //$NON-NLS-1$ //$NON-NLS-2$
		}

		i = showResultList(linesToShow, 0, next, posts);

		machine.listen();
		finished = true;
	}

	/**
	 * Generate text to speech output for shown entries
	 * 
	 * @param linesToShow
	 * @param i
	 * @param next
	 * @param posts
	 */
	private void readCurrent(int linesToShow, int i, int next, List<SocialServicePost> posts) {

		String sayHelper = ""; //$NON-NLS-1$

		for (i = 0; i < linesToShow; i++) {
			if (i + next < posts.size()) {
				String user = posts.get(i + next).getUsername();
				String text = posts.get(i + next).getText();
				sayHelper += user + Messages.getString("SocialListBuilder.Writes") + text; //$NON-NLS-1$

			}
		}
		machine.say(sayHelper);
	}

	@Override
	public boolean isFinished() {
		return finished;
	}

	@Override
	public UserInterface getUi() {
		return ui;
	}

	@Override
	public String getMode() {
		return "ListBuilder"; //$NON-NLS-1$
	}

}
