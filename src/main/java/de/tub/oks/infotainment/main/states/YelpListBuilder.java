/* 
 * Copyright 2013 Berlin Institute of Technology (TU Berlin), 
 * Faculty IV (CS), Department for Open Communication Systems (OKS).
 * 
 * This file is part of SpeechRecognition.
 * SpeechRecognition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SpeechRecognition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SpeechRecognition.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.tub.oks.infotainment.main.states;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import de.tub.oks.infotainment.main.RecognizedStateMachineState;
import de.tub.oks.infotainment.main.UserInterface;
import de.tub.oks.infotainment.main.Messages;
import de.tub.oks.infotainment.main.RecognizingStateMachine;
import de.tub.oks.infotainment.main.SettingsAndEnvironment;

public class YelpListBuilder implements RecognizedStateMachineState {

	/**
	 * Logger for this class
	 */
	private static final Logger logger = LoggerFactory.getLogger(YelpListBuilder.class);

	private RecognizingStateMachine machine;
	public String lati_current;
	public String long_current;
	public YelpResponse response;
	public String search_term;
	/**
	 * @uml.property name="misc"
	 * @uml.associationEnd
	 */
	private SettingsAndEnvironment misc = SettingsAndEnvironment.getInstance();

	/**
	 * @uml.property name="finished"
	 */
	private boolean finished = true;

	enum Status {
		START, DETAIL
	};

	/**
	 * current status of this process
	 */
	private Status currentStatus = Status.START;

	/**
	 * text which will be repeat on asking for help
	 */
	private String currentHelpText = "";

	private YelpListBuilderUi ui = new YelpListBuilderUi();

	public YelpListBuilder(RecognizingStateMachine machine, YelpResponse obj, String lati, String longi, String search_term) {
		this.machine = machine;
		this.response = obj;
		this.lati_current = lati;
		this.long_current = longi;
		this.search_term = search_term;
	}

	@Override
	public void stateStarted() {
		logger.debug("stateStarted() - {}", "Yelp list builder state started ...................................");
		machine.setGrammar("resultlist"); //$NON-NLS-1$
		// i = showResultList(linesToShow, 0, next, resultList);
		//sayOptions();
		currentHelpText = showResultsList();
		machine.say(currentHelpText + Messages.getString("Yelp.SayOneTwoThreeFourForSelect"));
		machine.waitForSpeech();
		machine.listen();

	}

	@Override
	public void stateFinished() {
		// TODO Auto-generated method stub

	}

	//	private void sayOptions() {
	//		machine.say("Your query returned " + this.response.total + "results");
	//
	//	}

	private String showResultsList() {
		String printHelper = "Search Results for " + search_term + " :\n";
		int resultsToShow = 4;
		String mapResults = "";
		ArrayList<Business> businesses = new ArrayList<Business>();
		if (response.total == 0) {
			logger.debug("showResultsList() - {}", "Error: No businesses were found near that location");
			return "";
		}
		else if (response.total < 4) {
			resultsToShow = response.total;
		}
		for (int i = 0; i < resultsToShow; i++) {
			Business biz = response.businesses.get(i);
			printHelper += (i + 1) + " ) " + biz.name + "\n";
			businesses.add(biz);
		}
		logger.debug("showResultsList() - {}", printHelper);
		ui.setResult(printHelper);

		Gson gson = new Gson();
		// convert java object to JSON format,
		// and returned as JSON formatted string
		mapResults += gson.toJson(businesses);
		logger.debug("showResultsList() - {}", mapResults);
		ui.setMapResults(mapResults);
		ui.setCanvas("4");
		machine.listen();
		return printHelper;

	}

	private void showChosenResult(int i) {
		// set the chosen destination to display in browser
		logger.debug("showChosenResult(int) - {}", "showing chosen result from the list of yelp");
		ui.setPrintLatFrom(lati_current);
		ui.setPrintLngFrom(long_current);
		ui.setPrintLatTo("" + response.businesses.get(i).location.coordinate.latitude);
		ui.setPrintLngTo("" + response.businesses.get(i).location.coordinate.longitude);
		ui.setResult(response.businesses.get(i).name);
		ui.setCanvas("3");
	}

	@Override
	public void handleActionWords(String words) {
		finished = false;
		if (currentStatus == Status.START) {
			if (words.matches(Messages.getString("ListBuilder.recOne"))) { //$NON-NLS-1$
				machine.say(Messages.getString("Output.YouHaveCosenResult") + Messages.getString("Output.One") + "  " + response.businesses.get(0).name
						+ "with distance : " + response.businesses.get(0).distance);
				showChosenResult(0);
				machine.listen();

			}
			//			if(words.matches(Messages.getString("ListBuilder.detail"))) 
			//			currentStatus = Status.DETAIL;
			else if (words.matches(Messages.getString("ListBuilder.recTwo"))) { //$NON-NLS-1$
				if (ui.resultsToShow >= 2) {
					machine.say(Messages.getString("Output.YouHaveCosenResult") + Messages.getString("Output.Two")); //$NON-NLS-1$ //$NON-NLS-2$
					showChosenResult(1);
					//machine.listen();
				}
				else {
					machine.say(Messages.getString("Output.NotPossible")); //$NON-NLS-1$
				}
			}
			else if (words.matches(Messages.getString("ListBuilder.recThree"))) { //$NON-NLS-1$
				if (ui.resultsToShow >= 3) {
					machine.say(Messages.getString("Output.YouHaveCosenResult") + Messages.getString("Output.Three")); //$NON-NLS-1$ //$NON-NLS-2$
					showChosenResult(2);
				}
				else {
					machine.say(Messages.getString("Output.NotPossible")); //$NON-NLS-1$
				}
			}
			else if (words.matches(Messages.getString("ListBuilder.recFour"))) { //$NON-NLS-1$
				if (ui.resultsToShow == 4) {
					machine.say(Messages.getString("Output.YouHaveCosenResult") + Messages.getString("Output.Four")); //$NON-NLS-1$ //$NON-NLS-2$
					showChosenResult(3);
				}
				else {
					machine.say(Messages.getString("Output.NotPossible")); //$NON-NLS-1$
				}
			}
			else if (words.matches(Messages.getString("Global.recBack"))) { //$NON-NLS-1$
				machine.setState(new MainMenu(machine));
			}
			else { // Did not understand what the User said
				machine.say(Messages.getString("Output.DidNotUnderstand") + Messages.getString("Output.PleaseRepeatYourRequest")); //$NON-NLS-1$ //$NON-NLS-2$
			}
			machine.listen();

		}

		//		if (currentStatus == Status.DETAIL) {
		//			String detailedInfo=response.businesses.get(0).phone;
		//			machine.say(detailedInfo);
		//		}

		finished = true;
	}

	@Override
	public boolean isFinished() {
		// TODO Auto-generated method stub
		return finished;
	}

	@Override
	public UserInterface getUi() {
		// TODO Auto-generated method stub
		return ui;
	}

	@Override
	public String getMode() {
		// TODO Auto-generated method stub
		return "ListBuilder";
	}

}
