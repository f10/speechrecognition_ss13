/* 
 * Copyright 2013 Berlin Institute of Technology (TU Berlin), 
 * Faculty IV (CS), Department for Open Communication Systems (OKS).
 * 
 * This file is part of SpeechRecognition.
 * SpeechRecognition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SpeechRecognition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SpeechRecognition.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.tub.oks.infotainment.main.states;

import de.tub.oks.infotainment.main.UserInterface;
import de.tub.oks.infotainment.main.Messages;

/**
 * google places state UI handler
 * 
 */
public class PoiUi implements UserInterface {

	private static final String poiImage = "<img src='images/menu/poi.png' width='398' height='253' alt='POI'>"; //$NON-NLS-1$
	private static final String poiRightImage = "<img src='images/menu/poiright.png' width='398' height='253' alt='POI Right'>"; //$NON-NLS-1$
	private static final String poiLeftImage = "<img src='images/menu/poileft.png' width='398' height='253' alt='POI Left'>"; //$NON-NLS-1$
	private static final String poiNoResultImage = "<img src='images/menu/poinoresults.png' width='398' height='253' alt='No Result'>"; //$NON-NLS-1$
	private static final String poiAnalysingImage = "<img src='images/menu/poianalysing.png' width='398' height='253' alt='Analysing'>"; //$NON-NLS-1$
	private static final String poiIncreaseRadius = "<img src='images/menu/poiincreaseradius.png' width='398' height='253' alt='Analysing'>"; //$NON-NLS-1$
	private static final String poiNoMatchImage = "<img src='images/menu/poinomatch.png' width='398' height='253' alt='No Match'>"; //$NON-NLS-1$
	private static final String poiCanceledImage = "<img src='images/menu/poicanceled.png' width='398' height='253' alt='POI Left'>"; //$NON-NLS-1$

	private boolean canvasChanged = true;

	// webserver output
	private String printMenuToWebBrowser = ""; //$NON-NLS-1$
	private String printFooterMenuToWebBrowser = ""; //$NON-NLS-1$

	private String printResultToWebBrowser = Messages.getString("Output.NoResults"); //$NON-NLS-1$

	// Your are Here
	private String printLatFrom = ""; //$NON-NLS-1$

	private String printLngFrom = ""; //$NON-NLS-1$

	// Chosen Result
	private String printLatTo = ""; //$NON-NLS-1$
	private String printLngTo = ""; //$NON-NLS-1$

	private String canvasToWebBrowser = "";

	public void startRemote() {
		printMenuToWebBrowser = "\t"; //$NON-NLS-1$
		printResultToWebBrowser = Messages.getString("Output.StartRemoteService"); //$NON-NLS-1$
		printFooterMenuToWebBrowser = "\t"; //$NON-NLS-1$
	}

	public void poi() {
		canvasChanged = true;
		canvasToWebBrowser = poiImage;
	}

	public void poiWaiting() {
		printResultToWebBrowser = Messages.getString("Output.PleaseSayASearchItem"); //$NON-NLS-1$
	}

	public void poiRight() {
		canvasChanged = true;
		canvasToWebBrowser = poiRightImage;
	}

	public void poiLeft() {
		canvasChanged = true;
		canvasToWebBrowser = poiLeftImage;
	}

	public void poiNoResults(String searchTerm, int radius) {
		canvasChanged = true;
		canvasToWebBrowser = poiNoResultImage;
		printResultToWebBrowser = "" //$NON-NLS-1$
				+ Messages.getString("Output.SearchResultsFor") + searchTerm + "\"\n" //$NON-NLS-1$ //$NON-NLS-2$
				+ Messages.getString("Output.NoResultsFound") //$NON-NLS-1$
				+ Messages.getString("Output.PleaseChangeSearchRequest") //$NON-NLS-1$
				+ Messages.getString("Output.CurrentRadius") + radius + Messages.getString("Output.shortMeters") //$NON-NLS-1$ //$NON-NLS-2$
		;
		printFooterMenuToWebBrowser = Messages.getString("Output.Radius"); //$NON-NLS-1$
	}

	public void poiAnalysing() {
		canvasChanged = true;
		canvasToWebBrowser = poiAnalysingImage;
		printResultToWebBrowser = Messages.getString("Output.AnalysingSearch"); //$NON-NLS-1$
	}

	public void poiIncreaseRadius(int radius) {
		canvasChanged = true;
		canvasToWebBrowser = poiIncreaseRadius;
		printResultToWebBrowser = Messages.getString("Output.IncreaseSearchRadius") //$NON-NLS-1$
				+ Messages.getString("Output.NewRadius") + radius + Messages.getString("Output.shortMeters") //$NON-NLS-1$ //$NON-NLS-2$
		;
	}

	public void poiNotUnderstand() {
		canvasChanged = true;
		canvasToWebBrowser = poiNoMatchImage;
		printResultToWebBrowser = Messages.getString("Output.SearchRequestNotUnderstand"); //$NON-NLS-1$
	}

	public void poiReceiving() {
		printResultToWebBrowser = Messages.getString("Output.ReservedResults"); //$NON-NLS-1$
	}

	public void poiCanceled() {
		printResultToWebBrowser = Messages.getString("Output.SearchAborted"); //$NON-NLS-1$
		canvasChanged = true;
		canvasToWebBrowser = poiCanceledImage;
	}

	@Override
	public String getMenu() {
		return printMenuToWebBrowser;
	}

	@Override
	public String getFooter() {
		return printFooterMenuToWebBrowser;
	}

	@Override
	public String getResult() {
		return printResultToWebBrowser;
	}

	@Override
	public String getServerStatus() {
		return null;
	}

	@Override
	public String getCanvas() {
		return canvasToWebBrowser;
	}

	@Override
	public boolean isCanvasChanged() {
		return canvasChanged;
	}

	@Override
	public void setCanvasChanged(boolean changed) {
		canvasChanged = changed;
	}

	@Override
	public String getResultsToShow() {
		return null;
	}

	@Override
	public String getMapResults() {
		return null;
	}

	@Override
	public String getLatFrom() {
		return printLatFrom;
	}

	@Override
	public String getLngFrom() {
		return printLngFrom;
	}

	@Override
	public String getLatTo() {
		return printLatTo;
	}

	@Override
	public String getLngTo() {
		return printLngTo;
	}

	@Override
	public void writeToConsole() {
	}

	public void setPrintLatFrom(String printLatFrom) {
		this.printLatFrom = printLatFrom;
	}

	public void setPrintLngFrom(String printLngFrom) {
		this.printLngFrom = printLngFrom;
	}

	public void setPrintLatTo(String printLatTo) {
		this.printLatTo = printLatTo;
	}

	public void setPrintLngTo(String printLngTo) {
		this.printLngTo = printLngTo;
	}

	@Override
	public Boolean isIncreasedResult() {
		return false;
	}

}
