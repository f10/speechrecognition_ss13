/* 
 * Copyright 2013 Berlin Institute of Technology (TU Berlin), 
 * Faculty IV (CS), Department for Open Communication Systems (OKS).
 * 
 * This file is part of SpeechRecognition.
 * SpeechRecognition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SpeechRecognition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SpeechRecognition.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.tub.oks.infotainment.main.states;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.tub.oks.infotainment.main.RecognizedStateMachineState;
import de.tub.oks.infotainment.main.UserInterface;
import de.tub.oks.infotainment.main.Messages;
import de.tub.oks.infotainment.main.RecognizingStateMachine;

/**
 * Initial state in state machine
 */
public class StartUp implements RecognizedStateMachineState {

	/**
	 * Logger for this class
	 */
	private static final Logger logger = LoggerFactory.getLogger(StartUp.class);

	private boolean finished = true;

	private RecognizingStateMachine machine;

	private StartUpUi ui = new StartUpUi();

	/**
	 * mutes speech outputs if true
	 */
	private boolean mute;

	public StartUp(RecognizingStateMachine machine) {
		this(machine, false);
	}

	public StartUp(RecognizingStateMachine machine, boolean mute) {
		this.machine = machine;
		this.mute = mute;
	}

	@Override
	public boolean isFinished() {
		return finished;
	}

	@Override
	public void stateStarted() {
		machine.setGrammar("standby"); //$NON-NLS-1$
		if (!mute) {
			machine.say(Messages.getString("Output.WelcomeToYourIntelligentControlUnit") + Messages.getString("StartUp.WakeUpComputer")
					+ Messages.getString("StartUp.ExitClose"));
		}
		machine.listen();
	}

	@Override
	public void stateFinished() {
	}

	@Override
	public void handleActionWords(String words) {
		finished = false;

		if (words.matches(Messages.getString("StartUp.recWakeUp")) || words.matches(Messages.getString("StartUp.recComputer"))) { // Starte //$NON-NLS-1$ //$NON-NLS-2$
			// das
			// Hauptmen�
			ui.writeToConsole();
			logger.debug("handleActionWords(String) - {}", Messages.getString("StartUp.WakingUp")); //$NON-NLS-1$
			machine.setState(new MainMenu(machine));
		}
		else if (words.matches(Messages.getString("Global.recExit"))) { // Beende das Programm //$NON-NLS-1$
			machine.setState(new Exit(machine));
		}
		else {
			// Benutzereingabe nicht verstanden
			machine.say(Messages.getString("Output.DidNotUnderstand") + Messages.getString("Output.PleaseRepeatYourRequest"));
			machine.listen();
		}

		finished = true;

	}

	@Override
	public UserInterface getUi() {
		return ui;
	}

	@Override
	public String getMode() {
		return "StartUp";
	}

}
