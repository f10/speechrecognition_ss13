/* 
 * Copyright 2013 Berlin Institute of Technology (TU Berlin), 
 * Faculty IV (CS), Department for Open Communication Systems (OKS).
 * 
 * This file is part of SpeechRecognition.
 * SpeechRecognition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SpeechRecognition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SpeechRecognition.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.tub.oks.infotainment.main;

/**
 * Every functionality in the application is represented by a state every state
 * needs to implement this interface
 * 
 */
public interface RecognizedStateMachineState {

	/**
	 * is called just after the new state becomes available
	 */
	public void stateStarted();

	/**
	 * is called at least step before the state will be leaved
	 */
	public void stateFinished();

	/**
	 * this method is called for every recognized speech words (only if the
	 * state has called one of the listen methods
	 * 
	 * @param words
	 */
	public void handleActionWords(String words);

	/**
	 * checks if the current state is still handling some speech recognitions
	 * 
	 * @return
	 */
	public boolean isFinished();

	/**
	 * get appropriate user interface implementation for the state
	 * 
	 * @return
	 */
	public UserInterface getUi();

	/**
	 * Should describe function or sub-function of this state for recognition
	 * 
	 * @return
	 */
	public String getMode();

}
