/* 
 * Copyright 2013 Berlin Institute of Technology (TU Berlin), 
 * Faculty IV (CS), Department for Open Communication Systems (OKS).
 * 
 * This file is part of SpeechRecognition.
 * SpeechRecognition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SpeechRecognition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SpeechRecognition.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.tub.oks.infotainment.main;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.util.Locale;
import java.util.Properties;
import java.util.StringTokenizer;

/**
 * Provides general application settings; is implemented as singleton
 */
public class SettingsAndEnvironment {

	/**
	 * Logger for this class
	 */
	private static final Logger logger = LoggerFactory.getLogger(SettingsAndEnvironment.class);

	/**
	 * possible text to speech and recognition engine Google
	 */
	public static final String GoogleSpeechAPI = "google";
	/**
	 * possible text to speech and recognition engine Microsoft SAPI
	 */
	public static final String MicrosoftSpeechAPI = "sapi";
	/**
	 * possible speech to text engine sphinx
	 */
	public static final String SphinxSpeechAPI = "sphinx";

	/**
	 * path to the property file
	 */
	private static final String PROP_FILE = "/de/tub/oks/infotainment/res/settings/app.properties";

	/**
	 * Debug Settings 1= System.outPrintln activated 2 = System.outPrintln +
	 * System Messages
	 */
	public int debug;

	public boolean takeCityAsSampleResult = false;
	/**
	 * if true no Remote-Services and using samples
	 */
	public boolean poiStaysOffline;
	public boolean useGPSStick = false;

	/**
	 * comport for gps stick
	 */
	public String comport;

	// Synthesizer Settings
	/**
	 * voice name
	 */
	public String voice;
	/**
	 * voice speed
	 */
	public double speed;
	/**
	 * voice volume
	 */
	public double volume;
	/**
	 * current type of synthesize engine
	 */
	private String synthEngine;

	/**
	 * current type of recogniton engine
	 */
	private String localRecognitonEngine;

	/**
	 * Redirect url for facebook oauth servlet
	 */
	private String redirectUrlFacebook;
	/**
	 * Redirect url for twitter oauth servlet
	 */
	private String redirectUrlTwitter;

	/**
	 * url which restarts the update servlet
	 */
	private String restartUpdateUrl;

	/**
	 * entry url
	 */
	private String startUrl;

	/**
	 * current configured language and country
	 */
	private Locale locale;

	/**
	 * if true recognized speech will be repeated immediately
	 */
	private boolean repeatRecognizedWords;

	/**
	 * if true stop every speech output after new recognition
	 */
	private boolean stopCurrentSpeechOnRecognition;

	/**
	 * property object
	 */
	private Properties prop;

	/**
	 * singleton instance
	 */
	private static SettingsAndEnvironment instance = null;

	/**
	 * get singleton instance
	 * 
	 * @return instance
	 */
	public static SettingsAndEnvironment getInstance() {
		if (instance == null) {
			try {
				instance = new SettingsAndEnvironment();
			}
			catch (Exception e) {
				logger.error("getInstance()", e);
				System.exit(-1);
			}
		}
		return instance;
	}

	/**
	 * default constructor, read all configuration from property file
	 * 
	 * @throws Exception
	 */
	protected SettingsAndEnvironment() throws Exception {
		readProperties();
	}

	/**
	 * read properties from predefined file
	 * 
	 * @throws Exception
	 */
	private void readProperties() throws Exception {
		try {
			InputStream is = getClass().getResourceAsStream(PROP_FILE);
			prop = new Properties();
			prop.load(is);
			is.close();
		}
		catch (Exception e) {
			throw new Exception("Failed to read from " + PROP_FILE + " file. \n Using default settings.");
		}

		// get external configuration from properties
		comport = prop.getProperty("comport");
		debug = Integer.valueOf(prop.getProperty("debug.level", "2"));
		voice = prop.getProperty("synthVoice");
		speed = Double.valueOf(prop.getProperty("synthSpeed"));
		volume = Double.valueOf(prop.getProperty("synthVolume"));
		redirectUrlFacebook = prop.getProperty("RedirectUrlFacebook");
		redirectUrlTwitter = prop.getProperty("RedirectUrlTwitter");
		restartUpdateUrl = prop.getProperty("RestartUpdateUrl");
		startUrl = prop.getProperty("StartUrl");
		synthEngine = prop.getProperty("synthEngine").toLowerCase();
		localRecognitonEngine = prop.getProperty("localRecognitionEngine").toLowerCase();

		poiStaysOffline = Boolean.valueOf(prop.getProperty("poiStaysOffline", "false")).booleanValue();

		useGPSStick = Boolean.valueOf(prop.getProperty("useGPSStick", "false")).booleanValue();

		takeCityAsSampleResult = Boolean.valueOf(prop.getProperty("takeCityAsSampleResult", "false")).booleanValue();

		repeatRecognizedWords = Boolean.valueOf(prop.getProperty("repeatRecognizedWords", "false")).booleanValue();

		stopCurrentSpeechOnRecognition = Boolean.valueOf(prop.getProperty("stopCurrentSpeechOnRecognition", "true")).booleanValue();

		StringTokenizer localeTokenizer = new StringTokenizer(prop.getProperty("locale", "en-US"), "-");
		locale = new Locale(localeTokenizer.nextToken(), localeTokenizer.nextToken());
		Messages.setLocale(locale);
	}

	/**
	 * @return the redirectUrlFacebook
	 */
	public String getRedirectUrlFacebook() {
		return redirectUrlFacebook;
	}

	/**
	 * @return the redirectUrlTwitter
	 */
	public String getRedirectUrlTwitter() {
		return redirectUrlTwitter;
	}

	/**
	 * @return the url
	 */
	public String getRestartUpdateUrl() {
		return restartUpdateUrl;
	}

	/**
	 * @return the startUrl
	 */
	public String getStartUrl() {
		return startUrl;
	}

	/**
	 * @return the configured text to speech engine
	 */
	public String getSynthEngine() {
		return synthEngine;
	}

	/**
	 * @return the general/local used recognition engine setting
	 */
	public String getLocalRecognitonEngine() {
		return localRecognitonEngine;
	}

	/**
	 * @return the locale setting
	 */
	public Locale getLocale() {
		return locale;
	}

	/**
	 * @return if every recognitized phrase should be repeated
	 */
	public boolean isRepeatRecognizedWords() {
		return repeatRecognizedWords;
	}

	/**
	 * 
	 * @return if current speech output is interrupted by a new proper
	 *         recognition
	 */
	public boolean isStopCurrentSpeechOnRecognition() {
		return stopCurrentSpeechOnRecognition;
	}

}
