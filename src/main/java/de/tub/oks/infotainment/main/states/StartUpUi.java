/* 
 * Copyright 2013 Berlin Institute of Technology (TU Berlin), 
 * Faculty IV (CS), Department for Open Communication Systems (OKS).
 * 
 * This file is part of SpeechRecognition.
 * SpeechRecognition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SpeechRecognition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SpeechRecognition.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.tub.oks.infotainment.main.states;

import de.tub.oks.infotainment.main.UserInterface;
import de.tub.oks.infotainment.main.Messages;

/**
 * Handles UI for StartUp state
 */
public class StartUpUi implements UserInterface {

	private static final String starswImage = "<img src='images/sternsw.png' width='200' height='200' alt='Command Online - Sleeping'>"; //$NON-NLS-1$

	boolean canvasChanged = true;

	@Override
	public String getMenu() {
		return "<table><tr>" + "<td>" + Messages.getString("StartUpUi.Computer") + "</td>" + "<td>" //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
				+ Messages.getString("StartUpUi.WakeUp") + "</td>" + "</tr></table>"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
	}

	@Override
	public String getFooter() {

		return null;
	}

	@Override
	public String getResult() {

		return null;
	}

	@Override
	public String getServerStatus() {
		return Messages.getString("Output.WakingUp"); //$NON-NLS-1$
	}

	@Override
	public String getCanvas() {
		return starswImage;
	}

	@Override
	public boolean isCanvasChanged() {
		return canvasChanged;
	}

	@Override
	public void setCanvasChanged(boolean changed) {
		canvasChanged = changed;
	}

	@Override
	public String getResultsToShow() {

		return null;
	}

	@Override
	public String getMapResults() {

		return null;
	}

	@Override
	public String getLatFrom() {

		return null;
	}

	@Override
	public String getLngFrom() {

		return null;
	}

	@Override
	public String getLatTo() {

		return null;
	}

	@Override
	public String getLngTo() {

		return null;
	}

	@Override
	public void writeToConsole() {

	}

	@Override
	public Boolean isIncreasedResult() {
		return false;
	}

}
