/* 
 * Copyright 2013 Berlin Institute of Technology (TU Berlin), 
 * Faculty IV (CS), Department for Open Communication Systems (OKS).
 * 
 * This file is part of SpeechRecognition.
 * SpeechRecognition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SpeechRecognition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SpeechRecognition.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.tub.oks.infotainment.main.states;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;

import de.tub.oks.infotainment.main.RecognizedStateMachineState;
import de.tub.oks.infotainment.main.UserInterface;
import de.tub.oks.infotainment.main.Messages;
import de.tub.oks.infotainment.main.RecognizingStateMachine;
import de.tub.oks.infotainment.main.SettingsAndEnvironment;
import de.tub.oks.infotainment.remoteservice.yelp.YelpApi2;

import org.scribe.builder.ServiceBuilder;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.model.Verb;
import org.scribe.oauth.OAuthService;

public class Yelp implements RecognizedStateMachineState {

  /**
   * Logger for this class
   */
  private static final Logger logger = LoggerFactory.getLogger(Yelp.class);

  private RecognizingStateMachine machine;
  // Update tokens here from Yelp developers site, Manage API access.
  private String consumerKey = "Qm88cy-I-pypG81JEngGTQ";
  private String consumerSecret = "bf-0gLv-aJNjxzSsheeAZ7o09EM";
  private String token = "9BvlzVQqc1c5VkZ8iosHIwBGt2adf2BL";
  private String tokenSecret = "V9oU3BkSTszCGVjmjDXcbHTBxuM";

  // Current Point-Coordinates of TEL
  public double sample_lati_current2 = 52.51298;
  public double sample_long_current2 = 13.3199;

  // Current Point-Coordinates of Frauenhofer
  public double sample_lati_current = 52.525909;
  public double sample_long_current = 13.314292;

  OAuthService service;
  Token accessToken;
  /**
   * @uml.property name="finished"
   */
  private boolean finished = true;

  enum Status {
    DEFAULT, DICTATION
  };

  private Status currentStatus = Status.DEFAULT;
  private String leave = "NO_MATCH";
  /**
   * @uml.property name="misc"
   * @uml.associationEnd
   */
  private SettingsAndEnvironment misc = SettingsAndEnvironment.getInstance();

  public Yelp(RecognizingStateMachine machine) {
    this.machine = machine;
  }

  private YelpUi ui = new YelpUi();

  @Override
  public void stateStarted() {
    logger.debug("stateStarted() - {}", "Yelp state started ...................................");
    machine.setGrammar("yelp"); //$NON-NLS-1$	
    this.service = new ServiceBuilder().provider(YelpApi2.class).apiKey(consumerKey).apiSecret(consumerSecret).build();
    this.accessToken = new Token(token, tokenSecret);
    ui.startRemote();
    ui.poi();
    machine.say(" say search to search for places : back for going back to main menu");

    ui.writeToConsole();
    // Show "you are here" in the Map
    ui.setPrintLatFrom("" + sample_lati_current);
    ui.setPrintLngFrom("" + sample_long_current);
    ui.setCanvas("1");
    machine.waitForSpeech();
    // this.handleActionWords("search");
    machine.listen();

  }

  /*
   * If a sucessful API response is received, place markers on the map. If not,
   * display an error.
   */
  private YelpResponse handleResults(String response) {
    Gson gson = new Gson();
    YelpResponse obj = gson.fromJson(response, YelpResponse.class);
    logger.debug("handleResults(String) - {}", "Handling results...");
    return obj;

  }

  /**
   * Search with term and location.
   * 
   * @param term
   *          Search term
   * @param latitude
   *          Latitude
   * @param longitude
   *          Longitude
   * @return JSON string response
   */
  public String search(String term, double latitude, double longitude) {
    ui.poiAnalysing();
    OAuthRequest request = new OAuthRequest(Verb.GET, "http://api.yelp.com/v2/search");
    request.addQuerystringParameter("term", term);
    request.addQuerystringParameter("ll", latitude + "," + longitude);
    this.service.signRequest(this.accessToken, request);
    Response response = request.send();
    logger.debug("search(String, double, double) - {}", "the request sent to Yelp:" + request.getClass().toString());
    logger.debug("search(String, double, double) - {}", "the response from Yelp: " + response.getClass().toString());
    return response.getBody();

  }

  @Override
  public void stateFinished() {
    logger.debug("stateFinished() - {}", "Yelp state is finished");
  }

  @Override
  public void handleActionWords(String words) {

    finished = false;
    boolean handled = false;
    logger.debug("handleActionWords(String) - {}", "Handleactionwords " + words);

    if (currentStatus == Status.DICTATION) {
      if (words.matches(Messages.getString("Global.recBack"))) {
        currentStatus = Status.DEFAULT;
        logger.debug("handleActionWords(String) - {}", "Going back to yelp root menu");
        machine.listen();
      }
      else if (words.equals("") || words == null) { //$NON-NLS-1$
        // If onlineSpeechToText did not understand
        leave = "NO_MATCH"; //$NON-NLS-1$
        if (misc.debug > 1)
          logger.debug("handleActionWords(String) - {}", Messages.getString("Global.HashlineLong") + leave + Messages.getString("Global.HashlineLong"));
        ui.poiNotUnderstand();
        machine.say(Messages.getString("Output.DidNotUnderstand") + Messages.getString("Output.PleaseRepeatYourRequest"));
        machine.listenOnline();
      }
      else {
        // it recognized the search term, now making Yelp API call.
        ui.poiReceiving();
        machine.say(Messages.getString("Poi.AnalysingRecording") + " your search term  " + words);
        String jsonResponse = this.search(words, sample_lati_current, sample_long_current);
        logger.debug("handleActionWords(String) - {}", jsonResponse);
        // If it cannot receive response from Yelp API

        if (jsonResponse == null || jsonResponse.equals("")) { //$NON-NLS-1$
          leave = "NO_API_RESPONSE";
          if (misc.debug > 1)
            logger.debug("handleActionWords(String) - {}", Messages.getString("Global.HashlineLong") + leave + Messages.getString("Global.HashlineLong"));
          ui.poiNoResults(words);
          machine.say(Messages.getString("Output.NoResultsAvailable") + Messages.getString("Output.PleaseChangeYourRequest"));

        }
        else {
          // it received response from Yelp API
          YelpResponse obj = handleResults(jsonResponse);
          if (obj.total == 0) {
            machine.say(Messages.getString("Output.NoResultsAvailable"));
            logger.debug("handleActionWords(String) - {}", "Total number of results: " + obj.total);
            currentStatus = Status.DEFAULT;
          }
          logger.debug("handleActionWords(String) - {}", obj.businesses.get(0).name);

          // just to test, already in the YelpListBuilder
          // System.out
          // .println("langitude of business: "
          // + obj.businesses.get(0).location.coordinate.latitude);
          // ui.setPrintLatTo(""+obj.businesses.get(0).location.coordinate.latitude);
          // ui.setPrintLngTo(""+obj.businesses.get(0).location.coordinate.longitude);
          // ui.setResultsToShow(obj.businesses.get(0).name);

          machine.setState(new YelpListBuilder(machine, obj, Double.toString(sample_lati_current), Double.toString(sample_long_current), words));
        }
      }

    }
    else if (currentStatus == Status.DEFAULT) {
      logger.debug("handleActionWords(String) - {}", "current status set to default");
      if (words.matches(Messages.getString("Global.recBack"))) { //$NON-NLS-1$
        logger.debug("handleActionWords(String) - {}", "current status set to default and said " + words);
        machine.setState(new MainMenu(machine));
        logger.debug("handleActionWords(String) - {}", "leaving yelp, going back to main menu");
      }
      else if (words.matches(Messages.getString("Yelp.recPlaces"))) { //$NON-NLS-1$
        currentStatus = Status.DICTATION;
        logger.debug("handleActionWords(String) - {}", "setting workingstate to DICTATION");
        machine.say(Messages.getString("Yelp.WhatToSearch"));
        // machine.listen();
        machine.listenOnline();
        // there are problems with google on win 7. normally it should listen
        // online here
      }
      else {
        // Speech is not valid for this menu point
        machine.say(Messages.getString("Output.DidNotUnderstand") + Messages.getString("Output.PleaseRepeatYourRequest"));
        machine.listen();
      }
    }
    finished = true;

  }

  @Override
  public boolean isFinished() {

    return finished;
  }

  @Override
  public UserInterface getUi() {
    return ui;
  }

  @Override
  public String getMode() {
    return "Yelp";
  }

}
