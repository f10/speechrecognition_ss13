/* 
 * Copyright 2013 Berlin Institute of Technology (TU Berlin), 
 * Faculty IV (CS), Department for Open Communication Systems (OKS).
 * 
 * This file is part of SpeechRecognition.
 * SpeechRecognition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SpeechRecognition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SpeechRecognition.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.tub.oks.infotainment.main.states;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import de.tub.oks.infotainment.main.RecognizedStateMachineState;
import de.tub.oks.infotainment.main.UserInterface;
import de.tub.oks.infotainment.main.Messages;
import de.tub.oks.infotainment.main.RecognizingStateMachine;
import de.tub.oks.infotainment.remoteservice.newsservice.News;
import de.tub.oks.infotainment.remoteservice.newsservice.NewsCategory;
import de.tub.oks.infotainment.remoteservice.newsservice.NewsEntry;
import de.tub.oks.infotainment.remoteservice.newsservice.NewsFactory;

/**
 * Handles news selection from Bing-News
 */
public class Bing implements RecognizedStateMachineState {

	/**
	 * Logger for this class
	 */
	private static final Logger logger = LoggerFactory.getLogger(Bing.class);

	private boolean finished = true;

	private RecognizingStateMachine machine;

	private News bing;

	private BingUi ui = new BingUi();

	enum Status {
		START, ALL, UNREAD, DICTATION, SEARCHED
	};

	/**
	 * current status of this process
	 */
	private Status currentStatus = Status.START;

	/**
	 * text which will be repeat on asking for help
	 */
	private String currentHelpText = ""; //$NON-NLS-1$

	@SuppressWarnings("deprecation")
	public static String getcontent(String url) {
		URL u;
		InputStream is = null;
		DataInputStream dis;
		String s;
		String temp;
		temp = "";

		try {

			u = new URL(url);
			is = u.openStream(); // throws an IOException
			dis = new DataInputStream(new BufferedInputStream(is));

			while ((s = dis.readLine()) != null) {
				temp = temp + s;
			}

		}
		catch (MalformedURLException mue) {

			logger.debug("getcontent(String) - {}", "Ouch - a MalformedURLException happened.");
			logger.error("getcontent()", mue);
			System.exit(1);

		}
		catch (IOException ioe) {

			logger.debug("getcontent(String) - {}", "Oops- an IOException happened.");
			logger.error("getcontent()", ioe);
			System.exit(1);

		}
		finally {

			try {
				is.close();
			}
			catch (IOException ioe) {
				logger.warn("getcontent(String) - exception ignored", ioe); //$NON-NLS-1$

				// just going to ignore this one
			}

		} // end of 'finally' clause

		return temp;
	}

	public String stripHtmlTags(String text) {
		return text.replaceAll("\\<.*?\\>", "");
	}

	public Bing(RecognizingStateMachine machine, Locale locale) {
		logger.debug("Bing(RecognizingStateMachine, Locale) - {}", "cute RecognizingStateMachine");
		this.machine = machine;
		bing = NewsFactory.getBingNews(locale);
	}

	@Override
	public boolean isFinished() {
		logger.debug("isFinished() - {}", "cute isFinished");
		return finished;
	}

	@Override
	public void stateStarted() {
		logger.debug("stateStarted() - {}", "cute stateStarted");
		machine.setGrammar("bing"); //$NON-NLS-1$
		ui.setDefault();

		currentStatus = Status.START;
		currentHelpText = Messages.getString("Bing.Welcome"); //$NON-NLS-1$
		machine.say(currentHelpText);

		ui.writeToConsole();

		machine.listen();
	}

	/**
	 * generates category selection speech
	 */
	private void sayCategorySelection() {
		logger.debug("sayCategorySelection() - {}", "cute sayCategorySelection");
		currentHelpText = Messages.getString("Bing.PleaseSelectCategory"); //$NON-NLS-1$

		List<NewsCategory> categories = bing.getCategories();

		for (int i = 0; i < categories.size(); i++) {
			currentHelpText += " " + (i + 1) + Messages.getString("Bing.For") //$NON-NLS-1$ //$NON-NLS-2$
					+ categories.get(i).getName() + ";"; //$NON-NLS-1$
		}

		currentHelpText += "." + Messages.getString("Bing.PleaseSayCategoryNumber"); //$NON-NLS-1$ //$NON-NLS-2$

		machine.say(currentHelpText);
	}

	@Override
	public void stateFinished() {
		logger.debug("stateFinished() - {}", Messages.getString("Bing.BingClosed")); //$NON-NLS-1$
		logger.debug("stateFinished() - {}", "cute stateFinished");
	}

	@SuppressWarnings("deprecation")
	@Override
	public void handleActionWords(String words) {
		logger.debug("handleActionWords(String) - {}", "bing handleActionWords - words: " + words);
		finished = false;

		boolean handled = false;

		if (currentStatus == Status.DICTATION) {
			//sage was wir gefunden haben
			//machine.say(Messages.getString("Bing.recognized"));
			//machine.say(words);	
			logger.debug("handleActionWords(String) - {}", "folgendes wurde erkannt: " + words);
			machine.say(Messages.getString("Bing.searchfor") + " " + words);
			//sayCategorySelection(words);
			//sayNewsContain(words);
			finished = true;
			handled = true;
			currentStatus = Status.START;
			machine.listen();

			String temp;
			String suche = words;
			String title = " ";
			String desc = "";
			String titleall = "";
			String tosay = "";

			//		   temp = getcontent("http://api.bing.com/rss.aspx?Source=News&Market=en-US&Version=2.0&Query=" + URLEncoder.encode(suche));
			temp = getcontent("http://news.google.com/?output=rss&q=" + URLEncoder.encode(suche));
			// muss laufen damit die ersten beiden entfernt werden
			temp = temp.substring(temp.indexOf("<title>") + 3, temp.length());
			title = temp.substring(4, temp.indexOf("</title>"));
			temp = temp.substring(temp.indexOf("<title>") + 3, temp.length());
			title = temp.substring(4, temp.indexOf("</title>"));

			List<NewsEntry> news = new ArrayList<NewsEntry>();

			title = "\n\n";

			while (temp.indexOf("<title>") > 0) {
				temp = temp.substring(temp.indexOf("<title>") + 3, temp.length());
				title = "\n" + temp.substring(4, temp.indexOf("</title>"));
				titleall = titleall + title;

				temp = temp.substring(temp.indexOf("<description>") + 3, temp.length());
				desc = temp.substring(10, temp.indexOf("</description>"));
				desc = desc.replaceAll("\"", "\\\""); //we do it to remove the html tags 
				desc = desc.replaceAll("&lt;", "<");
				desc = desc.replaceAll("&gt;", ">");
				logger.debug("handleActionWords(String) - {}", desc);
				//desc = "<table border=\"1\">micha</table>";
				desc = stripHtmlTags(desc);
				tosay = tosay + title + "\n" + desc + "\n";

				news.add(new NewsEntry(title, desc));
			}

			machine.setState(new NewsListBuilder(machine, news, this));

			machine.listen();

		}

		//check if all news or just unread news
		if (words.matches(Messages.getString("Bing.recAllNews"))) { //$NON-NLS-1$
			currentStatus = Status.ALL;
			sayCategorySelection();
			handled = true;
			machine.listen();
		}
		else if (words.matches(Messages.getString("Bing.recUnreadNews"))) { //$NON-NLS-1$
			currentStatus = Status.UNREAD;
			sayCategorySelection();
			handled = true;
			machine.listen();
			//get news for choosen category
		}
		else if (words.matches(Messages.getString("Bing.recSearch"))) { //$NON-NLS-1$
			logger.debug("handleActionWords(String) - {}", "Bereich Suche in News");
			currentStatus = Status.SEARCHED;
			//sayCategorySelection();
			machine.say(Messages.getString("Bing.WhatToSearch"));
			machine.listenOnline();
			currentStatus = Status.DICTATION;
			handled = true;
			//get news for searched word

		}
		else if (currentStatus != Status.START) {
			Integer categoryNumber;

			try {
				categoryNumber = new Integer(words);
			}
			catch (Exception e) {
				logger.error("handleActionWords(String)", e); //$NON-NLS-1$

				categoryNumber = null;
			}

			if (currentStatus == Status.UNREAD) {//ich habe nach etwas gesucht
													//suchbegriff = words
				bing.setOnlyUnread(false);
				machine.setState(new NewsListBuilder(machine, bing.getNews(null), this));
				handled = true;
			}

			if (categoryNumber != null) {
				categoryNumber--;
				List<NewsCategory> categories = bing.getCategories();
				if (categories.size() > categoryNumber) {
					if (currentStatus == Status.UNREAD) {
						bing.setOnlyUnread(true);
					}
					else {
						bing.setOnlyUnread(false);
					}
					//show news in special news state
					machine.setState(new NewsListBuilder(machine, bing.getNews(categories.get(categoryNumber)), this));
					handled = true;
				}
				else { //invalid category number
					machine.say(Messages.getString("Bing.NewsCategoryNumberInvalid")); //$NON-NLS-1$
					handled = true;
					machine.listen();
				}
			}
		}

		//default menu behavior
		if (!handled) {
			if (words.matches(Messages.getString("Global.recBack"))) { //$NON-NLS-1$
				machine.setState(new MainMenu(machine));
			}
			else if (words.matches(".*Help.*")) { //$NON-NLS-1$
				//machine.say(currentHelpText);
				machine.listen();
			}
			else {
				// Benutzereingabe nicht verstanden
				machine.say(//Messages.getString("Output.DidNotUnderstand") + //$NON-NLS-1$
				Messages.getString("Output.PleaseRepeatYourRequest")); //$NON-NLS-1$
				machine.listen();
			}
		}

		finished = true;
	}

	@Override
	public UserInterface getUi() {
		//		System.out.println("cute UserInterface");
		return ui;
	}

	@Override
	public String getMode() {
		//		System.out.println("cute getMode");
		return "Bing"; //$NON-NLS-1$
	}

}
