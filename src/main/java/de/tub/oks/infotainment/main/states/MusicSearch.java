/* 
 * Copyright 2013 Berlin Institute of Technology (TU Berlin), 
 * Faculty IV (CS), Department for Open Communication Systems (OKS).
 * 
 * This file is part of SpeechRecognition.
 * SpeechRecognition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SpeechRecognition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SpeechRecognition.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.tub.oks.infotainment.main.states;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.*;
import java.io.*;

import de.tub.oks.infotainment.audio.Mp3Player;
import de.tub.oks.infotainment.main.RecognizedStateMachineState;
import de.tub.oks.infotainment.main.UserInterface;
import de.tub.oks.infotainment.main.Messages;
import de.tub.oks.infotainment.main.RecognizingStateMachine;

/**
 * Radio example implementation plays hardcoded webstream from hitradio ffh ,
 * could easily expanded
 */
public class MusicSearch implements RecognizedStateMachineState {

	/**
	 * Logger for this class
	 */
	private static final Logger logger = LoggerFactory.getLogger(MusicSearch.class);

	/**
	 * @uml.property name="finished"
	 */
	private boolean finished = true;

	enum Status {
		START, DICTATION
	};

	private Status currentStatus = Status.START;

	/**
	 * @uml.property name="machine"
	 * @uml.associationEnd
	 */
	private RecognizingStateMachine machine;

	/**
	 * @uml.property name="ui"
	 * @uml.associationEnd
	 */
	private MusicSearchUi ui = new MusicSearchUi();

	public MusicSearch(RecognizingStateMachine machine) {
		this.machine = machine;
	}

	/**
	 * @return
	 * @uml.property name="finished"
	 */
	@Override
	public boolean isFinished() {
		return finished;
	}

	@SuppressWarnings("deprecation")
	public static String getcontent(String url) {
		URL u;
		InputStream is = null;
		DataInputStream dis;
		String s;
		String temp;
		temp = "";

		try {

			u = new URL(url);
			is = u.openStream(); // throws an IOException
			dis = new DataInputStream(new BufferedInputStream(is));

			while ((s = dis.readLine()) != null) {
				temp = temp + s;
			}

		}
		catch (MalformedURLException mue) {

			logger.debug("getcontent(String) - {}", "Ouch - a MalformedURLException happened.");
			logger.error("getcontent()", mue);
			System.exit(1);

		}
		catch (IOException ioe) {

			logger.debug("getcontent(String) - {}", "Oops- an IOException happened.");
			logger.error("getcontent()", ioe);
			System.exit(1);

		}
		finally {

			try {
				is.close();
			}
			catch (IOException ioe) {
				// just going to ignore this one
			}

		} // end of 'finally' clause

		return temp;
	}

	@Override
	public void stateStarted() {
		machine.setGrammar("musicsearch"); //$NON-NLS-1$
		machine.say(Messages.getString("Output.StartingMusicSearch") + " " + Messages.getString("MusicSearch.SayBackMainMenu"));
		ui.writeToConsole();
		machine.waitForSpeech();
		// hier muss online suchee gestartet werden
		// machine.listen();
		ui.setCanvasChanged(true);
		currentStatus = Status.DICTATION;
		machine.say(Messages.getString("MusicSearch.whattohear"));
		machine.listenOnline();

	}

	@Override
	public void stateFinished() {
		logger.debug("stateFinished() - {}", Messages.getString("Radio.RadioClosed")); //$NON-NLS-1$
	}

	@SuppressWarnings("deprecation")
	@Override
	public void handleActionWords(String words) {
		finished = false;
		logger.debug("handleActionWords(String) - {}", "Handleactionwords " + words);

		if (currentStatus != Status.DICTATION)
			if (words.matches(Messages.getString("Global.recBack"))) { //$NON-NLS-1$
				stopWebStream();
				machine.setState(new MainMenu(machine));
				logger.debug("handleActionWords(String) - {}", "verlasse music menu");
			}
			else {
				// Benutzereingabe nicht verstanden
				machine.say(Messages.getString("Output.DidNotUnderstand") + Messages.getString("Output.PleaseRepeatYourRequest"));
				machine.listen();
			}

		if (currentStatus == Status.DICTATION) {
			currentStatus = Status.START;
			machine.setGrammar("musicsearch"); //$NON-NLS-1$

			machine.waitForSpeech();
			logger.debug("handleActionWords(String) - {}", "state started- machine listenonline now in handleactionwords");
			String temp;
			String musicurl;
			logger.debug("handleActionWords(String) - {}", "suche bei Amazon nach dem Lied...bitte warten (1/3)");
			temp = getcontent("http://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Ddigital-music&field-keywords=" + URLEncoder.encode(words));
			logger.debug("handleActionWords(String) - {}", "suche bei Amazon nach dem Lied...bitte warten (2/3)");
			if (temp.indexOf("did not match any products") > 0) {
				logger.debug("handleActionWords(String) - {}", "Lied nicht gefunden");
			}
			else {
				temp = temp.substring(temp.indexOf("<td class=\"titleColOdd\">"), temp.length());
				temp = temp.substring(33, temp.indexOf("onclick") - 2);
				temp = "http://www.amazon.com" + temp;
				musicurl = getcontent(temp);
				logger.debug("handleActionWords(String) - {}", "suche bei Amazon nach dem Lied...bitte warten (3/3)");
				logger.debug("handleActionWords(String) - {}", musicurl);
				ui.musicSearchResults(words);

				try {
					startWebStream(new URL(musicurl));
				}
				catch (MalformedURLException e) {
					// e.printStackTrace();
				}
				// not sure why this was done. remove in future versions.
				//        catch (@SuppressWarnings("hiding") IOException e) {
				//          machine.setState(new MainMenu(machine));
				//          // e.printStackTrace();
				//        } 
				finally {
				}
			}
			logger.debug("handleActionWords(String) - {}", "machine listen called");
			machine.listen();

		}

		finished = true;

	}

	/**
	 * @return
	 * @uml.property name="ui"
	 */
	@Override
	public UserInterface getUi() {
		return ui;
	}

	@Override
	public String getMode() {
		return "MusicSearch";
	}

	/**
	 * @uml.property name="player"
	 * @uml.associationEnd
	 */
	private Mp3Player player;

	public void startWebStream(URL url) {
		try {
			player = new Mp3Player(url);
			player.play();
		}
		finally {
			logger.debug("startWebStream(URL) - {}", "Wiedergabe gestartet");
		}
	}

	public void stopWebStream() {
		if (player != null)
			if (player.isAlive() == true)
				player.stopPlay();
	}

	public boolean equals(Object anObject) {
		if (anObject instanceof MusicSearch) {
			return true;
		}
		return false;
	}

}
