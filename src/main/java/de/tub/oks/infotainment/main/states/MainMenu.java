/* 
 * Copyright 2013 Berlin Institute of Technology (TU Berlin), 
 * Faculty IV (CS), Department for Open Communication Systems (OKS).
 * 
 * This file is part of SpeechRecognition.
 * SpeechRecognition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SpeechRecognition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SpeechRecognition.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.tub.oks.infotainment.main.states;

import de.tub.oks.infotainment.main.Messages;
import de.tub.oks.infotainment.main.RecognizedStateMachineState;
import de.tub.oks.infotainment.main.RecognizingStateMachine;
import de.tub.oks.infotainment.main.SettingsAndEnvironment;
import de.tub.oks.infotainment.main.UserInterface;
import de.tub.oks.infotainment.main.states.Poi.RemoteType;

/**
 * main state of application starts most of all other states and is the default
 * return state after a state finished his job
 * 
 */
public class MainMenu implements RecognizedStateMachineState {

  private boolean finished = true;

  private RecognizingStateMachine machine;

  private MainMenuUi ui = new MainMenuUi();

  public MainMenu(RecognizingStateMachine machine) {
    this.machine = machine;
  }

  String currentHelpText = "";

  @Override
  public void handleActionWords(String words) {
    finished = false;
    String recMusic = Messages.getString("MainMenu.recMusic");
    if (words.matches(Messages.getString("MainMenu.recRadio"))) //$NON-NLS-1$
      machine.setState(new Radio(machine));
    else if (words.matches(Messages.getString("MainMenu.recCD"))) //$NON-NLS-1$
      machine.setState(new CD(machine));
    else if (words.matches(Messages.getString("MainMenu.recYelp"))) //$NON-NLS-1$
      machine.setState(new Yelp(machine)); // RemoteService
    //		else if (words.matches(Messages.getString("MainMenu.recMusic"))) //$NON-NLS-1$
    // machine.setState(new MusicSearch(machine)); // RemoteService
    else if (words.matches(Messages.getString("MainMenu.recChangeVoice"))) //$NON-NLS-1$
      machine.setState(new ChangeVoice(machine));
    else if (words.matches(Messages.getString("MainMenu.recSearch"))) //$NON-NLS-1$
      machine.setState(new Poi(machine, RemoteType.all)); // RemoteService
    // only
    else if (words.matches(Messages.getString("MainMenu.recRight"))) //$NON-NLS-1$
      machine.setState(new Poi(machine, RemoteType.right, 150, 150));// RemoteService
    // (PoiFilter)
    else if (words.matches(Messages.getString("MainMenu.recLeft"))) //$NON-NLS-1$
      machine.setState(new Poi(machine, RemoteType.left, 150, 150)); // RemoteService
    // (PoiFilter)
    else if (words.matches(Messages.getString("MainMenu.recStandby"))) { //$NON-NLS-1$
      machine.say(Messages.getString("Output.StartStandby"));
      machine.setState(new StartUp(machine, true)); // RemoteService
    }
    else if (words.matches(Messages.getString("MainMenu.recFacebook"))) //$NON-NLS-1$
      machine.setState(new Facebook(machine)); // RemoteService
    else if (words.matches(Messages.getString("MainMenu.recTwitter"))) //$NON-NLS-1$
      machine.setState(new Twitter(machine)); // RemoteService
    else if (words.matches(Messages.getString("MainMenu.recNews"))) //$NON-NLS-1$
      machine.setState(new Bing(machine, SettingsAndEnvironment.getInstance().getLocale())); // RemoteService
    else if (words.matches(Messages.getString("MainMenu.recMusic"))) //$NON-NLS-1$
      machine.setState(new Spotify(machine, SettingsAndEnvironment.getInstance().getLocale())); // RemoteService
    else if (words.matches(Messages.getString("Global.recExit"))) //$NON-NLS-1$
      ;
    // machine.setState(new Exit(machine));
    else if (words.matches(Messages.getString("MainMenu.recHelp"))) { //$NON-NLS-1$
      machine.say(Messages.getString("Outout.HelpFeedback") + Messages.getString("Output.HelpMainMenu"));
      machine.close();
      machine.listen();
    }
    else {
      // Benutzereingabe nicht verstanden
      machine.say(Messages.getString("Output.DidNotUnderstand") + Messages.getString("Output.PleaseRepeatYourRequest"));
      machine.listen();
    }

    finished = true;
  }

  @Override
  public boolean isFinished() {
    return finished;
  }

  @Override
  public void stateStarted() {
    machine.setGrammar("mainmenu"); //$NON-NLS-1$
    currentHelpText = Messages.getString("Output.EnteringMainMenu");
    machine.say(currentHelpText);
    ui.writeToConsole();
    machine.listen();
  }

  @Override
  public void stateFinished() {

  }

  @Override
  public UserInterface getUi() {
    return ui;
  }

  @Override
  public String getMode() {
    return "MainMenu";
  }

}
