/* 
 * Copyright 2013 Berlin Institute of Technology (TU Berlin), 
 * Faculty IV (CS), Department for Open Communication Systems (OKS).
 * 
 * This file is part of SpeechRecognition.
 * SpeechRecognition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SpeechRecognition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SpeechRecognition.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.tub.oks.infotainment.main.states;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import de.tub.oks.infotainment.main.RecognizedStateMachineState;
import de.tub.oks.infotainment.main.UserInterface;
import de.tub.oks.infotainment.main.Messages;
import de.tub.oks.infotainment.main.RecognizingStateMachine;

/**
 * enable change of voice if available
 * 
 */
public class ChangeVoice implements RecognizedStateMachineState {

	/**
	 * Logger for this class
	 */
	private static final Logger logger = LoggerFactory.getLogger(ChangeVoice.class);

	private boolean finished = true;

	private RecognizingStateMachine machine;

	private ChangeVoiceUi ui = new ChangeVoiceUi();

	public ChangeVoice(RecognizingStateMachine machine) {
		this.machine = machine;
	}

	@Override
	public boolean isFinished() {
		return finished;
	}

	@Override
	public void stateStarted() {
		machine.setGrammar("changevoice"); //$NON-NLS-1$
		String currentVoice = Messages.getString("ChangeVoice.CurrentVoice") + machine.getVoice();
		ui.setResult(currentVoice);
		machine.say(currentVoice + " " + Messages.getString("ChangeVoice.SayChangeBack")); //$NON-NLS-1$	
		machine.listen();
	}

	@Override
	public void stateFinished() {
	}

	@Override
	public void handleActionWords(String words) {
		finished = false;

		if (words.matches(Messages.getString("ChangeVoice.recChange"))) { //$NON-NLS-1$
			machine.say(Messages.getString("Output.ChangeingVoice"));
			//select next voice
			List<String> voices = machine.getVoices();

			logger.debug("handleActionWords(String) - {}", voices);

			int i = (voices.indexOf(machine.getVoice()));
			i++;
			if (i >= voices.size()) {
				i = 0;
			}
			if (voices.size() > 0) {
				machine.setVoice(voices.get(i));
				String currentVoice = Messages.getString("ChangeVoice.CurrentVoice") + voices.get(i);
				ui.setResult(currentVoice);
				machine.say(Messages.getString("Output.VoiceChangedMyNameIs") + voices.get(i) + "."); //$NON-NLS-1$
			}
			machine.listen();
		}
		else if (words.matches(Messages.getString("Global.recBack"))) { //$NON-NLS-1$
			machine.setState(new MainMenu(machine));
		}
		else {
			// Benutzereingabe nicht verstanden
			machine.say(Messages.getString("Output.DidNotUnderstand") + Messages.getString("Output.PleaseRepeatYourRequest"));
			machine.listen();
		}

		finished = true;

	}

	@Override
	public UserInterface getUi() {
		return ui;
	}

	@Override
	public String getMode() {
		return "Change Voice";
	}

}
