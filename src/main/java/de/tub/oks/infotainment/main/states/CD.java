/* 
 * Copyright 2013 Berlin Institute of Technology (TU Berlin), 
 * Faculty IV (CS), Department for Open Communication Systems (OKS).
 * 
 * This file is part of SpeechRecognition.
 * SpeechRecognition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SpeechRecognition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SpeechRecognition.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.tub.oks.infotainment.main.states;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

import de.tub.oks.infotainment.audio.Mp3Player;
import de.tub.oks.infotainment.main.RecognizedStateMachineState;
import de.tub.oks.infotainment.main.UserInterface;
import de.tub.oks.infotainment.main.Messages;
import de.tub.oks.infotainment.main.RecognizingStateMachine;

/**
 * CD state plays hardcoded mp3 file. just a example, could easily expanded
 */
public class CD implements RecognizedStateMachineState {

	/**
	 * Logger for this class
	 */
	private static final Logger logger = LoggerFactory.getLogger(CD.class);

	private boolean finished = true;

	private RecognizingStateMachine machine;

	private CDUi ui = new CDUi();

	public CD(RecognizingStateMachine machine) {
		this.machine = machine;
	}

	@Override
	public boolean isFinished() {
		return finished;
	}

	@Override
	public void stateStarted() {

		machine.setGrammar("cd"); //$NON-NLS-1$
		machine.say(Messages.getString("Output.StartingCDPlayer") + " "
				+ Messages.getString("CD.SayBackMainMenu"));
		ui.writeToConsole();
		machine.waitForSpeech();
		try {
			playAudioFile("/de/tub/oks/infotainment/res/music/FreshBodyShop-Niceday.mp3");
		} catch (IOException e) {
			logger.error("stateStarted()", e);
		}
		machine.listen();
	}

	@Override
	public void stateFinished() {
		logger.debug("stateFinished() - {}", Messages.getString("CD.CDClosed")); //$NON-NLS-1$

	}

	@Override
	public void handleActionWords(String words) {
		finished = false;

		if (words.matches(Messages.getString("Global.recBack"))) { //$NON-NLS-1$
			stopAudioFile();
			machine.setState(new MainMenu(machine));
		} else {
			// Benutzereingabe nicht verstanden
			machine.say(Messages.getString("Output.DidNotUnderstand")
					+ Messages.getString("Output.PleaseRepeatYourRequest"));
			machine.listen();
		}

		finished = true;

	}

	@Override
	public UserInterface getUi() {
		return ui;
	}

	@Override
	public String getMode() {
		return "CD";
	}

	private Mp3Player player;

	public void playAudioFile(String file) throws IOException {

		player = new Mp3Player(getClass().getResourceAsStream(file));
		player.play();
	}

	public void stopAudioFile() {
		player.stopPlay();
	}

}
