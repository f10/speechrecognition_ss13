/* 
 * Copyright 2013 Berlin Institute of Technology (TU Berlin), 
 * Faculty IV (CS), Department for Open Communication Systems (OKS).
 * 
 * This file is part of SpeechRecognition.
 * SpeechRecognition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SpeechRecognition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SpeechRecognition.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.tub.oks.infotainment.main;

import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.tub.oks.infotainment.audio.RecognizedSpeechEvent;
import de.tub.oks.infotainment.audio.SpeechToText;
import de.tub.oks.infotainment.audio.SpeechToTextException;
import de.tub.oks.infotainment.audio.SpeechToTextFactory;
import de.tub.oks.infotainment.audio.SpeechToTextListener;
import de.tub.oks.infotainment.audio.TextToSpeech;
import de.tub.oks.infotainment.audio.TextToSpeechFactory;
import de.tub.oks.infotainment.audio.TextToSpeechFactory.Engine;
import de.tub.oks.infotainment.main.states.StartUp;

/**
 * This state machine holds internal references to all important objects all
 * parts of the application are separated in states, which could be changed in
 * this state machine.
 */
public class RecognizingStateMachine implements SpeechToTextListener {

	/**
	 * Logger for this class
	 */
	private static final Logger logger = LoggerFactory.getLogger(RecognizingStateMachine.class);

	/**
	 * Main entry point of application
	 */
	public static void main(String[] args) throws Exception {
		RecognizingStateMachine machine;
		machine = RecognizingStateMachine.getInstance();
		machine.setState(new StartUp(machine));
	}

	private RecognizedStateMachineState currentState;

	private SpeechToText localSpeechToText;

	private SpeechToText onlineSpeechToText;

	private SettingsAndEnvironment misc;

	private TextToSpeech text2Speech;

	enum SPEECH_REC_ENGINE {
		REMOTE, LOCAL
	}

	private SPEECH_REC_ENGINE currentEngine;

	/**
	 * singleton instance
	 */
	private static RecognizingStateMachine instance;

	/**
	 * get singleton instance
	 * 
	 * @return
	 */
	public static RecognizingStateMachine getInstance() {
		synchronized (RecognizingStateMachine.class) {
			if (null == instance) {
				instance = new RecognizingStateMachine();
			}
		}
		return instance;
	}

	private RecognizingStateMachine() {

		misc = SettingsAndEnvironment.getInstance();

		Locale locale = misc.getLocale();
		initText2SpeechEngine(locale);
		try {
			initSpeech2TextEngines(locale);
		}
		catch (SpeechToTextException e) {
			throw new RuntimeException(e);
		}

	}

	/**
	 * Initialization of local and onle speech to text engines
	 * 
	 * @param locale
	 * @throws SpeechToTextException
	 * @throws Exception
	 */
	private void initSpeech2TextEngines(Locale locale) throws SpeechToTextException {
		de.tub.oks.infotainment.audio.SpeechToTextFactory.Engine speech2TextEngine;

		// choose configured local recognition engine with configured language
		if (misc.getLocalRecognitonEngine().equals(SettingsAndEnvironment.MicrosoftSpeechAPI)) {
			speech2TextEngine = de.tub.oks.infotainment.audio.SpeechToTextFactory.Engine.CLOUDGARDEN;
		}
		else if (misc.getLocalRecognitonEngine().equals(SettingsAndEnvironment.SphinxSpeechAPI)) {
			speech2TextEngine = de.tub.oks.infotainment.audio.SpeechToTextFactory.Engine.SPHINX;
		}
		else if (misc.getLocalRecognitonEngine().equals(SettingsAndEnvironment.GoogleSpeechAPI)) {
			speech2TextEngine = de.tub.oks.infotainment.audio.SpeechToTextFactory.Engine.GOOGLE;
		}
		else { // default
			speech2TextEngine = de.tub.oks.infotainment.audio.SpeechToTextFactory.Engine.CLOUDGARDEN;
		}

		localSpeechToText = SpeechToTextFactory.getSpeechRecognitionEngine(locale, speech2TextEngine);
		localSpeechToText.addSpeechToTextListener(this);

		onlineSpeechToText = SpeechToTextFactory.getSpeechRecognitionEngine(locale, de.tub.oks.infotainment.audio.SpeechToTextFactory.Engine.GOOGLE);
		onlineSpeechToText.addSpeechToTextListener(this);
	}

	/**
	 * Initialization of used text to speech engine
	 * 
	 * @param locale
	 */
	private void initText2SpeechEngine(Locale locale) {
		de.tub.oks.infotainment.audio.TextToSpeechFactory.Engine text2SpeechEngine;

		// choose configured text2speech engine with configured language
		if (misc.getSynthEngine().equals(SettingsAndEnvironment.MicrosoftSpeechAPI)) {
			text2SpeechEngine = Engine.CLOUDGARDEN;
		}
		else if (misc.getSynthEngine().equals(SettingsAndEnvironment.GoogleSpeechAPI)) {
			text2SpeechEngine = Engine.GOOGLE;
		}
		else { // default
			text2SpeechEngine = Engine.CLOUDGARDEN;
		}

		text2Speech = TextToSpeechFactory.getTextToSpeechEngine(misc.voice, (float) misc.volume, (float) misc.speed, locale, text2SpeechEngine);
	}

	/**
	 * changes current state of state machine
	 * 
	 * @param newState
	 */
	public synchronized void setState(RecognizedStateMachineState newState) {
		if (currentState != null) {
			currentState.stateFinished();
		}
		currentState = newState;
		currentState.stateStarted();
	}

	/**
	 * get current state of the state machine
	 * 
	 * @return current state
	 */
	public RecognizedStateMachineState getCurrentState() {
		return currentState;
	}

	@Override
	public synchronized void handleRecognizedSpeech(RecognizedSpeechEvent e) {

		if (currentState != null) {
			while (!currentState.isFinished()) {
				try {
					Thread.sleep(10);
				}
				catch (InterruptedException e1) {
					logger.warn("handleRecognizedSpeech(RecognizedSpeechEvent) - exception ignored", e1); //$NON-NLS-1$

				}
			}
			String words = e.toString();
			if (words.isEmpty())
				return;

			if (misc.isStopCurrentSpeechOnRecognition()) {
				// stop current speech output
				text2Speech.stop();
			}
			if (misc.isRepeatRecognizedWords()) {
				say(words);
			}
			currentState.handleActionWords(words);
		}
	}

	/**
	 * uses text to speech engine for audio output
	 */
	public void say(String textToSay) {
		text2Speech.speak(textToSay);
	}

	/**
	 * wait for finishing current speech output
	 */
	public void waitForSpeech() {
		text2Speech.waitForSpeech();
	}

	/**
	 * change voice of text2speech engine
	 */
	public void setVoice(String voice) {
		text2Speech.setVoice(voice);
	}

	/*
	 * gets current voice of text2speech engine
	 */
	public String getVoice() {
		return text2Speech.getVoice();
	}

	/**
	 * get all available voices from text2speech engine
	 */
	public List<String> getVoices() {
		return text2Speech.getVoices();
	}

	/**
	 * use offline text to speech engine for listing
	 */
	public synchronized void listen() {
		try {
			if (currentEngine == SPEECH_REC_ENGINE.REMOTE) {
				onlineSpeechToText.pause();
			}
			localSpeechToText.listen();
			currentEngine = SPEECH_REC_ENGINE.LOCAL;

		}
		catch (SpeechToTextException e) {
			logger.error("listen() - SpeechToTextException occured. Exiting application.", e);
			System.exit(1);
		}
	}

	/**
	 * use online text to speech engine for listing
	 */
	public synchronized void listenOnline() {
		try {
			if (currentEngine == SPEECH_REC_ENGINE.LOCAL) {
				localSpeechToText.pause();
			}
			currentEngine = SPEECH_REC_ENGINE.REMOTE;
			onlineSpeechToText.listen();
		}
		catch (Exception e) {
			logger.error("listenOnline()", e);
			System.exit(-1);
		}
	}

	/**
	 * set grammar file for local speech engine
	 * 
	 * @param grammar
	 */
	public synchronized void setGrammar(String grammar) {
		try {
			localSpeechToText.setGrammar(grammar);
			logger.debug("setGrammar(String) - {}", "current grammar:" + grammar);
		}
		catch (Exception e) {
			logger.error("setGrammar()", e);
			System.exit(-1);
		}
	}

	/**
	 * set grammar file for online speech engine (if supported)
	 * 
	 * @param grammar
	 */
	public synchronized void setGrammarOnline(String grammar) {
		try {
			onlineSpeechToText.setGrammar(grammar);
		}
		catch (Exception e) {
			logger.error("setGrammarOnline()", e);
			System.exit(-1);
		}
	}

	/**
	 * get UI handler from current state
	 * 
	 * @return
	 */
	public UserInterface getUi() {
		if (currentState != null)
			return currentState.getUi();
		else
			return null;

	}

	@Override
	protected void finalize() throws Throwable {
		close();
	}

	public void close() {
		localSpeechToText.close();
		onlineSpeechToText.close();
	}

	@Override
	public void informProcessing() {
		say(Messages.getString("Global.InformationProcessing"));
	}

	@Override
	public void informRecognized() {
		say(Messages.getString("Global.InformationRecognized"));
	}

	@Override
	public void informBadResult() {
		say(Messages.getString("Global.InformationBad"));
	}

}
