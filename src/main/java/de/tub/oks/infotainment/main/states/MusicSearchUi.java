/* 
 * Copyright 2013 Berlin Institute of Technology (TU Berlin), 
 * Faculty IV (CS), Department for Open Communication Systems (OKS).
 * 
 * This file is part of SpeechRecognition.
 * SpeechRecognition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SpeechRecognition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SpeechRecognition.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.tub.oks.infotainment.main.states;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.tub.oks.infotainment.main.UserInterface;
import de.tub.oks.infotainment.main.Messages;

/**
 * Handles UI for MusicSearch speech interaction
 */
public class MusicSearchUi implements UserInterface {

	/**
	 * Logger for this class
	 */
	private static final Logger logger = LoggerFactory.getLogger(MusicSearchUi.class);

	/**
	 * @uml.property name="canvasChanged"
	 */
	boolean canvasChanged = true;

	private String Suchbegriff = "\n\nBitte nennen Sie die Musik\nwenn sie dazu aufgefordert werden\n";

	private String MusicSearchImage = "<img src='images/menu/MusicSearch.png' width='398' height='253' alt='MusicSearch'>"; //$NON-NLS-1$
	@SuppressWarnings("unused")
	private String printResultToWebBrowser = Messages.getString("Output.NoResults"); //$NON-NLS-1$

	@Override
	public String getMenu() {
		return "<table><tr>" //$NON-NLS-1$
				+ "<td>" + "Music Search" + "</td>" //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				+ "</tr></table>"; //$NON-NLS-1$
	}

	@Override
	public String getFooter() {
		return "<table><tr>" + "<td>" + Messages.getString("Output.Back") + "</td>" + "</tr></table>";
	}

	@Override
	public String getResult() {
		return Suchbegriff;
	}

	public void musicSearchResults(String searchTerm) {
		Suchbegriff = "\n\nMusicSearch\n\ngesucht wird nach\n" + searchTerm + "\n";
		canvasChanged = true;
		printResultToWebBrowser = "" //$NON-NLS-1$
				+ Messages.getString("Output.SearchResultsFor") + searchTerm + "\"\n"; //$NON-NLS-1$ //$NON-NLS-2$

	}

	@Override
	public String getServerStatus() {

		return null;
	}

	@Override
	public String getCanvas() {
		return MusicSearchImage;
	}

	/**
	 * @return
	 * @uml.property name="canvasChanged"
	 */
	@Override
	public boolean isCanvasChanged() {
		return canvasChanged;
	}

	/**
	 * @param changed
	 * @uml.property name="canvasChanged"
	 */
	@Override
	public void setCanvasChanged(boolean changed) {
		canvasChanged = changed;
	}

	@Override
	public String getResultsToShow() {

		return null;
	}

	@Override
	public String getMapResults() {

		return null;
	}

	@Override
	public String getLatFrom() {

		return null;
	}

	@Override
	public String getLngFrom() {

		return null;
	}

	@Override
	public String getLatTo() {

		return null;
	}

	@Override
	public String getLngTo() {

		return null;
	}

	@Override
	public void writeToConsole() {
		logger.debug("writeToConsole() - {}", Messages.getString("Output.StartMusicSearch")); //$NON-NLS-1$

	}

	@Override
	public Boolean isIncreasedResult() {
		return false;
	}

}
