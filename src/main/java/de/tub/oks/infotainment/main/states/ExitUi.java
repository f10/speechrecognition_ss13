/* 
 * Copyright 2013 Berlin Institute of Technology (TU Berlin), 
 * Faculty IV (CS), Department for Open Communication Systems (OKS).
 * 
 * This file is part of SpeechRecognition.
 * SpeechRecognition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SpeechRecognition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SpeechRecognition.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.tub.oks.infotainment.main.states;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.tub.oks.infotainment.main.UserInterface;
import de.tub.oks.infotainment.main.Messages;

/**
 * Handles UI for Exit status
 */
public class ExitUi implements UserInterface {

	/**
	 * Logger for this class
	 */
	private static final Logger logger = LoggerFactory.getLogger(ExitUi.class);

	private static final String starswImage = "<img src='images/sternsw.png' width='200' height='200' alt='Command Online - Sleeping'>"; //$NON-NLS-1$

	private boolean changeCanvasToWebBrowser = true;

	@Override
	public String getMenu() {
		return "\t"; //$NON-NLS-1$
	}

	@Override
	public String getFooter() {

		return null;
	}

	@Override
	public String getResult() {
		return "\n\n\n" + Messages.getString("ExitUi.GoodBye"); //$NON-NLS-1$ //$NON-NLS-2$
	}

	@Override
	public String getServerStatus() {
		return Messages.getString("Output.ShuttingDownSystem"); //$NON-NLS-1$;
	}

	@Override
	public String getCanvas() {
		return starswImage;
	}

	@Override
	public boolean isCanvasChanged() {
		return changeCanvasToWebBrowser;
	}

	@Override
	public void setCanvasChanged(boolean changed) {
		changeCanvasToWebBrowser = changed;
	}

	@Override
	public String getResultsToShow() {

		return null;
	}

	@Override
	public String getMapResults() {

		return null;
	}

	@Override
	public String getLatFrom() {

		return null;
	}

	@Override
	public String getLngFrom() {

		return null;
	}

	@Override
	public String getLatTo() {

		return null;
	}

	@Override
	public String getLngTo() {

		return null;
	}

	@Override
	public void writeToConsole() {
		logger.debug("writeToConsole() - {}", Messages.getString("ExitUi.GoodBye")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$

	}

	@Override
	public Boolean isIncreasedResult() {
		return false;
	}

}
