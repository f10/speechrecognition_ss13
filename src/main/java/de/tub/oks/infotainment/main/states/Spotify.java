/* 
 * Copyright 2013 Berlin Institute of Technology (TU Berlin), 
 * Faculty IV (CS), Department for Open Communication Systems (OKS).
 * 
 * This file is part of SpeechRecognition.
 * SpeechRecognition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SpeechRecognition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SpeechRecognition.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.tub.oks.infotainment.main.states;

import jahspotify.PlaybackListener;
import jahspotify.SearchResult;
import jahspotify.media.Image;
import jahspotify.media.Link;
import jahspotify.media.LoadableListener;
import jahspotify.media.Track;

import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.tub.oks.infotainment.main.Messages;
import de.tub.oks.infotainment.main.RecognizedStateMachineState;
import de.tub.oks.infotainment.main.RecognizingStateMachine;
import de.tub.oks.infotainment.main.UserInterface;
import de.tub.oks.infotainment.remoteservice.spotify.SpotifyService;

/**
 * @author flo
 * 
 */
public class Spotify implements RecognizedStateMachineState, PlaybackListener, LoadableListener<Image> {

  /**
   * Logger for this class
   */
  private static final Logger logger = LoggerFactory.getLogger(Spotify.class);

  private boolean finished = true;

  private RecognizingStateMachine machine;

  private SpotifyService spotifyService;

  private SpotifyUI spotifyUI;

  private Locale locale;

  private String currentHelpText;

  enum Status {
    DEFAULT, DICTATION
  };

  private Status currentStatus = Status.DEFAULT;

  private String leave = "NO_MATCH";

  private static boolean isMediaPlayerMute = false;
  private static boolean isMediaPlayerPause = false;

  public Spotify(RecognizingStateMachine machine, Locale locale) {
    this.machine = machine;
    this.locale = locale;
    this.spotifyService = new SpotifyService();
    this.spotifyUI = new SpotifyUI();
    spotifyUI.setCurrentPlaylist(spotifyService.getCurrentTrackList());
    spotifyService.initialize();
    SpotifyService.getJahSpotify().addPlaybackListener(this);
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * de.tub.oks.infotainment.main.RecognizedStateMachineState#stateStarted()
   */
  @Override
  public void stateStarted() {

    machine.setGrammar("spotify"); //$NON-NLS-1$
    // spotifyUI.setDefault();

    // currentStatus = Status.START;
    currentHelpText = Messages.getString("Spotify.Welcome"); //$NON-NLS-1$
    machine.say(currentHelpText);

    spotifyUI.startRemote();
    spotifyUI.writeToConsole();

    machine.listen();

  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * de.tub.oks.infotainment.main.RecognizedStateMachineState#stateFinished()
   */
  @Override
  public void stateFinished() {
    finished = true;
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * de.tub.oks.infotainment.main.RecognizedStateMachineState#handleActionWords
   * (java.lang.String)
   */
  @Override
  public void handleActionWords(String words) {
    finished = false;
    logger.debug("handleActionWords(String) - {}", "Handleactionwords " + words);

    if (currentStatus == Status.DEFAULT) {

      logger.debug("handleActionWords(String) - {}", "current status set to default");

      if (words.matches(Messages.getString("Global.recBack"))) { //$NON-NLS-1$
        logger.debug("handleActionWords(String) - current status set to default and said " + words);
        machine.setState(new MainMenu(machine));
        logger.debug("handleActionWords(String) - leaving spotify, going back to main menu");
      }
      else if (words.matches(Messages.getString("Spotify.recSearch")) || words.matches(Messages.getString("Spotify.recFind"))) { //$NON-NLS-1$

        logger.debug("handleActionWords(String) - {}", "setting workingstate to DICTATION");

        currentStatus = Status.DICTATION;
        machine.say(Messages.getString("Spotify.WhatToSearch"));
        // machine.listen();
        machine.listenOnline();
        // there are problems with google on win 7. normally it should listen
        // online here <- copied comment
      }
      else if (words.matches(Messages.getString("Spotify.recPlay"))) { //$NON-NLS-1$

        logger.info("Invoking Spotify service play");

        if (isMediaPlayerMute == true) {
          machine.say(Messages.getString("Spotify.VolumeOff"));
        }
        if (isMediaPlayerPause == true) {
          isMediaPlayerPause = false;
          spotifyService.pausePlaying();
          currentStatus = Status.DEFAULT;
          machine.listen();
        }
        spotifyService.play();
        currentStatus = Status.DEFAULT;
        machine.listen();
      }
      else if (words.matches(Messages.getString("Spotify.recPause")) || words.matches(Messages.getString("Spotify.recStop"))) { //$NON-NLS-1$

        logger.info("Invoking Spotify service pause");

        if (isMediaPlayerPause == true) {
          isMediaPlayerPause = false;
        }
        else {
          isMediaPlayerPause = true;
        }
        spotifyService.pausePlaying();
        currentStatus = Status.DEFAULT;
        machine.listen();
      }
      else if (words.matches(Messages.getString("Spotify.recSkipLast"))) { //$NON-NLS-1$

        logger.info("Invoking Spotify service prev");

        SpotifyService.getMediaPlayer().prev();
        currentStatus = Status.DEFAULT;
        machine.listen();
      }
      else if (words.matches(Messages.getString("Spotify.recClearPlaylist"))) { //$NON-NLS-1$
        logger.info("Invoking Spotify service prev");
        spotifyService.clearPlaylist();
        currentStatus = Status.DEFAULT;
        machine.listen();
      }

      else if (words.matches(Messages.getString("Spotify.recSkipNext"))) { //$NON-NLS-1$
        logger.info("Invoking Spotify service skipTrack");
        spotifyService.skipTrack();
        currentStatus = Status.DEFAULT;
        machine.listen();

      }
      else if (words.matches(Messages.getString("Spotify.recRewind"))) { //$NON-NLS-1$

        logger.info("Invoking Spotify service rewind");
        if (SpotifyService.getMediaPlayer().isPlaying()) {
          SpotifyService.getMediaPlayer().seek(5 * SpotifyService.getMediaPlayer().getDuration() / 100 + SpotifyService.getMediaPlayer().getPosition());
        }
        currentStatus = Status.DEFAULT;
        machine.listen();

      }
      else if (words.matches(Messages.getString("Spotify.recFastForward"))) { //$NON-NLS-1$

        logger.info("Invoking Spotify service fast forward");
        if (SpotifyService.getMediaPlayer().isPlaying()) {
          SpotifyService.getMediaPlayer().seek(5 * SpotifyService.getMediaPlayer().getDuration() / 100 + SpotifyService.getMediaPlayer().getPosition());
        }
        currentStatus = Status.DEFAULT;
        machine.listen();

      }
      else if (words.matches(Messages.getString("Spotify.recVolumeDown"))) { //$NON-NLS-1$

        logger.info("Invoking Spotify service volume Down");

        spotifyService.volumeDown();
        currentStatus = Status.DEFAULT;
        machine.listen();

      }
      else if (words.matches(Messages.getString("Spotify.recVolumeUp"))) { //$NON-NLS-1$

        logger.info("Invoking Spotify service volume up");

        if (isMediaPlayerMute == true) {
          isMediaPlayerMute = false;
        }
        spotifyService.volumeUp();
        currentStatus = Status.DEFAULT;
        machine.listen();

      }

      else if (words.matches(Messages.getString("Spotify.recMute"))) { //$NON-NLS-1$

        logger.info("Invoking Spotify service mute");

        isMediaPlayerMute = true;
        spotifyService.mute();
        currentStatus = Status.DEFAULT;
        machine.listen();

      }
      else {
        // Speech is not valid for this menu point
        machine.say(Messages.getString("Output.DidNotUnderstand") + Messages.getString("Output.PleaseRepeatYourRequest"));
        spotifyUI.poiNotUnderstand();
        currentStatus = Status.DEFAULT;
        machine.listen();
      }
    }
    else if (currentStatus == Status.DICTATION) {
      if (words.matches(Messages.getString("Global.recBack"))) {
        currentStatus = Status.DEFAULT;
        logger.debug("handleActionWords(String) - {}", "Going back to yelp root menu");
        machine.listen();
      }
      else if (words.equals("") || words == null) { //$NON-NLS-1$
        // If onlineSpeechToText did not understand
        leave = "NO_MATCH"; //$NON-NLS-1$

        logger.debug("handleActionWords(String) - {}", Messages.getString("Global.HashlineLong") + leave + Messages.getString("Global.HashlineLong"));
        spotifyUI.poiNotUnderstand();
        machine.say(Messages.getString("Output.DidNotUnderstand") + Messages.getString("Output.PleaseRepeatYourRequest"));
        machine.listenOnline();
      }
      else {
        // it recognized the search term.
        spotifyUI.poiReceiving();
        machine.say(Messages.getString("Poi.AnalysingRecording") + " your search term  " + words);

        SearchResult searchResult = spotifyService.search(words);

        // If it cannot receive response from Yelp API

        if (searchResult == null) { //$NON-NLS-1$
          leave = "SPOTIFY_ERROR_SEARCH_RESULT_NULL";
          logger.debug("handleActionWords(String) - {}", Messages.getString("Global.HashlineLong") + leave + Messages.getString("Global.HashlineLong"));
          spotifyUI.poiNoResults(words);
          machine.say(Messages.getString("Output.NoResultsAvailable") + Messages.getString("Output.PleaseChangeYourRequest"));

        }
        else {
          logger.debug("handleActionWords(String) - {}", searchResult.toString());
          // found tracks

          if (searchResult.getTotalNumTracks() == 0) {
            spotifyUI.poiNoResults(words);
            machine.say(Messages.getString("Output.NoResultsAvailable") + " ZERO tracks found... damnit! "
                    + Messages.getString("Output.PleaseChangeYourRequest"));

            logger.info("handleActionWords(String) - {}", "Total number of tracks: " + searchResult.getTotalNumTracks());

            currentStatus = Status.DEFAULT;
            machine.listen();
          }
          else {
            List<Track> tracks = spotifyService.loadTracksFound();
            spotifyService.addTracksToPlaylist(tracks);
            machine.say(tracks.size() + " tracks added to playlist. Say play to start playing.");
            currentStatus = Status.DEFAULT;
            machine.listen();
            // machine.setState(new YelpListBuilder(machine, obj,
            // Double.toString(sample_lati_current),
            // Double.toString(sample_long_current), words));
          }
        }
      }
    }
    finished = true;
  }

  /*
   * (non-Javadoc)
   * 
   * @see de.tub.oks.infotainment.main.RecognizedStateMachineState#isFinished()
   */
  @Override
  public boolean isFinished() {
    // TODO Auto-generated method stub
    return finished;
  }

  /*
   * (non-Javadoc)
   * 
   * @see de.tub.oks.infotainment.main.RecognizedStateMachineState#getUi()
   */
  @Override
  public UserInterface getUi() {
    // TODO Auto-generated method stub
    return spotifyUI;
  }

  /*
   * (non-Javadoc)
   * 
   * @see de.tub.oks.infotainment.main.RecognizedStateMachineState#getMode()
   */
  @Override
  public String getMode() {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public void trackStarted(Link link) {
    logger.debug("trackStarted() - start"); //$NON-NLS-1$

    spotifyService.loadAllForCurrentTrack();
    spotifyService.getCurrentCover().addLoadableListener(this);
    spotifyUI.showTrack(SpotifyService.getMediaPlayer().getCurrentTrack(), spotifyService.getCurrentAlbum(), spotifyService.getCurrentArtists(),
            spotifyService.getCurrentCover());
    logger.debug("trackStarted(Link link={}) - end", link); //$NON-NLS-1$
  }

  @Override
  public void trackEnded(Link link, boolean forcedEnd) {
    logger.debug("trackEnded() - start"); //$NON-NLS-1$

    // TODO Auto-generated method stub

    logger.debug("trackEnded(Link link={}, boolean forcedEnd={}) - end", link, forcedEnd); //$NON-NLS-1$
  }

  @Override
  public Link nextTrackToPreload() {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public void setAudioFormat(int rate, int channels) {
    // TODO Auto-generated method stub

  }

  @Override
  public int addToBuffer(byte[] buffer) {
    // TODO Auto-generated method stub
    return 0;
  }

  @Override
  public void playTokenLost() {
    // TODO Auto-generated method stub

  }

  @Override
  public void loaded(Image media) {
    logger.debug("loaded() - start"); //$NON-NLS-1$

    if (media == spotifyService.getCurrentCover()) {
      spotifyUI.showTrack(SpotifyService.getMediaPlayer().getCurrentTrack(), spotifyService.getCurrentAlbum(), spotifyService.getCurrentArtists(),
              spotifyService.getCurrentCover());
    }

    logger.debug("loaded(Image media={}) - end", media); //$NON-NLS-1$
  }
}
