/* 
 * Copyright 2013 Berlin Institute of Technology (TU Berlin), 
 * Faculty IV (CS), Department for Open Communication Systems (OKS).
 * 
 * This file is part of SpeechRecognition.
 * SpeechRecognition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SpeechRecognition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SpeechRecognition.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.tub.oks.infotainment.main.states;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.tub.oks.infotainment.main.RecognizedStateMachineState;
import de.tub.oks.infotainment.main.UserInterface;
import de.tub.oks.infotainment.main.Messages;
import de.tub.oks.infotainment.main.RecognizingStateMachine;
import de.tub.oks.infotainment.main.SettingsAndEnvironment;

/**
 * 
 * handles rendering of poi and enables navigation...
 * 
 */
public class PoiListBuilder implements RecognizedStateMachineState {

	/**
	 * Logger for this class
	 */
	private static final Logger logger = LoggerFactory.getLogger(PoiListBuilder.class);

	private String searchTerm = "EMPTY"; //$NON-NLS-1$

	private SettingsAndEnvironment misc = SettingsAndEnvironment.getInstance();

	private boolean finished = true;

	private RecognizingStateMachine machine;

	private String resultList[][];

	private RecognizedStateMachineState lastState;

	private PoiListBuilderUi ui = new PoiListBuilderUi();

	public PoiListBuilder(RecognizingStateMachine machine, String resultList[][], RecognizedStateMachineState lastState, String searchTerm, String latFrom,
			String longFrom) {
		this.machine = machine;
		this.resultList = resultList;
		this.lastState = lastState;
		this.searchTerm = searchTerm;
		ui.setPrintLatFrom(latFrom);
		ui.setPrintLngFrom(longFrom);
	}

	// builds the List to Browser and CMD
	private int showResultList(int linesToShow, int i, int next, String resultList[][]) {
		int resultsToShow = 0;

		if (misc.debug >= 1)
			logger.debug("showResultList(int, int, int, String[][]) - {}", Messages.getString("Global.HashlineLong"));

		// searchTerm wird gesetzt in RemoteHandler.runRemoteSearch();
		String printHelper = Messages.getString("ListBuilder.SearchResults") + searchTerm + "\"\n\n\n"; //$NON-NLS-1$ //$NON-NLS-2$
		String mapResultHelper = ""; //$NON-NLS-1$

		for (i = 0; i < linesToShow; i++) {
			if (i + next < resultList.length) {
				logger.debug("showResultList(int, int, int, String[][]) - {}", i + 1 + ") " + resultList[i + next][4]); //$NON-NLS-1$

				printHelper += i + 1 + ") " + resultList[i + next][4] + "\n"; //$NON-NLS-1$ //$NON-NLS-2$
				mapResultHelper += resultList[i + next][0] + ":" //$NON-NLS-1$
						+ resultList[i + next][1] + "#"; //$NON-NLS-1$
				resultsToShow = i + 1;
			}
			else
				logger.debug("showResultList(int, int, int, String[][]) - {}", ""); // F�lle die Liste auf, wenn der Array
			// leer ist
		}

		// aufrunden der Seitenzahl
		int noOfPages10 = (resultList.length * 10) / linesToShow; // java Trick:
																	// int/int
																	// wird
																	// automatisch
																	// gerundet,
																	// daher *10
		int noOfPagesRounded10 = (resultList.length / linesToShow) * 10;
		int noOfPagesRounded = noOfPagesRounded10 / 10;
		if (noOfPages10 - noOfPagesRounded10 > 0)
			noOfPagesRounded++;

		if (misc.debug >= 1)
			logger.debug(
					"showResultList(int, int, int, String[][]) - {}", Messages.getString("Global.HashlineLong") + "\n" + Messages.getString("Output.Previous") + "||" + Messages.getString("Output.Next") + "||" + Messages.getString("Output.Back") + "||" + Messages.getString("ListBuilder.Page") + (next + 4) / 4 + "/" + noOfPagesRounded + Messages.getString("Global.HashlineLong"));//$NON-NLS-1$

		ui.printResultToWebBrowser = printHelper;
		ui.printMapResultsToWebBrowser = mapResultHelper;
		ui.resultsToShow = resultsToShow;
		ui.buildMapResults(((i + next) / linesToShow), noOfPagesRounded);

		return i;
	}

	private void showChoosenResult(String resultList[][], int zeile) {

		// 0 lat, 1 lng, 2 icon, 3 id, 4 name, 5 reference, 6 types, 7 vicinity

		ui.printLatTo = resultList[zeile][0];
		ui.printLngTo = resultList[zeile][1];
		ui.printResultToWebBrowser = resultList[zeile][4];
		ui.changeCanvasToWebBrowser = true;
		ui.canvasToWebBrowser = "3"; //$NON-NLS-1$
		machine.say(resultList[zeile][4]);

		logger.debug(
				"showChoosenResult(String[][], int) - {}", "\n\n       " + Messages.getString("Global.HashlineLong") + "\n" + Messages.getString("ListBuilder.Latidude") + resultList[zeile][0] + "\n" + Messages.getString("ListBuilder.Longitude") + resultList[zeile][1] + "\n" + Messages.getString("ListBuilder.Icon") + resultList[zeile][2] + "\n" + Messages.getString("ListBuilder.Id") + resultList[zeile][3] + "\n" + Messages.getString("ListBuilder.Name") + resultList[zeile][4] + Messages.getString("ListBuilder.Reference") + resultList[zeile][5] + "\n" + Messages.getString("ListBuilder.Types") + resultList[zeile][6] + "\n" + Messages.getString("ListBuilder.Vicinity") + resultList[zeile][7] + "\n" + Messages.getString("Global.HashlineLong") + "\n\n"); //$NON-NLS-1$ //$NON-NLS-2$
	}

	@Override
	public void stateStarted() {
		machine.setGrammar("resultlist"); //$NON-NLS-1$
		i = showResultList(linesToShow, 0, next, resultList);
		machine.say(Messages.getString("Output.PleaseSelect")); //$NON-NLS-1$
		machine.say(Messages.getString("ListBuilder.SayOneTwoThreeFourForSelect")); //$NON-NLS-1$	
		machine.listen();
	}

	@Override
	public void stateFinished() {
	}

	private int linesToShow = 4;
	private int next = 0;
	private int i = 0;

	@Override
	public void handleActionWords(String words) {
		finished = false;

		i = showResultList(linesToShow, 0, next, resultList);

		if (words.matches(Messages.getString("ListBuilder.recOne"))) { //$NON-NLS-1$
			machine.say(Messages.getString("Output.YouHaveCosenResult") + Messages.getString("Output.One")); //$NON-NLS-1$ //$NON-NLS-2$
			showChoosenResult(resultList, (1 + next - 1));
		}
		else if (words.matches(Messages.getString("ListBuilder.recTwo"))) { //$NON-NLS-1$
			if (ui.resultsToShow >= 2) {
				machine.say(Messages.getString("Output.YouHaveCosenResult") + Messages.getString("Output.Two")); //$NON-NLS-1$ //$NON-NLS-2$
				showChoosenResult(resultList, (2 + next - 1));
			}
			else {
				machine.say(Messages.getString("Output.NotPossible")); //$NON-NLS-1$
			}
		}
		else if (words.matches(Messages.getString("ListBuilder.recThree"))) { //$NON-NLS-1$
			if (ui.resultsToShow >= 3) {
				machine.say(Messages.getString("Output.YouHaveCosenResult") + Messages.getString("Output.Three")); //$NON-NLS-1$ //$NON-NLS-2$
				showChoosenResult(resultList, (3 + next - 1));
			}
			else {
				machine.say(Messages.getString("Output.NotPossible")); //$NON-NLS-1$
			}
		}
		else if (words.matches(Messages.getString("ListBuilder.recFour"))) { //$NON-NLS-1$
			if (ui.resultsToShow == 4) {
				machine.say(Messages.getString("Output.YouHaveCosenResult") + Messages.getString("Output.Four")); //$NON-NLS-1$ //$NON-NLS-2$
				showChoosenResult(resultList, (4 + next - 1));
			}
			else {
				machine.say(Messages.getString("Output.NotPossible")); //$NON-NLS-1$
			}
		}
		else if (words.matches(Messages.getString("ListBuilder.recNext"))) { //$NON-NLS-1$
			if (next <= resultList.length - i - 1) {
				machine.say(Messages.getString("Output.ListingNextResults")); //$NON-NLS-1$
				next = next + linesToShow;
			}
			else
				machine.say(Messages.getString("Output.NoMoreResults")); //$NON-NLS-1$
		}
		else if (words.matches(Messages.getString("ListBuilder.recPrevious"))) { //$NON-NLS-1$ //$NON-NLS-2$
			if (next >= 1) {
				machine.say(Messages.getString("Output.ListingPreviousResults")); //$NON-NLS-1$
				next = next - linesToShow;
			}
			else
				machine.say(Messages.getString("Output.NotPossible")); //$NON-NLS-1$
		}
		else if (words.matches(Messages.getString("Global.recBack"))) { //$NON-NLS-1$
			machine.setState(lastState);
		}
		else { // Did not understand what the User said
			machine.say(Messages.getString("Output.DidNotUnderstand") + Messages.getString("Output.PleaseRepeatYourRequest")); //$NON-NLS-1$ //$NON-NLS-2$
		}
		machine.listen();
		finished = true;
	}

	@Override
	public boolean isFinished() {
		return finished;
	}

	@Override
	public UserInterface getUi() {
		return ui;
	}

	@Override
	public String getMode() {
		return "ListBuilder";
	}

}
