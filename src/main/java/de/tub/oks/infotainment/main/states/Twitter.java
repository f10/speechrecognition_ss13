/* 
 * Copyright 2013 Berlin Institute of Technology (TU Berlin), 
 * Faculty IV (CS), Department for Open Communication Systems (OKS).
 * 
 * This file is part of SpeechRecognition.
 * SpeechRecognition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SpeechRecognition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SpeechRecognition.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.tub.oks.infotainment.main.states;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import de.tub.oks.infotainment.main.RecognizedStateMachineState;
import de.tub.oks.infotainment.main.UserInterface;
import de.tub.oks.infotainment.main.Messages;
import de.tub.oks.infotainment.main.RecognizingStateMachine;
import de.tub.oks.infotainment.main.SettingsAndEnvironment;
import de.tub.oks.infotainment.remoteservice.socialservice.ISocialService;
import de.tub.oks.infotainment.remoteservice.socialservice.SocialServiceFactory;
import de.tub.oks.infotainment.remoteservice.socialservice.SocialServicePost;

/**
 * Handles speech interaction with twitter this state has its own little state
 * machine
 */
public class Twitter implements RecognizedStateMachineState {

	/**
	 * Logger for this class
	 */
	private static final Logger logger = LoggerFactory.getLogger(Twitter.class);

	private boolean finished = true;

	private RecognizingStateMachine machine;

	ISocialService twitter = SocialServiceFactory.getTwitterService();

	private TwitterUi ui = new TwitterUi();

	public Twitter(RecognizingStateMachine machine) {
		this.machine = machine;
		twitter.setRedirectUrl(SettingsAndEnvironment.getInstance().getRedirectUrlTwitter());
	}

	public void setUserAuthetication(String userAuthetication) {
		twitter.setAuthenticationAnswer(userAuthetication);
		stateStarted();
		machine.listen();
	}

	@Override
	public boolean isFinished() {
		return finished;
	}

	@Override
	public void stateStarted() {
		machine.setGrammar("twitter"); //$NON-NLS-1$

		// check if we are already authenticated or need authentication
		if (!twitter.autheticate()) {
			ui.setAuthenticationState(twitter.getAuthenticationLink());
			currentHelpText = Messages.getString("Social.PleaseClickAuth"); //$NON-NLS-1$
			machine.say(currentHelpText);
			currentWorkingState = WorkingState.AUTHENTICATION;
		}
		else {
			ui.setDefault();
			currentWorkingState = WorkingState.DEFAULT;
			currentHelpText = Messages.getString("Social.SayPostAllUpdatesUnreadUpdates"); //$NON-NLS-1$
			machine.say(currentHelpText);
		}
		ui.writeToConsole();
		machine.say(Messages.getString("CD.SayBackMainMenu")); //$NON-NLS-1$
		machine.listen();
	}

	@Override
	public void stateFinished() {
		logger.debug("stateFinished() - {}", Messages.getString("Social.FacebookClosed")); //$NON-NLS-1$
	}

	enum WorkingState {
		DEFAULT, DICTATION, DICTATION_REVIEW, AUTHENTICATION
	}

	WorkingState currentWorkingState = WorkingState.DEFAULT;
	String posting = ""; //$NON-NLS-1$

	/*
	 * text which will be repeat on asking for help
	 */
	String currentHelpText = ""; //$NON-NLS-1$

	@Override
	public void handleActionWords(String words) {
		finished = false;

		boolean handled = false;
		if (currentWorkingState == WorkingState.DICTATION) {
			if (words.toLowerCase().matches(".*" + Messages.getString("Global.recFinishFinish") + ".*")) { //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				if (!words.toLowerCase().matches(Messages.getString("Global.recFinishFinish"))) { //$NON-NLS-1$
					words = words.toLowerCase().substring(0, words.indexOf(Messages.getString("Global.recFinishFinish")) - 1); //$NON-NLS-1$
					posting += " " + words; //$NON-NLS-1$
				}
				currentWorkingState = WorkingState.DICTATION_REVIEW;
				ui.setDicationReview();
				currentHelpText = Messages.getString("Social.YourRecordedPostingIs") //$NON-NLS-1$
						+ posting + Messages.getString("Social.PleaseSayAcceptAbort"); //$NON-NLS-1$
				machine.say(currentHelpText);
				//TODO workaround for some hang ups which occur sometimes after changeing recognition engine
				try {
					Thread.sleep(4000);
				}
				catch (InterruptedException e) {
					logger.error("handleActionWords()", e);
				}
				machine.listen();
			}
			else {
				machine.say(Messages.getString("Social.ContinueRecognizing")); //$NON-NLS-1$
				posting += " " + words; //$NON-NLS-1$
				machine.listenOnline();
			}
			ui.setPosting(posting);
			handled = true;
		}
		else if (currentWorkingState == WorkingState.DICTATION_REVIEW) {
			boolean reviewpassed = false;
			if (words.matches(Messages.getString("Social.recAbort"))) { //$NON-NLS-1$
				machine.say(Messages.getString("Social.PostingAborted")); //$NON-NLS-1$
				ui.setPosting(""); //$NON-NLS-1$
				reviewpassed = true;
			}
			else if (words.matches(Messages.getString("Social.recAccept"))) { //$NON-NLS-1$
				machine.say(Messages.getString("Social.PostingWillbeCommitted")); //$NON-NLS-1$
				twitter.post(posting);
				ui.setPosting(""); //$NON-NLS-1$
				reviewpassed = true;
			}
			if (reviewpassed) {
				currentWorkingState = WorkingState.DEFAULT;
				ui.setDefault();
				machine.say(Messages.getString("Social.WaitingForNextCommand")); //$NON-NLS-1$
				machine.listen();
				handled = true;
			}
		}
		else if (currentWorkingState == WorkingState.DEFAULT) {
			if (words.matches(Messages.getString("Social.recAllUpdate"))) { //$NON-NLS-1$
				machine.say(Messages.getString("Social.ReceivingInformation")); //$NON-NLS-1$
				twitter.setOnlyUnread(false);
				List<SocialServicePost> posts = twitter.getLatestPosts();
				machine.setState(new SocialListBuilder(machine, posts, this));
				handled = true;
			}
			else if (words.matches(Messages.getString("Social.recUnreadUpdate"))) { //$NON-NLS-1$
				machine.say(Messages.getString("Social.ReceivingInformation")); //$NON-NLS-1$
				twitter.setOnlyUnread(true);
				List<SocialServicePost> posts = twitter.getLatestPosts();
				if (posts.size() > 0) {
					machine.setState(new SocialListBuilder(machine, posts, this));
				}
				else {
					machine.say(Messages.getString("Global.NoUpdatesAvailable")); //$NON-NLS-1$
					machine.listen();
				}
				handled = true;
			}
			else if (words.matches(Messages.getString("Social.recPost"))) { //$NON-NLS-1$
				currentWorkingState = WorkingState.DICTATION;
				ui.setDictation();
				posting = ""; //$NON-NLS-1$
				ui.setPosting(posting);
				machine.say(Messages.getString("Social.PleaseDictate")); //$NON-NLS-1$

				machine.listenOnline();
				handled = true;
			}
			else if (words.matches(Messages.getString("Social.recLogout"))) { //$NON-NLS-1$
				twitter.deautheticate();
				stateStarted();
				machine.listen();
				handled = true;
			}
		}
		if (!handled) {
			if (words.matches(Messages.getString("Global.recBack"))) { //$NON-NLS-1$
				machine.setState(new MainMenu(machine));
			}
			else if (words.matches(".*Help.*")) { //$NON-NLS-1$
				machine.say(currentHelpText);
				machine.listen();
			}
			else {
				// Benutzereingabe nicht verstanden
				machine.say(Messages.getString("Output.DidNotUnderstand") //$NON-NLS-1$
						+ Messages.getString("Output.PleaseRepeatYourRequest")); //$NON-NLS-1$
				machine.listen();
			}
		}

		finished = true;
	}

	@Override
	public UserInterface getUi() {
		return ui;
	}

	@Override
	public String getMode() {
		return "Twitter"; //$NON-NLS-1$
	}

}
