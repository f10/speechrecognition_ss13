/* 
 * Copyright 2013 Berlin Institute of Technology (TU Berlin), 
 * Faculty IV (CS), Department for Open Communication Systems (OKS).
 * 
 * This file is part of SpeechRecognition.
 * SpeechRecognition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SpeechRecognition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SpeechRecognition.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.tub.oks.infotainment.main.states;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.tub.oks.infotainment.geo.GeoLocatorFactory;
import de.tub.oks.infotainment.geo.GeoLocator;
import de.tub.oks.infotainment.geo.gps.Coordinate;
import de.tub.oks.infotainment.geo.gps.InitReaderThread;
import de.tub.oks.infotainment.main.RecognizedStateMachineState;
import de.tub.oks.infotainment.main.UserInterface;
import de.tub.oks.infotainment.main.Messages;
import de.tub.oks.infotainment.main.RecognizingStateMachine;
import de.tub.oks.infotainment.main.SettingsAndEnvironment;
import de.tub.oks.infotainment.remoteservice.googleplaces.POIFinder;
import de.tub.oks.infotainment.remoteservice.googleplaces.Parser;
import de.tub.oks.infotainment.remoteservice.googleplaces.ParserPOI;
import de.tub.oks.infotainment.remoteservice.googleplaces.SampleReader;

/**
 * 
 * gives access to google places search
 * 
 */
public class Poi implements RecognizedStateMachineState {

	/**
	 * Logger for this class
	 */
	private static final Logger logger = LoggerFactory.getLogger(Poi.class);

	// Google Maps Settins
	public int searchRadius = 500; // in meters
	public int addToIncreaseRadius = 500; // in meters

	public enum RemoteType {
		all, left, right
	}

	private RemoteType type;

	private enum RemoteStates {
		LeftRight, RemoteSearch, Cancel, Finished
	}

	private RemoteStates currentState;

	private RecognizingStateMachine machine;

	private SettingsAndEnvironment misc = SettingsAndEnvironment.getInstance();

	private ParserPOI parserPOI = new ParserPOI();

	private POIFinder poiFinder = new POIFinder();

	private SampleReader sampleReader = new SampleReader();

	private Parser parser = new Parser(sampleReader);

	private GeoLocator geoLocator;
	private String gpsFile;

	private boolean finished = true;

	private PoiUi ui = new PoiUi();

	public Poi(RecognizingStateMachine machine, RemoteType type, int startRadius, int radiusIncreaseStep) {
		this.machine = machine;
		this.type = type;
		initGps();
		searchRadius = startRadius;
		addToIncreaseRadius = radiusIncreaseStep;
	}

	public Poi(RecognizingStateMachine machine, RemoteType type) {
		this(machine, type, 500, 500);
	}

	private void initGps() {
		geoLocator = GeoLocatorFactory.getGeoLocator();

		gpsFile = System.getProperty("java.io.tmpdir") + "gps.txt"; //$NON-NLS-1$ //$NON-NLS-2$

		if (misc.useGPSStick) {
			Thread t = new Thread(new InitReaderThread(gpsFile));
			logger.debug("initGps() - {}", t.getUncaughtExceptionHandler());
			t.start();
		}
	}

	private StringBuffer current_long = new StringBuffer(""); //$NON-NLS-1$
	private StringBuffer current_lati = new StringBuffer(""); //$NON-NLS-1$
	private String result = Messages.getString("Poi.RemoteServiceOff"); //$NON-NLS-1$
	private String resultList[][] = null;
	private String resultListTemp[][] = null;

	private boolean allowRadiusChange = false; // is true after the first run

	private void getCoordinates(StringBuffer current_long, StringBuffer current_lati) {
		if (misc.useGPSStick == false)
			logger.debug("getCoordinates(StringBuffer, StringBuffer) - {}", Messages.getString("Poi.NoGPSDevice")); //$NON-NLS-1$

		// Get GPS-Coordinates
		if ((misc.useGPSStick == false) && (type == RemoteType.left || type == RemoteType.right)) {
			// Use "Unter den Linden" Sample
			if (misc.debug >= 1)
				logger.debug("getCoordinates(StringBuffer, StringBuffer) - {}", Messages.getString("Poi.DriveOnUnterDenLinden")); //$NON-NLS-1$
			current_long.append(poiFinder.sample_long_current);
			current_lati.append(poiFinder.sample_lati_current);
		}
		else if ((misc.useGPSStick == false) && (type == RemoteType.all)) {
			// Use "FrauenhoferInstitute" Sample
			if (misc.debug >= 1)
				logger.debug("getCoordinates(StringBuffer, StringBuffer) - {}", Messages.getString("Poi.CurrentLocation")); //$NON-NLS-1$
			current_long.append(poiFinder.longtitudeExample);
			current_lati.append(poiFinder.latitudeExample);
		}
		else {
			// Use Data read from GPS-Stick
			Coordinate coordinates = geoLocator.getCoordinates(gpsFile);
			current_long.append(coordinates.getLongtitude());
			current_lati.append(coordinates.getLatitude());
		}
	}

	private String leave = "NO_MATCH"; //$NON-NLS-1$

	// search with external speech api
	private void runRemoteSearch(String words, boolean allowRadiusChange, String current_lati, String current_long) throws Exception {

		if (!allowRadiusChange)
			ui.poiWaiting();

		if ((allowRadiusChange == true) && words.matches(Messages.getString("Poi.recRadius"))) { //$NON-NLS-1$
			// leave = remoteResult = "INCREASE_RADIUS";
			machine.say(Messages.getString("Output.IncreasingSearchRadius"));
			searchRadius = searchRadius + addToIncreaseRadius;
			ui.poiIncreaseRadius(searchRadius);
			logger.debug(
					"runRemoteSearch(String, boolean, String, String) - {}", Messages.getString("Poi.RadiusIncreasedTo") + searchRadius + Messages.getString("Poi.shortMeters")); //$NON-NLS-1$
		}
		else {
			ui.poiAnalysing();
		}

		// �bergebe die erkannten W�rter an den Lister zur Darstellung
		String searchTerm = words;

		if (words.equals("") || words == null) { //$NON-NLS-1$
			// Wenn Google den Suchbegriff nicht verstanden hat
			leave = "NO_MATCH"; //$NON-NLS-1$
			if (misc.debug > 1)
				logger.debug("runRemoteSearch(String, boolean, String, String) - {}",
						Messages.getString("Global.HashlineLong") + leave + Messages.getString("Global.HashlineLong"));
			ui.poiNotUnderstand();
			machine.say(Messages.getString("Output.DidNotUnderstand") + Messages.getString("Output.PleaseRepeatYourRequest"));
		}

		// Der Suchbegriff wurde verstanden
		ui.poiReceiving();
		machine.say(Messages.getString("Poi.AnalysingRecording"));

		String result = poiFinder.getPlaces(words, current_lati, current_long, searchRadius); // sendet den ResponseText an einen
		// weiteren Remote-Dienst

		if ((result.contains("ZERO_RESULTS") //$NON-NLS-1$
				|| result == null || result.equals("")) //$NON-NLS-1$
				&& !leave.equals("NO_MATCH")) { //$NON-NLS-1$
			// Der Suchbegriff brachte beim Remote-Dienst kein Ergebnis
			leave = "ZERO_RESULTS"; //$NON-NLS-1$
			if (misc.debug > 1)
				logger.debug("runRemoteSearch(String, boolean, String, String) - {}",
						Messages.getString("Global.HashlineLong") + leave + Messages.getString("Global.HashlineLong"));
			ui.poiNoResults(searchTerm, searchRadius);
			machine.say(Messages.getString("Output.NoResultsAvailable") + Messages.getString("Output.PleaseChangeYourRequest"));
			allowRadiusChange = true;
		}
		else {
			resultList = parser.arrayCreator(result);
			// Schr�nke die Ergebnisse im Radius ein
			resultList = poiFinder.filterPoiByRadius(resultList, searchRadius, current_lati.toString(), current_long.toString());
			machine.setState(new PoiListBuilder(machine, resultList, new MainMenu(machine), words, current_lati.toString(), current_long.toString()));
			currentState = RemoteStates.Finished;
		}

	}

	@Override
	public void stateStarted() {
		machine.setGrammar("poi"); //$NON-NLS-1$
		getCoordinates(current_long, current_lati);

		// Show the correct source in the Map
		ui.setPrintLatFrom(current_lati.toString());
		ui.setPrintLngFrom(current_long.toString());

		if (misc.poiStaysOffline == false) {
			ui.startRemote();
			machine.say(Messages.getString("Output.StartingRemoteService"));
			if (type == RemoteType.right)
				ui.poiRight();
			else if (type == RemoteType.left)
				ui.poiLeft();
			else
				ui.poi();

			if (misc.debug >= 1) {
				logger.debug("stateStarted() - {}",
						Messages.getString("Global.HashlineLong") + Messages.getString("Global.Hashline") + Messages.getString("Poi.StartRemoteService")
								+ Messages.getString("Global.Hashline") + Messages.getString("Global.HashlineLong"));
			}

			if (!(type == RemoteType.right) && !(type == RemoteType.left)) {
				machine.say(Messages.getString("Output.PlreaseDefineYourPointOfInterest"));
				currentState = RemoteStates.RemoteSearch;
				machine.listenOnline();
			}
			else {
				currentState = RemoteStates.LeftRight;
				try {
					getLeftRightPoi();
				}
				catch (Exception e) {
					// TODO better error handling
					logger.error("stateStarted()", e);
					machine.setState(new MainMenu(machine));
				}
				machine.listen();
			}

		}
		else { // Stay offline, use sample
			logger.debug("stateStarted() - {}", Messages.getString("Poi.BuildListFromTestString")); //$NON-NLS-1$
			logger.debug("stateStarted() - {}", Messages.getString("Poi.IncreaseRadiusToMax")); //$NON-NLS-1$
			result = sampleReader.readSample();
			// if (misc.debug >= 2)
			// System.out.println(result);

			// Baue einen Array aus dem Result
			resultListTemp = parser.arrayCreator(result);

			// Schr�nke die Ergebnisse im Radius ein
			// Hier der Erdumfang, da die Sample ergebnisse aus Australien
			// beinhaltet
			resultListTemp = poiFinder.filterPoiByRadius(resultListTemp, 40000000, current_lati.toString(), //$NON-NLS-1$
					current_long.toString());

			if (resultListTemp.length == 0)
				ui.poiNoResults("", searchRadius); //$NON-NLS-1$

			machine.setState(new PoiListBuilder(machine, resultListTemp, new MainMenu(machine), "Offline Sample", current_lati.toString(), current_long
					.toString()));
		}

	}

	@Override
	public void handleActionWords(String words) {

		finished = false;

		if (words.matches(Messages.getString("Global.recCancel"))) { //$NON-NLS-1$
			ui.poiCanceled();
			machine.say(Messages.getString("Output.CanceledReturningToMainMenu"));
			currentState = RemoteStates.Cancel;
		}

		try {
			switch (currentState) {
			case Cancel:
				machine.setState(new MainMenu(machine));
				break;
			case LeftRight:
				if (checkForRadiusIncrease(words)) {
					getLeftRightPoi();
				}
				break;
			case RemoteSearch:
				// or general
				// Frage den Benutzer nach dem Suchwort
				runRemoteSearch(words, allowRadiusChange, current_lati.toString(), current_long.toString());
				if (currentState != RemoteStates.Finished) {
					machine.listenOnline();
				}
				break;
			case Finished:
				break;
			default:
				logger.error("Ouch no default.");
				break;
			}
		}
		catch (Exception e) {
			logger.error("handleActionWords()", e);
			machine.setState(new MainMenu(machine));
		}

		finished = true;

	}

	@Override
	public void stateFinished() {
	}

	@Override
	public boolean isFinished() {
		return finished;
	}

	private boolean checkForRadiusIncrease(String words) {
		// Increase Radius
		if (allowRadiusChange == true) {
			if ((allowRadiusChange == true) && words.matches(Messages.getString("Poi.recRadius"))) { //$NON-NLS-1$
				// leave = remoteResult = "INCREASE_RADIUS";
				machine.say(Messages.getString("Output.IncreasingSearchRadius"));
				searchRadius = searchRadius + addToIncreaseRadius;
				ui.poiIncreaseRadius(searchRadius);
				logger.debug("checkForRadiusIncrease(String) - {}", Messages.getString("Poi.RadiusIncreasedTo") + searchRadius + "m"); //$NON-NLS-1$

			}
			else {
				ui.poiNotUnderstand();
				machine.say(Messages.getString("Output.DidNotUnderstand") + Messages.getString("Output.PleaseRepeatYourRequest"));
			}
			return true;
		}
		else {
			return false;
		}
	}

	private void getLeftRightPoi() throws Exception {
		String searchTerm = ""; //$NON-NLS-1$

		if (type == RemoteType.right)
			searchTerm = Messages.getString("Poi.Right"); //$NON-NLS-1$
		if (type == RemoteType.left)
			searchTerm = Messages.getString("Poi.Left"); //$NON-NLS-1$

		ui.setPrintLatFrom(current_lati.toString());
		ui.setPrintLngFrom(current_long.toString());

		// Take the Sample if not Know Result
		if (SettingsAndEnvironment.getInstance().poiStaysOffline) {
			//System.out.println(Messages.getString("Poi.ArrayBrokenUseSample")); //$NON-NLS-1$
			result = sampleReader.readSample();
		}
		else {
			// Search PointOfInterest
			result = poiFinder.getPlaces(poiFinder.searchType, current_lati.toString(), current_long.toString(), searchRadius);
		}
		// Baue einen Array aus dem Result
		resultList = parserPOI.createArray(result);

		// schr�nke die Ergebnisse mit der Seite bezogen auf die
		// Fahrtrichtung ein
		resultList = poiFinder.filterPoiByDirection(poiFinder.calcBearingFromFile(gpsFile), resultList, type, current_lati.toString(), current_long.toString());

		// Schr�nke die Ergebnisse im Radius ein
		resultListTemp = poiFinder.filterPoiByRadius(resultList, searchRadius, current_lati.toString(), current_long.toString());

		// ist das gefilterte Ergebnis leer?
		if (resultListTemp.length == 0) {
			machine.say(Messages.getString("Output.NoResultsAvailable") + Messages.getString("Output.PleaseChangeYourRequest"));
			ui.poiNoResults(searchTerm, searchRadius);
			allowRadiusChange = true; // allows Radius Change after the first
										// run
			machine.say(Messages.getString("Poi.SayRadiusToIncreaseSearchRadiusSayCancelForGoingBackToMainMenu"));
			machine.listen();

			return;
		}
		else {
			machine.say(Messages.getString("Poi.GetResults"));
		}

		machine.setState(new PoiListBuilder(machine, resultList, new MainMenu(machine), searchTerm, current_lati.toString(), current_long.toString()));

	}

	@Override
	public UserInterface getUi() {
		return ui;
	}

	@Override
	public String getMode() {
		return "Poi";
	}
}
