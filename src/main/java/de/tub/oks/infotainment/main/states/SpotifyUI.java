/* 
 * Copyright 2013 Berlin Institute of Technology (TU Berlin), 
 * Faculty IV (CS), Department for Open Communication Systems (OKS).
 * 
 * This file is part of SpeechRecognition.
 * SpeechRecognition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SpeechRecognition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SpeechRecognition.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.tub.oks.infotainment.main.states;

import jahspotify.media.Album;
import jahspotify.media.Artist;
import jahspotify.media.Image;
import jahspotify.media.Track;

import java.util.List;
import java.util.Queue;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.tub.oks.infotainment.main.Messages;
import de.tub.oks.infotainment.main.UserInterface;

/**
 * @author flo
 * 
 */
public class SpotifyUI implements UserInterface {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LoggerFactory.getLogger(SpotifyUI.class);

	//	private static final String poiImage = "<img src='images/menu/Yelp.png' width='398' height='253' alt='Yelp'>"; //$NON-NLS-1$
	//	private static final String poiRightImage = "<img src='images/menu/poiright.png' width='398' height='253' alt='POI Right'>"; //$NON-NLS-1$
	//	private static final String poiLeftImage = "<img src='images/menu/poileft.png' width='398' height='253' alt='POI Left'>"; //$NON-NLS-1$
	private static final String poiNoResultImage = "<img src='images/menu/poinoresults.png' width='398' height='253' alt='No Result'>"; //$NON-NLS-1$
	private static final String poiAnalysingImage = "<img src='images/menu/yelpPoi.png' width='398' height='253' alt='Analysing'>"; //$NON-NLS-1$
	//	private static final String poiIncreaseRadius = "<img src='images/menu/poiincreaseradius.png' width='398' height='253' alt='Analysing'>"; //$NON-NLS-1$
	private static final String poiNoMatchImage = "<img src='images/menu/poinomatch.png' width='398' height='253' alt='No Match'>"; //$NON-NLS-1$
	private static final String poiCanceledImage = "<img src='images/menu/poicanceled.png' width='398' height='253' alt='POI Left'>"; //$NON-NLS-1$

	private static final String spotifyLogoImage = "<img src='images/menu/spotify-linux.png' width='156' height='156' alt='spotify penguin'>"; //$NON-NLS-1$

	private boolean canvasChanged;

	// webserver output
	private String printMenuToWebBrowser = ""; //$NON-NLS-1$
	private String printFooterMenuToWebBrowser = ""; //$NON-NLS-1$

	private String printResultToWebBrowser = "......"; //$NON-NLS-1$

	private String canvasToWebBrowser = "";

	private Queue<Track> currentPlaylist;

	public void setCurrentPlaylist(Queue currentPlaylist) {
		this.currentPlaylist = currentPlaylist;
	}

	public void startRemote() {
		canvasChanged = true;
		printMenuToWebBrowser = "\t Search \t Back"; //$NON-NLS-1$
		canvasToWebBrowser = spotifyLogoImage;
		printResultToWebBrowser = Messages.getString("Output.StartSpotify"); //$NON-NLS-1$
		printFooterMenuToWebBrowser = "\t"; //$NON-NLS-1$
	}

	public void poiNoResults(String searchTerm) {
		canvasChanged = true;
		canvasToWebBrowser = poiNoResultImage;
		printResultToWebBrowser = "" //$NON-NLS-1$
				+ Messages.getString("Output.SearchResultsFor") + searchTerm + "\"\n" //$NON-NLS-1$ //$NON-NLS-2$
				+ Messages.getString("Output.NoResultsFound");

	}

	public void poiAnalysing() {
		canvasChanged = true;
		canvasToWebBrowser = poiAnalysingImage;
		printResultToWebBrowser = Messages.getString("Output.AnalysingSearch"); //$NON-NLS-1$
	}

	public void poiNotUnderstand() {
		canvasChanged = true;
		canvasToWebBrowser = poiNoMatchImage;
		printResultToWebBrowser = Messages.getString("Output.SearchRequestNotUnderstand"); //$NON-NLS-1$
	}

	public void poiReceiving() {
		printResultToWebBrowser = Messages.getString("Output.ReservedResults"); //$NON-NLS-1$
	}

	public void poiCanceled() {
		printResultToWebBrowser = Messages.getString("Output.SearchAborted"); //$NON-NLS-1$
		canvasChanged = true;
		canvasToWebBrowser = poiCanceledImage;
	}

	@Override
	public String getMenu() {
		return printMenuToWebBrowser;
	}

	@Override
	public String getFooter() {
		return printFooterMenuToWebBrowser;
	}

	@Override
	public String getResult() {
		return printResultToWebBrowser;
	}

	@Override
	public String getServerStatus() {
		return null;
	}

	@Override
	public String getCanvas() {
		return canvasToWebBrowser;
	}

	/**
	 * @return
	 * @uml.property name="canvasChanged"
	 */
	@Override
	public boolean isCanvasChanged() {
		return canvasChanged;
	}

	/**
	 * @param changed
	 * @uml.property name="canvasChanged"
	 */
	@Override
	public void setCanvasChanged(boolean changed) {
		canvasChanged = changed;
	}

	public void setCanvas(String canvasSetting) {
		canvasChanged = true;
		canvasToWebBrowser = canvasSetting;
	}

	public void setResultsToShow(String result) {
		printResultToWebBrowser = result;
	}

	@Override
	public String getResultsToShow() {
		return printResultToWebBrowser;
	}

	@Override
	public String getMapResults() {
		return null;
	}

	@Override
	public void writeToConsole() {
	}

	@Override
	public Boolean isIncreasedResult() {
		return false;
	}

	@Override
	public String getLatFrom() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getLngFrom() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getLatTo() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getLngTo() {
		// TODO Auto-generated method stub
		return null;
	}

	public void showTrack(Track currentTrack, Album album, List<Artist> artists, Image image) {

		printMenuToWebBrowser = "\t PLAY \t PAUSE \t NEXT \t MUTE"; //$NON-NLS-1$

		StringBuilder canvasStringBuilder = new StringBuilder();
		if (null != image && null != image.getBytes()) {
			canvasStringBuilder.append("<img width='156' height='156' src='data:image/jpeg;base64,");
			canvasStringBuilder.append(Base64.encodeBase64String(image.getBytes()));
			canvasStringBuilder.append("' />");
		}
		else {
			canvasStringBuilder.append(spotifyLogoImage);

		}

		logger.debug("image is [{}] ", (null == image ? "null" : image.toString() + " isLoaded = [" + image.isLoaded() + "]"));

		canvasStringBuilder.append("<br/>");

		canvasStringBuilder.append("<b><font color='#89B700'>");

		if (null != album) {
			canvasStringBuilder.append("Album: " + album.getName());
			canvasStringBuilder.append("<br/>");
		}
		if (null != artists && null != artists.get(0)) {
			canvasStringBuilder.append("Artist: " + artists.get(0).getName());
			canvasStringBuilder.append("<br/>");
		}
		if (null != currentTrack && null != currentTrack.getTitle()) {
			canvasStringBuilder.append("Track: " + currentTrack.getTitle());
		}
		canvasStringBuilder.append("</font></b>");
		canvasToWebBrowser = canvasStringBuilder.toString();
		printResultToWebBrowser = queueToHTML(currentTrack);
		printFooterMenuToWebBrowser = "STOP \t SEARCH \t CLEAR \t BACK"; //$NON-NLS-1$
		canvasChanged = true;

	}

	public String queueToHTML(Track currentTrack) {
		StringBuffer tableStringBuffer = new StringBuffer("<table border='1' bordercolor='#89B700'>");

		boolean trackFound = false;
		int rowLimit = 5;
		for (Track track : currentPlaylist) {

			if (trackFound) {

				tableStringBuffer.append("<tr>");

				tableStringBuffer.append("<td>");
				tableStringBuffer.append(track.getTitle());
				tableStringBuffer.append("</td>");

				tableStringBuffer.append("</tr>");
				if (0 == --rowLimit) {
					break;
				}
			}
			else if (track.getId().getId().equals(currentTrack.getId().getId())) {
				trackFound = true;
				tableStringBuffer.append("<tr bgcolor='#89B700'>");

				tableStringBuffer.append("<td>");
				tableStringBuffer.append(track.getTitle());
				tableStringBuffer.append("</td>");

				tableStringBuffer.append("</tr>");

			}

		}
		tableStringBuffer.append("</table>");

		return tableStringBuffer.toString();
	}
}
