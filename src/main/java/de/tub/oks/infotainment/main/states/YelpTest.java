/* 
 * Copyright 2013 Berlin Institute of Technology (TU Berlin), 
 * Faculty IV (CS), Department for Open Communication Systems (OKS).
 * 
 * This file is part of SpeechRecognition.
 * SpeechRecognition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SpeechRecognition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SpeechRecognition.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.tub.oks.infotainment.main.states;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.junit.Before;

import de.tub.oks.infotainment.main.RecognizedStateMachineState;
import de.tub.oks.infotainment.main.RecognizingStateMachine;
import de.tub.oks.infotainment.servlet.Updater;

import junit.framework.*;

public class YelpTest extends TestCase {

	/**
	 * Logger for this class
	 */
	private static final Logger logger = LoggerFactory.getLogger(YelpTest.class);

	/**
	 * @uml.property name="machine"
	 * @uml.associationEnd
	 */
	private RecognizingStateMachine machine;
	/**
	 * @uml.property name="myYelp"
	 * @uml.associationEnd
	 */
	private Yelp myYelp;
	private YelpListBuilder myYelpListBuilder;

	public YelpTest(String name) {
		super(name);
	}

	@Before
	protected void setUp() {

		RecognizingStateMachine machine = RecognizingStateMachine.getInstance();
		machine.say("Test Case with Yelp started");
		myYelp = new Yelp(machine);
		machine.setState(myYelp);
	}

	public void testStateYelp() {
		RecognizingStateMachine machine = RecognizingStateMachine.getInstance();
		logger.debug("testStateYelp() - {}", "In the test State Yelp");
		//myYelp.handleActionWords("places");
		myYelp.handleActionWords("pizza");

		//Updater updater = new Updater();
		//updater.doGet(request, response);
		//machine.getCurrentState().handleActionWords(".*two.*");
		logger.debug("testStateYelp() - {}", " current state in test state yelp: " + machine.getCurrentState());
		logger.debug("testStateYelp() - {}", " current ui: " + machine.getUi());
		//System.out.println(" resultstoshow: "+machine.getUi().getResultsToShow());
		//i send the resultslist in yelplistbuilder to resultstoshow,what about result ? should check this later.

		//myYelpListBuilder.handleActionWords("one");
		Assert.assertEquals(machine.getCurrentState(), de.tub.oks.infotainment.main.states.YelpListBuilder.class);
	}

	public static Test suite() {
		TestSuite suite = new TestSuite();
		suite.addTest(new YelpTest("isStateYelp") {
			protected void runTest() {
				testStateYelp();
			}
		});
		return suite;
	}

}
