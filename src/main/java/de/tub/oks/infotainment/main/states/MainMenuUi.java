/* 
 * Copyright 2013 Berlin Institute of Technology (TU Berlin), 
 * Faculty IV (CS), Department for Open Communication Systems (OKS).
 * 
 * This file is part of SpeechRecognition.
 * SpeechRecognition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SpeechRecognition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SpeechRecognition.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.tub.oks.infotainment.main.states;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.tub.oks.infotainment.main.UserInterface;
import de.tub.oks.infotainment.main.Messages;

/**
 * Handles UI for main menu
 */
public class MainMenuUi implements UserInterface {

	/**
	 * Logger for this class
	 */
	private static final Logger logger = LoggerFactory.getLogger(MainMenuUi.class);

	private static final String starImage = "<img src='images/stern.png' width='200' height='200' alt='Command Online'>"; //$NON-NLS-1$

	boolean canvasChanged = true;

	@Override
	public String getMenu() {
		return "<table><tr>" //$NON-NLS-1$
				+ "<td>" + Messages.getString("Output.Radio") + "</td>" //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				+ "<td>" + Messages.getString("Output.CD") + "</td>" //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				+ "<td>" + Messages.getString("Output.Search") + "</td>" //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				+ "<td>" + Messages.getString("MainMenuUi.Yelp") + "</td>" //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				+ "<td>" + Messages.getString("Output.LeftRight") + "</td>" //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$;
				+ "<td>" + Messages.getString("MainMenuUi.MusicSearch") + "</td>" + "</tr>" //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
				+ "<td>" + Messages.getString("MainMenuUi.News") + "</td>" + "</tr></table>"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
	}

	@Override
	public String getFooter() {
		return "<table><tr>" + "<td>" + Messages.getString("MainMenuUi.Facebook") + "</td>" + "<td>" //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
				+ Messages.getString("MainMenuUi.Twitter") + "</td>" //$NON-NLS-1$ //$NON-NLS-2$
				+ "<td>" + Messages.getString("Output.Standby") + "</td>" //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				+ "<td>" + Messages.getString("Output.ChangeVoice") + "</td>" //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				+ "<td>" + Messages.getString("Output.Exit") + "</td>" //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$;
				+ "</tr></table>"; //$NON-NLS-1$
	}

	@Override
	public String getResult() {
		return Messages.getString("Output.PleaseSayYourChoice"); //$NON-NLS-1$
	}

	@Override
	public String getServerStatus() {
		return Messages.getString("MainMenuUi.Active"); //$NON-NLS-1$
	}

	@Override
	public String getCanvas() {
		return starImage;
	}

	@Override
	public boolean isCanvasChanged() {
		return canvasChanged;
	}

	@Override
	public void setCanvasChanged(boolean changed) {
		canvasChanged = changed;
	}

	@Override
	public String getResultsToShow() {
		return null;
	}

	@Override
	public String getMapResults() {
		return null;
	}

	@Override
	public String getLatFrom() {
		return null;
	}

	@Override
	public String getLngFrom() {
		return null;
	}

	@Override
	public String getLatTo() {
		return null;
	}

	@Override
	public String getLngTo() {
		return null;
	}

	@Override
	public void writeToConsole() {
		logger.debug(
				"writeToConsole() - {}",
				"\n===============================\n     " + Messages.getString("MainMenuUi.WelcomeToMainMenu") + "   \n===============================\n"
						+ Messages.getString("Output.PossibleSelections") + "\n          " + Messages.getString("MainMenuUi.Radio")
						+ "                \n          " + Messages.getString("MainMenuUi.CD") + "                  \n          "
						+ Messages.getString("MainMenuUi.PoiSearch") + "               \n          " + Messages.getString("MainMenuUi.RightLeft")
						+ "   \n                               \n" + Messages.getString("Output.Properties") + "\n          "
						+ Messages.getString("MainMenuUi.ChangeVoice") + "         \n                               \n          "
						+ Messages.getString("MainMenuUi.Exit") + "                 \n===============================\n===============================\n");
	}

	@Override
	public Boolean isIncreasedResult() {
		return false;
	}
}
