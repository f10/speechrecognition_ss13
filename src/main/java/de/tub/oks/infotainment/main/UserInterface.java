/* 
 * Copyright 2013 Berlin Institute of Technology (TU Berlin), 
 * Faculty IV (CS), Department for Open Communication Systems (OKS).
 * 
 * This file is part of SpeechRecognition.
 * SpeechRecognition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SpeechRecognition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SpeechRecognition.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.tub.oks.infotainment.main;

/**
 * All necessary methods for a web userinterface --> get content for each state
 * in state machine could be improved because of Google Maps dependens -->legacy
 * of old project
 * 
 */
public interface UserInterface {

	//general purpose functions
	public String getMenu();

	public String getFooter();

	public String getResult();

	public String getServerStatus();

	public String getCanvas();

	public boolean isCanvasChanged();

	public void setCanvasChanged(boolean changed);

	public Boolean isIncreasedResult();

	//mostly used for google maps service
	public String getResultsToShow();

	public String getMapResults();

	public String getLatFrom();

	public String getLngFrom();

	public String getLatTo();

	public String getLngTo();

	public void writeToConsole();

}
