/* 
 * Copyright 2013 Berlin Institute of Technology (TU Berlin), 
 * Faculty IV (CS), Department for Open Communication Systems (OKS).
 * 
 * This file is part of SpeechRecognition.
 * SpeechRecognition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SpeechRecognition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SpeechRecognition.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.tub.oks.infotainment.main.states;

import de.tub.oks.infotainment.main.UserInterface;
import de.tub.oks.infotainment.main.Messages;

/**
 * 
 * displays news
 * 
 */
public class NewsListBuilderUi implements UserInterface {

	public int resultsToShow = 2;
	public String printMenuToWebBrowser = ""; //$NON-NLS-1$
	public String printFooterMenuToWebBrowser = ""; //$NON-NLS-1$
	public String printResultToWebBrowser = Messages.getString("Output.NoResults"); //$NON-NLS-1$

	// Chosen Result
	public String printLatTo = ""; //$NON-NLS-1$
	public String printLngTo = ""; //$NON-NLS-1$

	public String canvasToWebBrowser = "";
	public boolean changeCanvasToWebBrowser = true;
	public String printMapResultsToWebBrowser = ""; //$NON-NLS-1$

	public void setHeadMenu(int currentPage, int noOfPagesRounded) {
		printMenuToWebBrowser = Messages.getString("Output.Page") + currentPage + "/" + noOfPagesRounded; //$NON-NLS-1$ //$NON-NLS-2$
	}

	@Override
	public String getMenu() {
		return printMenuToWebBrowser;
	}

	@Override
	public String getFooter() {
		return "<table><tr>" + "<td>" + Messages.getString("Output.Previous") + "</td>" + "<td>" + Messages.getString("Output.Next") + "</td>" + "<td>"
				+ Messages.getString("Output.Back") + "</td>" + "<td>" + Messages.getString("Output.Read") + "</td>" + "</tr></table>";
	}

	@Override
	public String getResult() {
		return printResultToWebBrowser;
	}

	@Override
	public String getServerStatus() {
		return null;
	}

	@Override
	public String getCanvas() {
		return canvasToWebBrowser;
	}

	@Override
	public boolean isCanvasChanged() {
		return changeCanvasToWebBrowser;
	}

	@Override
	public void setCanvasChanged(boolean changed) {
		changeCanvasToWebBrowser = changed;
	}

	@Override
	public String getResultsToShow() {
		return null;
	}

	@Override
	public String getLatFrom() {
		return null;
	}

	@Override
	public String getLngFrom() {
		return null;
	}

	@Override
	public String getLatTo() {
		return printLatTo;
	}

	@Override
	public String getLngTo() {
		return printLngTo;
	}

	@Override
	public void writeToConsole() {
		// TODO Auto-generated method stub

	}

	@Override
	public String getMapResults() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean isIncreasedResult() {
		return true;
	}

}
