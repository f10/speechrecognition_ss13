/* 
 * Copyright 2013 Berlin Institute of Technology (TU Berlin), 
 * Faculty IV (CS), Department for Open Communication Systems (OKS).
 * 
 * This file is part of SpeechRecognition.
 * SpeechRecognition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SpeechRecognition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SpeechRecognition.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.tub.oks.infotainment.main.states;

import de.tub.oks.infotainment.main.UserInterface;
import de.tub.oks.infotainment.main.Messages;

/**
 * handles UI of voice change state
 * 
 */
public class ChangeVoiceUi implements UserInterface {

	private static final String voiceImage = "<img src='images/menu/voice.png' width='398' height='253' alt='Voice Changed'>"; //$NON-NLS-1$

	String voice = ""; //$NON-NLS-1$

	boolean canvasChanged = true;

	@Override
	public String getMenu() {
		return "<table><tr>" //$NON-NLS-1$
				+ "<td>" + Messages.getString("ChangeVoiceUi.ChangeVoice") + "</td>" //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				+ "</tr></table>"; //$NON-NLS-1$
	}

	@Override
	public String getFooter() {
		return "<table><tr>" //$NON-NLS-1$
				+ "<td>" + Messages.getString("ChangeVoiceUi.Back") + "</td>" //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				+ "</tr></table>"; //$NON-NLS-1$
	}

	@Override
	public String getResult() {
		return voice + "\n"; //$NON-NLS-1$
	}

	@Override
	public String getServerStatus() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getCanvas() {
		return voiceImage;
	}

	@Override
	public boolean isCanvasChanged() {
		return canvasChanged;
	}

	@Override
	public void setCanvasChanged(boolean changed) {
		canvasChanged = changed;
	}

	@Override
	public String getResultsToShow() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getMapResults() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getLatFrom() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getLngFrom() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getLatTo() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getLngTo() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void writeToConsole() {
		// TODO Auto-generated method stub

	}

	public String getVoice() {
		return voice;
	}

	public void setResult(String voice) {
		this.voice = voice;
	}

	@Override
	public Boolean isIncreasedResult() {
		return false;
	}
}
