/* 
 * Copyright 2013 Berlin Institute of Technology (TU Berlin), 
 * Faculty IV (CS), Department for Open Communication Systems (OKS).
 * 
 * This file is part of SpeechRecognition.
 * SpeechRecognition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SpeechRecognition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SpeechRecognition.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.tub.oks.infotainment.main.states;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.tub.oks.infotainment.main.UserInterface;
import de.tub.oks.infotainment.main.Messages;

/**
 * handles UI of Bing News state
 * 
 */
public class BingUi implements UserInterface {

	/**
	 * Logger for this class
	 */
	private static final Logger logger = LoggerFactory.getLogger(BingUi.class);

	private static final String bingImage = "<img src='images/menu/bing.png' width='398' height='253' alt='CD-Player'>"; //$NON-NLS-1$

	private boolean changeCanvasToWebBrowser = true;

	String result = ""; //$NON-NLS-1$

	public void setDefault() {
		changeCanvasToWebBrowser = true;
		result = ""; //$NON-NLS-1$
	}

	@Override
	public String getMenu() {
		return "<table><tr>" + "<td>" //$NON-NLS-1$ //$NON-NLS-2$ 
				+ Messages.getString("Bing.AllNews") + "</td>" + "<td>" + Messages.getString("Bing.UnreadNews") + "</td>" + "<td>" + Messages.getString("Bing.search") + "</td>"//$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
				+ "</tr></table>"; //$NON-NLS-1$
	}

	@Override
	public String getFooter() {
		return "<table><tr>" + "<td>" + Messages.getString("BingUi.Help") + "</td>" + "<td>" + Messages.getString("BingUi.Back") //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$
				+ "</td>" + "</tr></table>"; //$NON-NLS-1$ //$NON-NLS-2$
	}

	@Override
	public String getResult() {
		if (!result.isEmpty()) {
			return result;
		}
		else {
			return Messages.getString("BingUi.PleaseMakeASelection"); //$NON-NLS-1$
		}
	}

	public void NewsResults(String searchTerm) {
		result = searchTerm;

	}

	@Override
	public String getServerStatus() {
		return Messages.getString("BingUi.Bing"); //$NON-NLS-1$
	}

	@Override
	public String getCanvas() {
		return bingImage;
	}

	@Override
	public boolean isCanvasChanged() {
		return changeCanvasToWebBrowser;
	}

	@Override
	public void setCanvasChanged(boolean changed) {
		changeCanvasToWebBrowser = changed;
	}

	@Override
	public String getResultsToShow() {
		return null;
	}

	@Override
	public String getMapResults() {
		return null;
	}

	@Override
	public String getLatFrom() {
		return null;
	}

	@Override
	public String getLngFrom() {
		return null;
	}

	@Override
	public String getLatTo() {
		return null;
	}

	@Override
	public String getLngTo() {
		return null;
	}

	@Override
	public void writeToConsole() {
		if (!result.isEmpty()) {
			logger.debug("writeToConsole() - {}", result);
		}
		else {
			logger.debug("writeToConsole() - {}", Messages.getString("BingUi.PleaseMakeASelection")); //$NON-NLS-1$
		}
	}

	@Override
	public Boolean isIncreasedResult() {
		return false;
	}

}
