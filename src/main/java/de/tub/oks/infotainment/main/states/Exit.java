/* 
 * Copyright 2013 Berlin Institute of Technology (TU Berlin), 
 * Faculty IV (CS), Department for Open Communication Systems (OKS).
 * 
 * This file is part of SpeechRecognition.
 * SpeechRecognition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SpeechRecognition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SpeechRecognition.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.tub.oks.infotainment.main.states;

import de.tub.oks.infotainment.main.RecognizedStateMachineState;
import de.tub.oks.infotainment.main.UserInterface;
import de.tub.oks.infotainment.main.Messages;
import de.tub.oks.infotainment.main.RecognizingStateMachine;

/**
 * this state closes the application
 * 
 */
public class Exit implements RecognizedStateMachineState {

	private boolean finished = true;

	private RecognizingStateMachine machine;

	private ExitUi ui = new ExitUi();

	public Exit(RecognizingStateMachine machine) {
		this.machine = machine;
	}

	@Override
	public boolean isFinished() {
		return finished;
	}

	@Override
	public void stateStarted() {
		machine.say(Messages.getString("Output.GoodBye") + " . . . . " + Messages.getString("Output.ShuttingDownIn"));
		ui.writeToConsole();
		try {
			Thread.sleep(6500); // get time voice Feedback to end talking 
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		machine.close();
		System.exit(0); // Force close!
	}

	@Override
	public void stateFinished() {

	}

	@Override
	public void handleActionWords(String words) {
		finished = false;

		finished = true;

	}

	@Override
	public UserInterface getUi() {
		return ui;
	}

	@Override
	public String getMode() {
		return "Exit";
	}

}
