/* 
 * Copyright 2013 Berlin Institute of Technology (TU Berlin), 
 * Faculty IV (CS), Department for Open Communication Systems (OKS).
 * 
 * This file is part of SpeechRecognition.
 * SpeechRecognition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SpeechRecognition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SpeechRecognition.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.tub.oks.infotainment.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import de.tub.oks.infotainment.main.RecognizingStateMachine;
import de.tub.oks.infotainment.main.SettingsAndEnvironment;
import de.tub.oks.infotainment.main.states.Facebook;

/**
 * Handle OAuth callback for Twitter state
 */
public class FacebookOAuth extends HttpServlet {

	private static final long serialVersionUID = 4L;

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		RecognizingStateMachine machine = RecognizingStateMachine.getInstance();

		if (null != machine && machine.getCurrentState().getMode().equals("Facebook")) {
			String fbParam = request.getParameter("code");

			if (fbParam != null && !fbParam.isEmpty()) {
				Facebook fbState = (Facebook) machine.getCurrentState();
				fbState.setUserAuthetication(fbParam);
			}

			response.sendRedirect(SettingsAndEnvironment.getInstance().getRestartUpdateUrl());
		}
	}

}
