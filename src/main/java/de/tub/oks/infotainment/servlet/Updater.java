/* 
 * Copyright 2013 Berlin Institute of Technology (TU Berlin), 
 * Faculty IV (CS), Department for Open Communication Systems (OKS).
 * 
 * This file is part of SpeechRecognition.
 * SpeechRecognition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SpeechRecognition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SpeechRecognition.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.tub.oks.infotainment.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.JsonObject;

import de.tub.oks.infotainment.main.UserInterface;
import de.tub.oks.infotainment.main.RecognizingStateMachine;
import de.tub.oks.infotainment.main.SettingsAndEnvironment;

/**
 * servlet is used for updating Web-UI after application is started
 * 
 */
public class Updater extends HttpServlet {

	private static final long serialVersionUID = 2L;

	RecognizingStateMachine machine;

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		machine = RecognizingStateMachine.getInstance();
		if (null != machine) {

			JsonObject jsonFromServerToClient = new JsonObject();

			jsonFromServerToClient = refreshUiToJson(jsonFromServerToClient);

			if (!jsonFromServerToClient.has("menu")) {
				// TODO did not work
				response.sendRedirect(SettingsAndEnvironment.getInstance().getStartUrl());
			}
			else {

				response.setContentType("text/plain");
				response.setHeader("Cache-Control", "no-cache");
				response.getWriter().write(jsonFromServerToClient.toString());
			}
		}

	}

	/**
	 * generate json from backend logic for web frontend
	 * 
	 * @param jsonFromServerToClient
	 * @return
	 */
	private JsonObject refreshUiToJson(JsonObject jsonFromServerToClient) {
		UserInterface ui = machine.getUi();
		if (ui != null) {
			// Menu
			jsonFromServerToClient.addProperty("menu", toHtml(ui.getMenu()));
			jsonFromServerToClient.addProperty("footermenu", toHtml(ui.getFooter()));
			jsonFromServerToClient.addProperty("result", toHtml(ui.getResult()));
			jsonFromServerToClient.addProperty("server_status", toHtml(machine.getUi().getServerStatus()));
			jsonFromServerToClient.addProperty("canvas", toHtml(ui.getCanvas()));
			//System.out.println("canvas: " + ui.getCanvas().toString());
			jsonFromServerToClient.addProperty("changeCanvas", toHtml(String.valueOf(ui.isCanvasChanged())));
			jsonFromServerToClient.addProperty("mode", toHtml(String.valueOf(machine.getCurrentState().getMode())));

			jsonFromServerToClient.addProperty("increasedResult", toHtml(String.valueOf(ui.isIncreasedResult())));

			// GoogleMaps
			jsonFromServerToClient.addProperty("resultsToShow", toHtml(ui.getResultsToShow()));
			jsonFromServerToClient.addProperty("mapResults", toHtml(ui.getMapResults()));

			// You are Here
			jsonFromServerToClient.addProperty("latFrom", toHtml(ui.getLatFrom()));
			jsonFromServerToClient.addProperty("lngFrom", toHtml(ui.getLngFrom()));

			// You are Here
			jsonFromServerToClient.addProperty("latTo", toHtml(ui.getLatTo()));
			jsonFromServerToClient.addProperty("lngTo", toHtml(ui.getLngTo()));

			ui.setCanvasChanged(false);
		}

		return jsonFromServerToClient;
	}

	// Console to HTML Correction
	private String toHtml(String toConvert) {
		if (null == toConvert) {
			return "&nbsp;";
		}
		toConvert = toConvert
				.replaceAll("\n", "<br/>")
				.replaceAll("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;")
				// .replaceAll("'", "&apos;")
				// .replaceAll("&", "&amp;")

				// Deutsche Sonderzeichen
				.replaceAll("�", "&ouml;").replaceAll("�", "&Ouml;").replaceAll("�", "&auml;").replaceAll("�", "&auml;").replaceAll("�", "&uuml;")
				.replaceAll("�", "&uuml;").replaceAll("�", "&szlig;")

				// Franz�sische Sonderzeichen
				.replaceAll("�", "&aacute;").replaceAll("�", "&eacute;").replaceAll("�", "&agrave;").replaceAll("�", "&egrave;").replaceAll("�", "&acirc;")
				.replaceAll("�", "&ecirc;")

		;
		return toConvert;
	}

}
