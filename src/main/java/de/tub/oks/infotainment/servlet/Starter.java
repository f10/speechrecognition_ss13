/* 
 * Copyright 2013 Berlin Institute of Technology (TU Berlin), 
 * Faculty IV (CS), Department for Open Communication Systems (OKS).
 * 
 * This file is part of SpeechRecognition.
 * SpeechRecognition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SpeechRecognition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SpeechRecognition.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.tub.oks.infotainment.servlet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.tub.oks.infotainment.main.RecognizingStateMachine;
import de.tub.oks.infotainment.main.SettingsAndEnvironment;
import de.tub.oks.infotainment.main.states.Bing;
import de.tub.oks.infotainment.main.states.StartUp;
import de.tub.oks.infotainment.main.states.Yelp;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Initial servlet for starting the application
 */
public class Starter extends HttpServlet {

	/**
	 * Logger for this class
	 */
	private static final Logger logger = LoggerFactory.getLogger(Starter.class);

	private static final long serialVersionUID = 1L;

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		logger.debug("start");

		String startParam = request.getParameter("start");

		if (null != startParam) {

			if (startParam.equals("start")) {
				//synchronized (this) {
				RecognizingStateMachine machine = RecognizingStateMachine.getInstance();
				machine.setState(new StartUp(machine));
				if (SettingsAndEnvironment.getInstance().debug >= 1)
					logger.info("doGet(HttpServletRequest, HttpServletResponse) - {}", "I'm starting SpeechRecoApp now.");
				//}
			}
		}

		logger.debug("end");
	}
}
