/* 
 * Copyright 2013 Berlin Institute of Technology (TU Berlin), 
 * Faculty IV (CS), Department for Open Communication Systems (OKS).
 * 
 * This file is part of SpeechRecognition.
 * SpeechRecognition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SpeechRecognition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SpeechRecognition.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.tub.oks.infotainment.remoteservice;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TreeMap;

/**
 * General useful functions for remote services
 * 
 */
public class RemoteFunctions {

	/**
	 * Parses the query string of an URL into a Key-Value-Map
	 * 
	 * @param query
	 *            string of an URL everything after &
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public static Map<String, String> parseQueryString(String query) throws UnsupportedEncodingException {
		final Map<String, String> qps = new TreeMap<String, String>();
		final StringTokenizer pairs = new StringTokenizer(query, "&");
		while (pairs.hasMoreTokens()) {
			final String pair = pairs.nextToken();
			final StringTokenizer parts = new StringTokenizer(pair, "=");
			final String name = URLDecoder.decode(parts.nextToken(), "ISO-8859-1");
			final String value = URLDecoder.decode(parts.nextToken(), "ISO-8859-1");
			qps.put(name, value);
		}
		return qps;
	}
}
