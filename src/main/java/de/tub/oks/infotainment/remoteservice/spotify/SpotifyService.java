/* 
 * Copyright 2013 Berlin Institute of Technology (TU Berlin), 
 * Faculty IV (CS), Department for Open Communication Systems (OKS).
 * 
 * This file is part of SpeechRecognition.
 * SpeechRecognition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SpeechRecognition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SpeechRecognition.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.tub.oks.infotainment.remoteservice.spotify;

import jahspotify.AbstractConnectionListener;
import jahspotify.JahSpotify;
import jahspotify.Query;
import jahspotify.Search;
import jahspotify.SearchResult;
import jahspotify.media.Album;
import jahspotify.media.Artist;
import jahspotify.media.Image;
import jahspotify.media.Link;
import jahspotify.media.Track;
import jahspotify.services.JahSpotifyService;
import jahspotify.services.MediaHelper;
import jahspotify.services.MediaPlayer;
import jahspotify.services.SearchEngine;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * The Class SpotifyService is a convenience wrapper for the class JahSpotify and
 * MediaPlayer from libjahspotify, and provides methods for their use.
 * For libjahspotify API see {@link https://github.com/nvdweem/libjahspotify}
 */
public class SpotifyService extends AbstractConnectionListener {

  /** The spotify tmp dir. */
  public static String SPOTIFY_TMP_DIR = System.getProperty("java.io.tmpdir");
  
  /** Logger for this class. */
  private static final Logger logger = LoggerFactory.getLogger(SpotifyService.class);

  /** The jah spotify. */
  private static JahSpotify jahSpotify;
  
  /** The media player. */
  private static MediaPlayer mediaPlayer;

  /** The last search result. */
  private SearchResult lastSearchResult;
  
  /** The play list queue. */
  private Queue<Link> playListQueue = new LinkedList<Link>();
  
  /** The current track list. */
  private Queue<Track> currentTrackList = new LinkedList<Track>();
  
  /** The volume level before muting. */
  private int volumeBeforeMute;

  /** The current album. */
  private Album currentAlbum;
  
  /** The current artists. */
  private List<Artist> currentArtists;
  
  /** The current cover. */
  private Image currentCover;

  /**
   * Instantiates a new spotify service.
   * After instantiation the {@link #initialize()}
   * method has to be called before using this class.
   */
  //@TODO move initialize call into constructor
  public SpotifyService() {
  }

  /**
   * Initialize the JahSpotify/-Service singleton.
   */
  public void initialize() {
    JahSpotifyService.initialize(new File(SPOTIFY_TMP_DIR));
    jahSpotify = JahSpotifyService.getInstance().getJahSpotify();
    jahSpotify.addConnectionListener(this);
  }

  /**
   * Login. Called by {@link #initialized(boolean)} listener method.
   *
   * @param username the username
   * @param password the password
   */
  public void login(String username, String password) {
    // When JahSpotify is initialized, we can attempt to login.

    jahSpotify.login(username, password, null, false);

  }

  /**
   * Initialized listener method is invoked when the 
   * JahSpotify/-Service is initialized. It instantiates
   * the MediaPlayer singleton and calls {@link #login(String, String)}.
   * 
   * @param initialized boolen indicating if JahSpotifyServie was initialized.
   */
  @Override
  public void initialized(boolean initialized) {
    logger.debug("initialized(boolean initialize[{}]) - start", initialized); //$NON-NLS-1$
    if (initialized) {
      // Add the queue, and play the tracks.
      mediaPlayer = MediaPlayer.getInstance();
      mediaPlayer.addQueue(playListQueue);
      volumeBeforeMute = mediaPlayer.getVolume();
      login("3t-ten", "oks-2013");
    }
    logger.debug("initialized(boolean initialized={}) - end", initialized); //$NON-NLS-1$
  }

  /**
   * LoggedIn listener method invoked when async login
   * returns.
   *
   * @param success boolean indicating if login was successful.
   */
  @Override
  public void loggedIn(boolean success) {
    logger.debug("loggedIn() - start"); //$NON-NLS-1$

    if (success)
      logger.info("{}", "Logged in! You can now use all features"); //$NON-NLS-1$ //$NON-NLS-2$
    else
      logger.info("{}", "Could not log you in!");
    logger.debug("loggedIn(boolean success={}) - end", success); //$NON-NLS-1$
  }

  /**
   * Logged in.
   *
   * @return true, if logged into spotify service.
   */
  public boolean loggedIn() {
    return jahSpotify.isLoggedIn();
  }

  /**
   * Connected listener method. Invoked 
   * when JahSpotify is connected. 
   * Current implementation just prints out start and
   * end message if debug level is enabled.
   */
  @Override
  public void connected() {
    logger.debug("connected() - start"); //$NON-NLS-1$

    logger.debug("connected() - end"); //$NON-NLS-1$
  }

  /**
   * Disconnected listener method. Invoked 
   * when JahSpotify/-Service was disconnected. 
   * Current implementation just prints out start and
   * end message if debug level is enabled.
   */
  @Override
  public void disconnected() {
    logger.debug("disconnected() - start"); //$NON-NLS-1$

    logger.debug("disconnected() - end"); //$NON-NLS-1$
  }

  /**
   * LoggedOut listener method. Invoked 
   * when logged out by JahSpotify/-Service. 
   * Current implementation just prints out start and
   * end message if debug level is enabled.
   */
  @Override
  public void loggedOut() {
    logger.debug("loggedOut() - start"); //$NON-NLS-1$

    // TODO Auto-generated method stub

    logger.debug("loggedOut() - end"); //$NON-NLS-1$
  }

  /**
   * Perform a search in spotify for a given search term.
   * This can be anything, i.e. a genre, an artist, 
   * a track title or an album title. 
   *
   * @param searchTerm the search term as a string.
   * @return the search result as {@link SearchResult}
   */
  public SearchResult search(String searchTerm) {

    Query query = Query.token(searchTerm);
    Search search = new Search(query);
    lastSearchResult = SearchEngine.getInstance().search(search);

    if (null != lastSearchResult) {
      MediaHelper.waitFor(lastSearchResult, 10);
    }
    return lastSearchResult;
  }

  /**
   * Load an {@link Album} for a given track.
   *
   * @param track the track
   * @return the album
   */
  public Album loadAlbum(Track track) {

    Album album = jahSpotify.readAlbum(track.getAlbum());

    MediaHelper.waitFor(album, 30);
    return album;
  }

  /**
   * Load the {@link Artist}s for a given track
   *
   * @param track the track
   * @return the list of artists found
   */
  public List<Artist> loadArtist(Track track) {

    List<Artist> artists = new ArrayList<Artist>();
    for (Link artistLink : track.getArtists()) {
      Artist artist = jahSpotify.readArtist(artistLink);
      MediaHelper.waitFor(artist, 30);
      artists.add(artist);
    }
    return artists;
  }

  /**
   * Load album cover as {@link Image} for a given {@link Album}.
   *
   * @param album the album
   * @return the image
   */
  public Image loadCover(Album album) {
    Image cover = jahSpotify.readImage(album.getCover());

    // MediaHelper.waitFor(cover, 5);
    return cover;
  }

  /**
   * Loads/creates a list of {@link Album}s via the
   * last {@link SearchResult}.
   *
   * @return the list
   */
  public List<Album> loadAlbumsFound() {

    ArrayList<Album> albums = new ArrayList<Album>();

    for (Link albumLink : lastSearchResult.getAlbumsFound()) {
      Album album = jahSpotify.readAlbum(albumLink);
      logger.info("albumLink: id[{}]", albumLink.getId());

      albums.add(album);
    }

    MediaHelper.waitFor(albums, 30);
    return albums;
  }

  /**
   * Load/create list of {@link Track}s via the last
   * {@link SearchResult}.
   *
   * @return the list of tracks found
   */
  public List<Track> loadTracksFound() {
    List<Track> tracklist = loadTracks(lastSearchResult.getTracksFound());
    currentTrackList.addAll(tracklist);
    return tracklist;
  }

  /**
   * Load/create list of  {@link Track}s for a
   * given list of Track {@link Link}s.
   *
   * @param trackLinks the track links
   * @return the list
   */
  public List<Track> loadTracks(List<Link> trackLinks) {
    // Get tracks and let them load.
    ArrayList<Track> tracks = new ArrayList<Track>();

    for (Link trackLink : trackLinks) {
      Track track = jahSpotify.readTrack(trackLink);
      logger.info("trackLink: id[{}]", trackLink.getId());

      tracks.add(track);
    }

    MediaHelper.waitFor(tracks, 30);
    return tracks;

  }

  /**
   * Load/create a list of {@link Track}s for a 
   * given {@link Album}.
   *
   * @param album the given {@link Album}
   * @return the list of {@link Track}s found
   */
  public List<Track> loadTracksForAlbum(Album album) {

    List<Track> trackList = loadTracks(album.getTracks());

    return trackList;
  }

  /**
   * Adds the list of {@link Track}s to 
   * playlist queue.
   *
   * @param tracks the tracks
   */
  public void addTracksToPlaylist(List<Track> tracks) {

    for (Track track : tracks)
      playListQueue.add(track.getId());

    // Add the queue, and play the tracks.
    // mediaPlayer.addQueue(playListQueue);
  }

  /**
   * Clears the current playlist queue.
   */
  public void clearPlaylist() {
    // mediaPlayer.removeQueue(playListQueue);
    playListQueue.clear();
    currentTrackList.clear();
  }

  /**
   * Play the current {@link MediaPlayer}'s queue.
   */
  public void play() {
    if (!mediaPlayer.isPlaying()) {
      mediaPlayer.play();
    }
  }

  /**
   * Skip the current track.
   */
  public void skipTrack() {
    if (mediaPlayer.isPlaying())
      mediaPlayer.skip();
  }

  /**
   * Pause playing of current track.
   */
  public void pausePlaying() {
    try {
      mediaPlayer.pause();
    }// TODO handle this better sometimes audio is null;
    catch (NullPointerException nullPointerException) {
      logger.warn("pausePlaying() - exception ignored", nullPointerException);

    }

  }

  /**
   * Mute the {@link MediaPlayer}.
   */
  public void mute() {

    if (mediaPlayer.getVolume() != 0) {
      volumeBeforeMute = mediaPlayer.getVolume();
      mediaPlayer.setVolume(0);

    }
    else {
      mediaPlayer.setVolume(volumeBeforeMute);
    }
  }

  /**
   * Decrease the current {@link MediaPlayer}'s volume level
   * (currently by a value of 100).
   */
  public void volumeDown() {
    try {
      SpotifyService.getMediaPlayer().setVolume(SpotifyService.getMediaPlayer().getVolume() - 100);
    }
    catch (IllegalArgumentException illegalArgumentException) {
      logger.warn("volumeDown() - exception ignored", illegalArgumentException); //$NON-NLS-1$

    }
  }

  /**
   * Increase the {@link MediaPlayer}'s volume level
   * (currently by a value of 100).
   */
  public void volumeUp() {
    try {
      SpotifyService.getMediaPlayer().setVolume(SpotifyService.getMediaPlayer().getVolume() + 100);
    }
    catch (IllegalArgumentException illegalArgumentException) {
      logger.warn("volumeDown() - exception ignored", illegalArgumentException); //$NON-NLS-1$

    }
  }

  /**
   * Returns the {@link JahSpotify} singleton
   * used by this service.
   *
   * @return the {@link JahSpotify} singleton
   */
  public static JahSpotify getJahSpotify() {
    return jahSpotify;
  }

  /**
   * Returns the {@link MediaPlayer} singleton 
   * used by this service.
   *
   * @return the {@link MediaPlayer} singleton
   */
  public static MediaPlayer getMediaPlayer() {
    return mediaPlayer;
  }

  /**
   * Gets the current {@link Album}.
   *
   * @return the current album
   */
  public Album getCurrentAlbum() {
    return currentAlbum;
  }

  /**
   * Gets the current list of {@link Artist}s.
   *
   * @return the current artists
   */
  public List<Artist> getCurrentArtists() {
    return currentArtists;
  }

  /**
   * Gets the current {@link Album}'s cover as
   * {@link Image}.
   *
   * @return the current cover
   */
  public Image getCurrentCover() {
    return currentCover;
  }

  /**
   * Load the current {@link Album}, {@link Artist}
   * and album cover as {@link Image} for the current track.
   */
  public void loadAllForCurrentTrack() {
    currentAlbum = loadAlbum(mediaPlayer.getCurrentTrack());
    currentArtists = loadArtist(mediaPlayer.getCurrentTrack());
    currentCover = loadCover(currentAlbum);
  }

  /**
   * Gets the play list {@link Queue}.
   *
   * @return the play list queue
   */
  public Queue<Link> getPlayListQueue() {
    return playListQueue;
  }

  /**
   * Gets the current track list as
   * {@link Queue} of {@link Track}s.
   *
   * @return the current track list
   */
  public Queue<Track> getCurrentTrackList() {
    return currentTrackList;
  }

}