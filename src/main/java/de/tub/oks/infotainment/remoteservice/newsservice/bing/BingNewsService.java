/* 
 * Copyright 2013 Berlin Institute of Technology (TU Berlin), 
 * Faculty IV (CS), Department for Open Communication Systems (OKS).
 * 
 * This file is part of SpeechRecognition.
 * SpeechRecognition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SpeechRecognition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SpeechRecognition.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.tub.oks.infotainment.remoteservice.newsservice.bing;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import com.google.code.bing.search.client.BingSearchClient;
import com.google.code.bing.search.client.BingSearchClient.SearchRequestBuilder;
import com.google.code.bing.search.client.BingSearchServiceClientFactory;
import com.google.code.bing.search.schema.AdultOption;
import com.google.code.bing.search.schema.SearchOption;
import com.google.code.bing.search.schema.SearchResponse;
import com.google.code.bing.search.schema.SourceType;
import com.google.code.bing.search.schema.news.NewsResponse;
import com.google.code.bing.search.schema.news.NewsResult;

import de.tub.oks.infotainment.remoteservice.newsservice.News;
import de.tub.oks.infotainment.remoteservice.newsservice.NewsCategory;
import de.tub.oks.infotainment.remoteservice.newsservice.NewsEntry;

/**
 * news implementation which gets news from bing-news
 * 
 */
public class BingNewsService implements News {

	/**
	 * Logger for this class
	 */
	private static final Logger logger = LoggerFactory.getLogger(BingNewsService.class);

	//private static final String APP_ID = "1B052822B0ED531BE20144EAE9C20BDC51C7B4DB";
	private static final String APP_ID = "XN4Amstzg3gdXYTC2r1G8ja58JkYZlcDameO0DYKz24";

	//The default value for News.Count is 10, the minimum value is 1, and the maximum value is 15. Any value outside this range returns an error.
	private static final long REQUESTED_NEWS = 15L;

	private BingSearchServiceClientFactory factory;
	private BingSearchClient client;

	//the last know entries are stored for every category
	private HashMap<String, NewsEntry> lastKnownEntries = new HashMap<String, NewsEntry>();

	private boolean onlyUnread = false;

	private Locale locale;

	public BingNewsService(Locale locale) {
		this.locale = locale;
		factory = BingSearchServiceClientFactory.newInstance();
		client = factory.createBingSearchClient();
	}

	SearchRequestBuilder createRequest(String category) {
		SearchRequestBuilder builder = client.newSearchRequestBuilder();
		builder.withAppId(APP_ID);
		builder.withSourceType(SourceType.NEWS);
		builder.withVersion("2.0");
		builder.withMarket(locale.getLanguage() + "-" + locale.getCountry());
		builder.withAdultOption(AdultOption.OFF);
		builder.withSearchOption(SearchOption.ENABLE_HIGHLIGHTING);
//		builder.withNewsRequestCategory(category);
		builder.withQuery("sport");//default is local top news
		builder.withNewsRequestOffset(0L);
		builder.withNewsRequestCount(REQUESTED_NEWS);
		return builder;
	}

	@Override
	public List<NewsEntry> getNews(NewsCategory category) {

		NewsEntry lastKnownEntry = null;

		List<NewsEntry> news = new ArrayList<NewsEntry>();
		String internalCategory = ((BingNewsCategory) category).getInternalName();

		lastKnownEntry = lastKnownEntries.get(internalCategory);

		SearchRequestBuilder builder = createRequest(internalCategory);
		SearchResponse response = client.search(builder.getResult());

		NewsResponse newsResponse = response.getNews();
		if (newsResponse != null)
			for (NewsResult result : newsResponse.getResults()) {
				NewsEntry newEntry = new NewsEntry(result.getTitle(), result.getSnippet());
				if (onlyUnread && lastKnownEntry != null && lastKnownEntry.equals(newEntry)) {
					break;
				}
				news.add(newEntry);
			}
		else {
			logger.debug("getNews(NewsCategory) - {}", "No news received");
		}
		if (news.size() > 0) {
			lastKnownEntries.put(internalCategory, news.get(0));
		}
		return news;
	}

	@Override
	public List<NewsCategory> getCategories() {
		List<NewsCategory> categories = new ArrayList<NewsCategory>();

		categories.add(new BingNewsCategory("", "Top"));
		categories.add(new BingNewsCategory("rt_Business ", "Business"));
		categories.add(new BingNewsCategory("rt_Entertainment ", "Entertainment"));
		categories.add(new BingNewsCategory("rt_Health", "Health"));
		categories.add(new BingNewsCategory("rt_Politics", "Politics"));
		categories.add(new BingNewsCategory("rt_Sports", "Sports"));
		categories.add(new BingNewsCategory("rt_US", "US"));
		categories.add(new BingNewsCategory("rt_World", "World"));
		categories.add(new BingNewsCategory("rt_ScienceAndTechnology", "Science And Technology"));

		return categories;
	}

	@Override
	public void setOnlyUnread(boolean returnUnread) {
		onlyUnread = returnUnread;
	}

	@Override
	public boolean isOnlyUnread() {
		return onlyUnread;
	}

	/**
	 * example
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		BingNewsService service = new BingNewsService(Locale.GERMANY);
		List<NewsCategory> categories = service.getCategories();

		List<NewsEntry> news = service.getNews(categories.get(0));

		for (NewsEntry entry : news) {
			logger.debug("main(String[]) - {}", entry.getHeadline());
			logger.debug("main(String[]) - {}", entry.getContent());
		}
	}

}
