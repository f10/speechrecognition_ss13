/* 
 * Copyright 2013 Berlin Institute of Technology (TU Berlin), 
 * Faculty IV (CS), Department for Open Communication Systems (OKS).
 * 
 * This file is part of SpeechRecognition.
 * SpeechRecognition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SpeechRecognition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SpeechRecognition.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.tub.oks.infotainment.remoteservice.googleplaces;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.regex.Pattern;

public class ParserPOI {

	/**
	 * Logger for this class
	 */
	private static final Logger logger = LoggerFactory.getLogger(ParserPOI.class);

	// Creates a resultArray[][x] x= {0 lat, 1 lng, 2 icon, 3 id, 4 name, 5
	// reference, 6 types, 7 vicinity}
	public String[][] createArray(String result) {
		// Parse, Parse
		int i = 0;
		int j = 0;
		String resultArray[][] = null;
		String resultLines[] = null;

		// Summarize the Result
		String x[] = Pattern.compile("\"results\" : ").split(result);
		x[1] = x[1].substring(8, x[1].length() - 32);

		// one result contains fields separated by "},"
		// but the results are also separated by "},)
		// so separate everything by "},"
		String y[] = Pattern.compile("},").split(x[1]);

		i = 0;
		j = 0;
		ArrayList<String> resultLineList = new ArrayList<String>();

		int viewportCount = 0;

		for (i = 0; i < y.length; i = i + 2) {
			if (y[j + 1].contains("\"viewport\" :")) {
				resultLineList.add(j, y[i] + "},\n   {" + y[i + 1] + "},\n   {" + y[i + 2] + "},\n   {" + y[i + 3] + "  },");
				i = i + 2;
				viewportCount++;
			}
			else
				resultLineList.add(j, y[i] + "},\n   {" + y[i + 1] + "  },");
			j++;
		}

		// System.out.println("L�nge :" + resultLineList.size());
		// System.out.println("viewportCount :" + viewportCount);

		i = 0;
		j = 0;
		resultLines = new String[resultLineList.size() - viewportCount - 2];
		for (i = 0; i < resultLines.length; i++) {
			if (resultLineList.get(i).contains("viewport"))
				i++;
			resultLines[j] = resultLineList.get(i);
			// System.out.println("Result " +j+":"+resultLines[j]);
			j++;
		}

		// Result type is know, create an fill the resultArray
		resultArray = new String[resultLines.length - 1][8];
		resultArray = createResult(resultLines);

		// Print the final result (for debug)
		/**/
		i = 0;
		for (i = 0; i < resultLines.length - 1; i++) {
			logger.debug("createArray(String) - {}", i + 1 + ") ");
			j = 0;
			for (j = 0; j < 7; j++) {
				logger.debug("createArray(String) - {}", resultArray[i][j] + "| ");
			}
			logger.debug("createArray(String) - {}", "");
		}
		// */

		return resultArray;
	}

	// Parses the POI Results
	private static String[][] createResult(String resultLines[]) {

		int j = 0;

		// Cut the Result-Array
		String resultArray[][] = new String[resultLines.length - 1][8];
		String tempHelper[] = new String[8];

		for (j = 0; j < resultLines.length - 1; j++) {

			// System.out.println(resultLines[1]);

			// cut out Latitude
			tempHelper = resultLines[j].split("\"lat\" :");
			tempHelper = tempHelper[1].split("\"lng\" :");
			resultArray[j][0] = tempHelper[0].substring(tempHelper[0].contains("-") ? 0 : 1, tempHelper[0].length() - 17);

			// cut out the Longitude
			tempHelper = resultLines[j].split("\"lng\" :");
			tempHelper = tempHelper[1].split("\"icon\" :");
			resultArray[j][1] = tempHelper[0].substring(tempHelper[0].contains("-") ? 0 : 1, tempHelper[0].length() - 39);

			// cut out the icon
			tempHelper = resultLines[j].split("\"icon\" :");
			tempHelper = tempHelper[1].split("\"id\" :");
			resultArray[j][2] = tempHelper[0].substring(2, tempHelper[0].length() - 12);

			// cut out the id
			tempHelper = resultLines[j].split("\"id\" :");
			tempHelper = tempHelper[1].split("\"name\" :");
			resultArray[j][3] = tempHelper[0].substring(2, tempHelper[0].length() - 12);

			// cut out the name
			tempHelper = resultLines[j].split("\"name\" :");
			tempHelper = tempHelper[1].split("\"reference\" :");
			resultArray[j][4] = tempHelper[0].substring(2, tempHelper[0].length() - 12);

			// cut out the reference
			tempHelper = resultLines[j].split("\"reference\" :");
			tempHelper = tempHelper[1].split("\"types\" :");
			resultArray[j][5] = tempHelper[0].substring(2, tempHelper[0].length() - 12);

			// cut out the types
			tempHelper = resultLines[j].split("\"types\" :");
			tempHelper = tempHelper[1].split("\"vicinity\" :");
			resultArray[j][6] = tempHelper[0].substring(3, tempHelper[0].length() - 13).replaceAll("\"", "");
			// System.out.println("Types in Array " + resultArray[j][6]);

			// cut out the vicinity
			// resultArray[j][7] = resultArray[j][4];
			tempHelper = resultLines[j].split("\"vicinity\" :");
			resultArray[j][7] = tempHelper[1].substring(2, tempHelper[1].length() - 10);
		}
		return resultArray;
	}

}