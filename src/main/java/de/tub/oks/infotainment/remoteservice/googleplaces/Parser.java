/* 
 * Copyright 2013 Berlin Institute of Technology (TU Berlin), 
 * Faculty IV (CS), Department for Open Communication Systems (OKS).
 * 
 * This file is part of SpeechRecognition.
 * SpeechRecognition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SpeechRecognition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SpeechRecognition.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.tub.oks.infotainment.remoteservice.googleplaces;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.regex.Pattern;

import de.tub.oks.infotainment.main.SettingsAndEnvironment;

public class Parser {

	/**
	 * Logger for this class
	 */
	private static final Logger logger = LoggerFactory.getLogger(Parser.class);

	private SampleReader sampleReader;

	public Parser(SampleReader sampleReader) {
		this.sampleReader = sampleReader;
	}

	// Check if the Result is Know and can be used later or not
	private String checkResultType(String result) {
		if (result.contains("\"lat\" :") && result.contains("\"lng\" :") && result.contains("\"icon\" :") && result.contains("\"id\" :")
				&& result.contains("\"name\" :") && result.contains("\"reference\" :") && result.contains("\"types\" :") && result.contains("\"vicinity\" :"))
			return "normal";

		else if (result.contains("\"lat\" :") && result.contains("\"lng\" :") && result.contains("\"icon\" :") && result.contains("\"id\" :")
				&& result.contains("\"name\" :") && result.contains("\"reference\" :") && result.contains("\"types\" :") && result.contains("\"viewport\" :"))
			return "city";

		else
			return "NO_MATCH";
	}

	// Creates a resultArray[][x] x= {0 lat, 1 lng, 2 icon, 3 id, 4 name, 5
	// reference, 6 types, 7 vicinity}
	public String[][] arrayCreator(String result) {
		// Parse, Parse
		int i = 0;
		int j = 0;
		String resultArray[][] = null;
		String resultLines[] = null;

		String resultType = checkResultType(result);

		// Take the Sample if not Know Result
		if (resultType.equals("NO_MATCH")) {
			logger.debug("arrayCreator(String) - {}", "Array Feld fehlerhaft.\n Nehme Sample!");
			result = sampleReader.readSample();
			// correct the resultType for further use of the Sample
			if (SettingsAndEnvironment.getInstance().takeCityAsSampleResult)
				resultType = "city";
			else
				resultType = "city";
		}

		// Summarize the Result
		String x[] = Pattern.compile("\"results\" : ").split(result);
		x[1] = x[1].substring(8, x[1].length() - 32);

		// one result contains fields separated by "},"
		// but the results are also separated by "},)
		// so separate everything by "},"
		String y[] = Pattern.compile("  },").split(x[1]);

		// and now put the fields of one result together (depending on the
		// result)
		// normal Result
		if (resultType.equals("normal")) {
			// Put these two field in one field
			resultLines = new String[y.length / 2];
			for (i = 0; i < y.length; i = i + 2) {
				resultLines[j] = y[i] + "},\n   {" + y[i + 1] + "  },";
				j++;
			}
		}
		// City
		else if (resultType.equals("city")) {
			// Put these four field in one field
			resultLines = new String[y.length / 4];
			for (i = 0; i < y.length; i = i + 4) {
				resultLines[j] = y[i] + "},\n   {" + y[i + 1] + "},\n   {" + y[i + 2] + "},\n   {" + y[i + 3] + "  },";
				j++;
			}
		}
		// now resultLines is an Array with one result in each field
		// in the next Step we get each parameter of every result

		// Result type is know, create an fill the resultArray
		if (!resultType.equals("NO_MATCH")) {
			resultArray = new String[resultLines.length][8];
			resultArray = createResult(resultLines, resultType);
		}

		// Print the final result (for debug)
		/*
		 * i=0; for (i=0; i<resultLines.length; i++) {
		 * System.out.print(i+1+") "); j=0; for (j=0; j<7; j++){
		 * System.out.print(resultArray[i][j] + ", "); } System.out.println(); }
		 * //
		 */

		return resultArray;
	}

	//TODO complete buggy parsing from old group
	// Parses the Results, depending on the resultType
	private String[][] createResult(String resultLines[], String resultType) {

		int j = 0;

		// Cut the Result-Array
		String resultArray[][] = new String[resultLines.length][8];
		String tempHelper[] = new String[8];

		for (j = 0; j < resultLines.length; j++) {

			// cut out Latitude
			tempHelper = resultLines[j].split("\"lat\" :");
			tempHelper = tempHelper[1].split("\"lng\" :");
			resultArray[j][0] = tempHelper[0].substring(tempHelper[0].contains("-") ? 0 : 1, tempHelper[0].length() - 17);

			// cut out the Longitude
			tempHelper = resultLines[j].split("\"lng\" :");
			if (resultType.equals("city"))
				tempHelper = tempHelper[1].split("\"viewport\" :");
			if (resultType.equals("normal"))
				tempHelper = tempHelper[1].split("\"icon\" :");
			resultArray[j][1] = tempHelper[0].substring(tempHelper[0].contains("-") ? 0 : 1, tempHelper[0].length() - 39);

			// cut out the icon
			tempHelper = resultLines[j].split("\"icon\" :");
			tempHelper = tempHelper[1].split("\"id\" :");
			resultArray[j][2] = tempHelper[0].substring(2, tempHelper[0].length() - 12);

			// cut out the id
			tempHelper = resultLines[j].split("\"id\" :");
			tempHelper = tempHelper[1].split("\"name\" :");
			resultArray[j][3] = tempHelper[0].substring(2, tempHelper[0].length() - 12);

			// cut out the name
			tempHelper = resultLines[j].split("\"name\" :");
			tempHelper = tempHelper[1].split("\"reference\" :");
			resultArray[j][4] = tempHelper[0].substring(2, tempHelper[0].length() - 12);

			// cut out the reference
			tempHelper = resultLines[j].split("\"reference\" :");
			tempHelper = tempHelper[1].split("\"types\" :");
			resultArray[j][5] = tempHelper[0].substring(2, tempHelper[0].length() - 12);

			// cut out the types
			tempHelper = resultLines[j].split("\"types\" :");
			tempHelper = tempHelper[1].split("\"vicinity\" :");
			if (resultType.equals("city"))
				resultArray[j][6] = tempHelper[0].substring(3, tempHelper[0].length() - 11).replaceAll("\"", "");
			if (resultType.equals("normal"))
				resultArray[j][6] = tempHelper[0].substring(3, tempHelper[0].length() - 13).replaceAll("\"", "");
			// System.out.println("Types in Array " + resultArray[j][6]);

			// Name = Vicinity
			if (resultType.equals("city"))
				resultArray[j][7] = resultArray[j][4];

			// cut out the vicinity
			if (resultType.equals("normal")) {
				tempHelper = resultLines[j].split("\"vicinity\" :");
				resultArray[j][7] = tempHelper[1].substring(2, tempHelper[1].length() - 10);
			}
		}
		return resultArray;
	}

}
