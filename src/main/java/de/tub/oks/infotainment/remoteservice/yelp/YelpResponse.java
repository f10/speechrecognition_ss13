/* 
 * Copyright 2013 Berlin Institute of Technology (TU Berlin), 
 * Faculty IV (CS), Department for Open Communication Systems (OKS).
 * 
 * This file is part of SpeechRecognition.
 * SpeechRecognition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SpeechRecognition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SpeechRecognition.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.tub.oks.infotainment.remoteservice.yelp;

import java.lang.reflect.Array;
import java.util.List;
import java.util.ArrayList;
import java.util.Collection;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;

//class for deserialization yelp response
public class YelpResponse {
	public Region region;
	public int total;
	//public Array[] businesses ; this one works
	public ArrayList<Business> businesses;
	//public Business[] businesses;
}

class Region {
	public Span span;
	public Center center;
}

class Span {
	public double latitude_delta;
	public double longitude_delta;
}

class Center {
	public double latitude;
	public double longitude;
}

class Business {
	public boolean is_claimed;
	public long distance;
	public String mobile_url;
	public String rating_img_url;
	public int review_count;
	public String name;
	public String snippet_image_url;
	public double rating;
	public String url;
	public Location location;
	public String phone;
	public String snippet_text;
	public String image_url;
	//public String[] categories;
	public ArrayList<String[]> categories;
	public String display_phone;
	public String rating_img_url_large;
	public String id;
	public boolean is_closed;
	public String rating_img_url_small;

}

class Location {
	public String city;
	public ArrayList<String> display_address;
	public int geo_accuracy;
	public String postal_code;
	public String country_code;
	public ArrayList<String> address;
	public Coordinate coordinate;
	public String state_code;
}

class Coordinate {
	public double latitude;
	public double longitude;
}
