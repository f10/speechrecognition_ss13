/* 
 * Copyright 2013 Berlin Institute of Technology (TU Berlin), 
 * Faculty IV (CS), Department for Open Communication Systems (OKS).
 * 
 * This file is part of SpeechRecognition.
 * SpeechRecognition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SpeechRecognition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SpeechRecognition.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.tub.oks.infotainment.remoteservice.newsservice;

/**
 * general purpose news entry
 * 
 */
public class NewsEntry {

	private String headline;
	private String content;

	public NewsEntry(String headline, String snippet) {
		this.headline = headline;
		this.content = snippet;
	}

	/**
	 * @return the headline
	 */
	public String getHeadline() {
		return headline;
	}

	/**
	 * @return the content
	 */
	public String getContent() {
		return content;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof NewsEntry) {
			NewsEntry casted_obj = (NewsEntry) obj;
			if (this.headline.equals(casted_obj.getHeadline()) && this.content.equals(casted_obj.getContent())) {
				return true;
			}
			else {
				return false;
			}
		}
		return super.equals(obj);
	}
}
