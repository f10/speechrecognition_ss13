/* 
 * Copyright 2013 Berlin Institute of Technology (TU Berlin), 
 * Faculty IV (CS), Department for Open Communication Systems (OKS).
 * 
 * This file is part of SpeechRecognition.
 * SpeechRecognition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SpeechRecognition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SpeechRecognition.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.tub.oks.infotainment.remoteservice.socialservice.facebook;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;

import com.restfb.Connection;
import com.restfb.DefaultFacebookClient;
import com.restfb.FacebookClient;
import com.restfb.Parameter;
import com.restfb.types.FacebookType;
import com.restfb.types.Post;

import de.tub.oks.infotainment.remoteservice.RemoteFunctions;
import de.tub.oks.infotainment.remoteservice.socialservice.ISocialService;
import de.tub.oks.infotainment.remoteservice.socialservice.SocialServicePost;

/**
 * social network access implementation for facebook is able to post messages
 * and get timeline from user
 * 
 */
public class FacebookService implements ISocialService {

	/**
	 * Logger for this class
	 */
	private static final Logger logger = LoggerFactory.getLogger(FacebookService.class);

	private static final String APP_ID = "296968000324719";

	private static final String APP_SECRET = "f6588cbf4cc4a425d90581be5969cdda";

	private FacebookClient facebookClient;

	private boolean onlyUnread = false;

	private SocialServicePost lastKnownEntry;

	// static is fine for most applications because only one user at a time is
	// possible
	// change for different approach
	private static String accessToken;

	private String redirectUrl = "";

	public FacebookService() {
	}

	@Override
	public boolean autheticate() {
		if (null == accessToken)
			return false;
		facebookClient = new DefaultFacebookClient(accessToken);
		if (null != facebookClient) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public void post(String text) {
		FacebookType publishMessageResponse = facebookClient.publish("me/feed", FacebookType.class, Parameter.with("message", text));

		logger.debug("post(String) - {}", "Published message ID: " + publishMessageResponse.getId());

	}

	@Override
	public List<SocialServicePost> getLatestPosts() {
		List<SocialServicePost> posts = new ArrayList<SocialServicePost>();

		try {
			Connection<Post> fetchObjectsResults = facebookClient.fetchConnection("me/home", Post.class);
			for (Post post : fetchObjectsResults.getData()) {
				//if (post.getType().equals("status")) { // no videos,links..
				if (post.getMessage() == null) {
					continue;
				}
				SocialServicePost newPost = new SocialServicePost(post.getFrom().getName(), post.getMessage());
				if (onlyUnread && lastKnownEntry != null && lastKnownEntry.equals(post)) {
					break;
				}
				posts.add(newPost);
				//}
			}
			if (posts.size() > 0) {
				lastKnownEntry = posts.get(0);
			}
		}
		catch (Exception e) {
			logger.error("getLatestPosts()", e);
		}
		return posts;
	}

	@Override
	public void setOnlyUnread(boolean onlyUnread) {
		this.onlyUnread = onlyUnread;
	}

	@Override
	public boolean getOnlyUnread() {
		return onlyUnread;
	}

	@Override
	public void deautheticate() {
		accessToken = null;
	}

	@Override
	public String getAuthenticationLink() {
		return "https://www.facebook.com/dialog/oauth?client_id=" + APP_ID + "&redirect_uri=" + redirectUrl + "&scope=read_stream,publish_stream ";
	}

	@Override
	public void setAuthenticationAnswer(String userKey) {
		setAuthorizationCode(userKey);
	}

	@Override
	public void setRedirectUrl(String url) {
		redirectUrl = url;
	}

	/**
	 * @param authorizationCode
	 *            the authorizationCode to set
	 */
	public void setAuthorizationCode(String authorizationCode) {

		HttpClient httpclient = new HttpClient();
		String url = "https://graph.facebook.com/oauth/access_token?client_id=" + APP_ID + "&redirect_uri=" + redirectUrl + "&client_secret=" + APP_SECRET
				+ "&code=" + authorizationCode;
		GetMethod getMethod = new GetMethod(url);
		try {
			httpclient.executeMethod(getMethod);

			String responseBody = getMethod.getResponseBodyAsString();
			Map<String, String> variables = RemoteFunctions.parseQueryString(responseBody);

			accessToken = variables.get("access_token");

		}
		catch (Exception e) {
			logger.error("setAuthorizationCode()", e);
		}
		finally {
			// Release current connection to the connection pool once you are
			// done
			getMethod.releaseConnection();
		}
	}
}
