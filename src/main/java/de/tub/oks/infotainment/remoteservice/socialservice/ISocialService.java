/* 
 * Copyright 2013 Berlin Institute of Technology (TU Berlin), 
 * Faculty IV (CS), Department for Open Communication Systems (OKS).
 * 
 * This file is part of SpeechRecognition.
 * SpeechRecognition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SpeechRecognition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SpeechRecognition.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.tub.oks.infotainment.remoteservice.socialservice;

import java.util.List;

/**
 * general interface for a social network provider includes functionality for
 * authentication, posting and reading
 * 
 */
public interface ISocialService {

	/**
	 * try authentication if fails return false
	 * 
	 * @return authentication success
	 */
	public boolean autheticate();

	/**
	 * deletes last authentication
	 */
	public void deautheticate();

	/**
	 * get link for an oauth authentication or similar
	 * 
	 * @return
	 */
	public String getAuthenticationLink();

	/**
	 * set answer if available from authentication website
	 * 
	 * @param userKey
	 */
	public void setAuthenticationAnswer(String userKey);

	/**
	 * post user status
	 * 
	 * @param text
	 */
	public void post(String text);

	/**
	 * get latest messages from user connections gets all if onlyUnread=false
	 * get only unread (not retunred before) if onlyUnread=true
	 * 
	 * @return
	 */
	public List<SocialServicePost> getLatestPosts();

	/**
	 * set dependency which posts will be returned
	 * 
	 * @see ISocialService.getLatestPosts
	 * @param onlyUnread
	 */
	public void setOnlyUnread(boolean onlyUnread);

	/**
	 * get current configuration of onlyUnread
	 * 
	 * @return
	 */
	public boolean getOnlyUnread();

	/**
	 * you could define a url for redirection before generating
	 * 
	 * @see ISocialService.getAuthenticationLink
	 * @param url
	 */
	public void setRedirectUrl(String url);

}
