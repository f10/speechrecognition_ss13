/* 
 * Copyright 2013 Berlin Institute of Technology (TU Berlin), 
 * Faculty IV (CS), Department for Open Communication Systems (OKS).
 * 
 * This file is part of SpeechRecognition.
 * SpeechRecognition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SpeechRecognition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SpeechRecognition.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.tub.oks.infotainment.remoteservice.googleplaces;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

public class SampleReader {

	/**
	 * Logger for this class
	 */
	private static final Logger logger = LoggerFactory.getLogger(SampleReader.class);

	public String readSample() {

		String text = "";
		String sampleFile = "";

		BufferedReader input;
		try {
			sampleFile = "/de/tub/oks/infotainment/res/sampledata/poiSample.txt";
			input = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream(sampleFile)));

			String line;

			while ((line = input.readLine()) != null) {
				text = text + '\n' + line;
			}
			if (text.endsWith("\n"))
				text = text + "\n";

			input.close();
		}
		catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			logger.error("readSample()", e);
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			logger.error("readSample()", e);
		}
		return text;

	}

}
