/* 
 * Copyright 2013 Berlin Institute of Technology (TU Berlin), 
 * Faculty IV (CS), Department for Open Communication Systems (OKS).
 * 
 * This file is part of SpeechRecognition.
 * SpeechRecognition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SpeechRecognition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SpeechRecognition.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.tub.oks.infotainment.remoteservice.googleplaces;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Scanner;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;

import de.tub.oks.infotainment.main.SettingsAndEnvironment;
import de.tub.oks.infotainment.main.states.Poi.RemoteType;

public class POIFinder {

	/**
	 * Logger for this class
	 */
	private static final Logger logger = LoggerFactory.getLogger(POIFinder.class);

	public String searchType = "point_of_interest";

	// Koordinaten vom Frauenhoferinstitut
	public String latitudeExample = "52.526007";
	public String longtitudeExample = "13.314297";

	// Fahre Vom Brandenburger Tor Richtung Osten
	// Last Point
	public String sample_lati_last = "52.516381";
	public String sample_long_last = "13.381037";
	// Current Point
	public String sample_lati_current = "52.516554";
	public String sample_long_current = "13.38388";

	private SettingsAndEnvironment misc = SettingsAndEnvironment.getInstance();

	// used in runRemoteSearch in Poi
	public String getPlaces(String searchTerm, String current_lati, String current_long, int searchRadius) {

		if (searchTerm.contains(" "))
			searchTerm = searchTerm.replace(" ", "+");
		if (searchTerm.contains("�"))
			searchTerm = searchTerm.replace("�", "oe");
		if (searchTerm.contains("�"))
			searchTerm = searchTerm.replace("�", "ae");
		if (searchTerm.contains("�"))
			searchTerm = searchTerm.replace("�", "ue");
		// String strURL =
		// "https://maps.googleapis.com/maps/api/place/search/json?location=-33.8670522,151.1957362&radius=500&types=food&name=harbour&sensor=false&key=AIzaSyCCSBXKN-Qjtu_gLrmtg8zSNrAPPxOSf3M"
		// ;//ABQIAAAAgkVeUk-x-fdDbHlkxV2Y_xRtCta4EJMNOm4t9dzbDxVGnex2IhSLpjKDEzjNFVv-5BFxWvRTGjX8CQ
		// String strURL =
		// "https://maps.googleapis.com/maps/api/place/search/json?location=52.530697,13.317462&radius=500&name="
		// + searchTerm+
		// "&sensor=false&key=AIzaSyCCSBXKN-Qjtu_gLrmtg8zSNrAPPxOSf3M" ;
		String strURL = "";
		if (searchTerm.equals(searchType))
			strURL = "https://maps.googleapis.com/maps/api/place/search/json?location=" + current_lati + "," + current_long + "&radius=" + searchRadius
					+ "&type=" + searchType + "&sensor=false&key=AIzaSyCCSBXKN-Qjtu_gLrmtg8zSNrAPPxOSf3M";
		else
			strURL = "https://maps.googleapis.com/maps/api/place/search/json?location=" + current_lati + "," + current_long + "&radius=" + searchRadius
					+ "&name=" + searchTerm + "&sensor=false&key=AIzaSyCCSBXKN-Qjtu_gLrmtg8zSNrAPPxOSf3M";

		PostMethod post = new PostMethod(strURL);
		HttpClient httpclient = new HttpClient();
		String ret = null;
		try {
			int result = httpclient.executeMethod(post);
			if (misc.debug >= 2)
				logger.debug("getPlaces(String, String, String, int) - {}", "Response status code: " + result + " , error:" + post.getResponseHeaders().length);

			StringBuffer buffer = new StringBuffer();
			try {
				InputStream is = post.getResponseBodyAsStream();
				BufferedReader in = new BufferedReader(new InputStreamReader(is));
				String str = "";
				while (str != null) {
					str = in.readLine();
					buffer.append(str);
				}
			}
			catch (IOException e) {
				logger.error("getPlaces()", e);
			}

			ret = buffer.toString();
		}
		catch (Exception e) {
			logger.error("getPlaces()", e);
		}
		finally {
			// Release current connection to the connection pool once you are
			// done
			post.releaseConnection();
		}
		return ret;
		// https://maps.googleapis.com/maps/api/place/search/json?location=-33.8670522,151.1957362&radius=500&types=food&name=harbour&sensor=false&key=AIzaSyAiFpFd85eMtfbvmVNEYuNds5TEF9FjIPI
	}

	public String[][] filterPoiByRadius(String[][] poiList, int radius, String current_lati, String current_long) {

		ArrayList<String[]> filteredPoiList = new ArrayList<String[]>();

		for (int i = 0; i < poiList.length; i++) {
			if (distance(Double.valueOf(current_lati), Double.valueOf(current_long), Double.valueOf(poiList[i][0]), Double.valueOf(poiList[i][1])) <= (Double
					.valueOf(radius) / 1000))
				filteredPoiList.add(poiList[i]);
		}
		String[][] filteredPoiArray = new String[filteredPoiList.size()][8];

		for (int j = 0; j < filteredPoiList.size(); j++) {
			filteredPoiArray[j] = filteredPoiList.get(j);
		}

		return filteredPoiArray;
	}

	private boolean poiCheckIfSight(String type) {
		if (type.equals("establishment, lodging") || type.equals("lodging, establishment") || type.equals("establishment"))
			return true;

		else
			return false;
	}

	public String[][] filterPoiByDirection(double bearing, String[][] poiList, RemoteType type, String current_lati, String current_long) {
		boolean isType = false;
		double bearingForCurrentObject = 0;

		//System.out.println("Bearing: " + bearing + "\n");
		ArrayList<String[]> filteredPoiList = new ArrayList<String[]>();

		for (int i = 0; i < poiList.length; i++) {
			// Check if the current Entry in poiList is a Sight
			//System.out.println("\n\n\n\n Check: " + poiList[i][6] + "\n\n\n\n");

			isType = poiCheckIfSight(poiList[i][6]); // boolean

			// Get the Bearing for the current Entry
			bearingForCurrentObject = POIFinder.calcBearing(Double.valueOf(current_long), Double.valueOf(current_lati), Double.valueOf(poiList[i][1]),
					Double.valueOf(poiList[i][0]));

			//System.out.println("Bearing for Current object: "
			//		+ bearingForCurrentObject + "\n");

			// The sides are a quarter of a circle (front to 90� on the side)
			// Take only the sights on the RIGHT side
			if (type == RemoteType.right) {
				if (((bearing < bearingForCurrentObject) && (bearingForCurrentObject < ((bearing + 90.0)))) && isType) {
					filteredPoiList.add(poiList[i]);
				}
			}
			// Take only the sights on the LEFT side
			else if (type == RemoteType.left) {
				// if(bearingForCurrentObject >= 270 && bearingForCurrentObject
				// < 360)
				if (((bearing > bearingForCurrentObject) && (bearingForCurrentObject > ((bearing - 90.0) % 360))) && isType) {
					filteredPoiList.add(poiList[i]);
				}
			}
		}
		String[][] filteredPoiArray = new String[filteredPoiList.size()][8];

		for (int j = 0; j < filteredPoiList.size(); j++) {
			filteredPoiArray[j] = filteredPoiList.get(j);
		}

		return filteredPoiArray; // filterPoiByRadius(filteredPoiArray,
									// Misc.searchRadius, current_lati,
									// current_long);
	}

	// Calculate bearing from lat1/lon1 to lat2/lon2
	// Note lat1/lon1/lat2/lon2 must be in radians
	// Returns float bearing in degrees
	public double calcBearingFromFile(String gpsFile) {
		if (!misc.useGPSStick) {
			return calcBearing(Double.valueOf(sample_long_last), Double.valueOf(sample_lati_last), Double.valueOf(sample_long_current),
					Double.valueOf(sample_lati_current));
		}
		else {
			ArrayList<Double> lastTwoRecords = getCoordinatesForBearing(gpsFile);
			double lon1 = lastTwoRecords.get(0);
			double lat1 = lastTwoRecords.get(1);
			double lon2 = lastTwoRecords.get(2);
			double lat2 = lastTwoRecords.get(3);
			return calcBearing(lon1, lat1, lon2, lat2);
		}
	}

	private static double calcBearing(double lon1, double lat1, double lon2, double lat2) {

		double y = lat2 - lat1;
		double x = lon2 - lon1;
		double sqrtResult = Math.sqrt(x * x + y * y);

		double angle = Math.asin(Math.abs(x) / sqrtResult);
		double alpha = angle * 180.0 / Math.PI;

		if (x > 0) { // I or IV quadrant
			if (y < 0) { // IV quadrant
				alpha = 180 - alpha;
			}
		}
		else { // II or III quadrant
			if (y > 0) { // II quadrant
				alpha = -alpha;
			}
			else
				alpha = alpha - 180;
		}
		alpha = (alpha + 360.0) % 360.0;

		return alpha;
	}

	private static ArrayList<Double> getCoordinatesForBearing(String gpsFile) {
		double d;

		ArrayList<Double> coordsForBearing = new ArrayList<Double>();

		FileReader fin = null;
		try {
			fin = new FileReader(gpsFile);
		}
		catch (FileNotFoundException e1) {
			// Auto-generated catch block
			logger.error("getCoordinatesForBearing()", e1);
		}

		Scanner src = new Scanner(fin).useDelimiter("\\s");
		src.useLocale(java.util.Locale.ENGLISH);

		while (src.hasNext()) {
			if (src.hasNextDouble()) {
				d = src.nextDouble();
				coordsForBearing.add(d);
			}
		}
		// }
		try {
			fin.close();
		}
		catch (IOException e) {
			// Auto-generated catch block
			logger.error("getCoordinatesForBearing()", e);
		}

		return coordsForBearing;
	}

	private static double distance(double lat1, double lon1, double lat2, double lon2) {
		double theta = lon1 - lon2;
		double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
		dist = Math.acos(dist);
		dist = rad2deg(dist);
		dist = dist * 60 * 1.1515;

		dist = dist * 1.609344;

		return (dist);
	}

	private static double deg2rad(double deg) {
		return (deg * Math.PI / 180.0);
	}

	private static double rad2deg(double rad) {
		return (rad * 180.0 / Math.PI);
	}
}