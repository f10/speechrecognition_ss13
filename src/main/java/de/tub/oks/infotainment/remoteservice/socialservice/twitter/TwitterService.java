/* 
 * Copyright 2013 Berlin Institute of Technology (TU Berlin), 
 * Faculty IV (CS), Department for Open Communication Systems (OKS).
 * 
 * This file is part of SpeechRecognition.
 * SpeechRecognition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SpeechRecognition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SpeechRecognition.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.tub.oks.infotainment.remoteservice.socialservice.twitter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

import de.tub.oks.infotainment.remoteservice.socialservice.ISocialService;
import de.tub.oks.infotainment.remoteservice.socialservice.SocialServicePost;

import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;

/**
 * social network access implementation for twitter is able to post messages and
 * get timeline from user
 * 
 */
public class TwitterService implements ISocialService {

	/**
	 * Logger for this class
	 */
	private static final Logger logger = LoggerFactory.getLogger(TwitterService.class);

	private Twitter twitter;

	private final static String CONSUMER_KEY = "GkyqknTv5Qyb3juPeQ5Fw";

	private final static String CONSUMER_SECRET = "mmLn25aTKzqtBamBhyUfDG4y3st0mQeOmbD8ipkpV8";

	private boolean onlyUnread = false;

	private SocialServicePost lastKnownEntry;

	// static is fine for most applications because only one user at a time is
	// possible
	// change for different approach
	private static AccessToken accessToken;

	private String redirectUrl = "";

	public TwitterService() {
		twitter = new TwitterFactory().getInstance();
		twitter.setOAuthConsumer(CONSUMER_KEY, CONSUMER_SECRET);
	}

	@Override
	public boolean autheticate() {
		try {
			accessToken = twitter.getOAuthAccessToken();
		}
		catch (Exception e) {
			logger.error("autheticate()", e);
		}
		if (null != accessToken) {
			return true;
		}
		else {
			return false;
		}

	}

	@Override
	public void post(String text) {
		Status status;
		try {
			status = twitter.updateStatus(text);
			logger.debug("post(String) - {}", "Successfully updated the status to [" + status.getText() + "].");
		}
		catch (TwitterException e) {
			logger.error("post()", e);
			logger.debug("post(String) - {}", "Failed to update status");
		}

	}

	@Override
	public List<SocialServicePost> getLatestPosts() {
		List<SocialServicePost> timeline = new ArrayList<SocialServicePost>();
		try {
			@SuppressWarnings("deprecation")
			List<Status> statuses = twitter.getFriendsTimeline();
			for (Status status : statuses) {
				SocialServicePost post = new SocialServicePost(status.getUser().getName(), status.getText());
				if (onlyUnread && lastKnownEntry != null && lastKnownEntry.equals(post)) {
					break;
				}
				timeline.add(post);
			}
			if (timeline.size() > 0) {
				lastKnownEntry = timeline.get(0);
			}
		}
		catch (TwitterException e) {
			logger.error("getLatestPosts()", e);
			logger.debug("getLatestPosts() - {}", "Can't get timeline!");
		}
		return timeline;
	}

	@Override
	public void setOnlyUnread(boolean onlyUnread) {
		this.onlyUnread = onlyUnread;

	}

	@Override
	public boolean getOnlyUnread() {
		return onlyUnread;
	}

	@Override
	public void deautheticate() {
		accessToken = null;
		try {
			//logout with setting AccessToken to null
			twitter.setOAuthAccessToken(accessToken);
		}
		catch (Exception e) {
		}
	}

	RequestToken requestToken = null;

	@Override
	public String getAuthenticationLink() {
		try {
			requestToken = twitter.getOAuthRequestToken(redirectUrl);

			return requestToken.getAuthorizationURL();
		}
		catch (TwitterException e) {
			logger.error("getAuthenticationLink()", e);
		}
		return "";
	}

	@Override
	public void setAuthenticationAnswer(String userKey) {
		try {
			accessToken = twitter.getOAuthAccessToken(requestToken);
		}
		catch (TwitterException e) {
			logger.error("setAuthenticationAnswer()", e);
			accessToken = null;
		}
	}

	@Override
	public void setRedirectUrl(String url) {
		redirectUrl = url;
	}

}
