/* 
 * Copyright 2013 Berlin Institute of Technology (TU Berlin), 
 * Faculty IV (CS), Department for Open Communication Systems (OKS).
 * 
 * This file is part of SpeechRecognition.
 * SpeechRecognition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SpeechRecognition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SpeechRecognition.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.tub.oks.infotainment.remoteservice.newsservice;

import java.util.List;

/**
 * Interface represents general news provider methods for selecting news of
 * different categories
 * 
 */
public interface News {

	/**
	 * Get News dependent on setOnlyUnread setting
	 * 
	 * @param category
	 * @return
	 */
	public List<NewsEntry> getNews(NewsCategory category);

	/**
	 * Get available news categories
	 * 
	 * @return
	 */
	public List<NewsCategory> getCategories();

	/**
	 * Select if you want all available news or only available news which you do
	 * not get before
	 * 
	 * @param onlyUnread
	 */
	public void setOnlyUnread(boolean onlyUnread);

	/**
	 * Get only unread or all news status
	 * 
	 * @return
	 */
	public boolean isOnlyUnread();

}
