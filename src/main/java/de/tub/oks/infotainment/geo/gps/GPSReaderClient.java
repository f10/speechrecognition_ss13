/* 
 * Copyright 2013 Berlin Institute of Technology (TU Berlin), 
 * Faculty IV (CS), Department for Open Communication Systems (OKS).
 * 
 * This file is part of SpeechRecognition.
 * SpeechRecognition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SpeechRecognition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SpeechRecognition.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.tub.oks.infotainment.geo.gps;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

import ocss.nmea.api.NMEAClient;
import ocss.nmea.api.NMEAEvent;

/**
 * The simplest you can write
 */
public class GPSReaderClient extends NMEAClient {

	/**
	 * Logger for this class
	 */
	private static final Logger logger = LoggerFactory.getLogger(GPSReaderClient.class);

	public GPSReaderClient(String s, String[] sa, String gpsFile) {
		super(s, sa);
		this.gpsFile = gpsFile;
	}

	private Coordinate coordinates = null;
	private String gpsFile;

	private int rows = 0;

	private synchronized void setRows(int rows) {
		this.rows = rows;
	}

	private synchronized int getRows() {
		return this.rows;
	}

	public Coordinate getCoordinates() {
		return coordinates;
	}

	public void setCoordinates(Coordinate coordinates) {
		this.coordinates = coordinates;
		writeCoordinatesToFile(coordinates);
	}

	private void writeCoordinatesToFile(Coordinate coordinates) {

		try {

			File f;
			f = new File(gpsFile);
			if (!f.exists())
				f.createNewFile();

			FileReader fin = new FileReader(gpsFile);

			Scanner src = new Scanner(fin).useDelimiter("\\s");
			src.useLocale(java.util.Locale.ENGLISH);

			while (src.hasNextDouble()) {
				if (getRows() == 4) {
					logger.debug("writeCoordinatesToFile(Coordinate) - {}", "rows == " + src.next());
					return;
				}
				int tmp = getRows() + 1;
				setRows(tmp);
				src.next();
				// if(getRows() == 2){
				// Thread.currentThread().sleep(2000) ;
				// return;
				// }
			}
			//
			src.close();
			fin.close();
			FileWriter fout;

			fout = new FileWriter(gpsFile, true);
			logger.debug("writeCoordinatesToFile(Coordinate) - {}", "Writing " + coordinates.getLongtitude() + " " + coordinates.getLatitude() + " to file.\n");
			fout.write(coordinates.getLongtitude() + " " + coordinates.getLatitude() + " ");
			fout.close();

		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			logger.error("writeCoordinatesToFile()", e);
		} /*
		 * catch (InterruptedException e) { // TODO Auto-generated catch block
		 * e.printStackTrace(); }
		 */

	}

	private int skip = 0;

	public void dataDetectedEvent(NMEAEvent e) {

		skip++;
		if (skip % 2 == 0 || (skip > 0 && skip < 4))
			return;
		// System.out.println("Received:" + e.getContent());
		String nmeaString = e.getContent();
		Coordinate coordinates = NMEAStringParser.parseNMEA(nmeaString);

		setCoordinates(coordinates);

	}

}
