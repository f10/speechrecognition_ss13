/* 
 * Copyright 2013 Berlin Institute of Technology (TU Berlin), 
 * Faculty IV (CS), Department for Open Communication Systems (OKS).
 * 
 * This file is part of SpeechRecognition.
 * SpeechRecognition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SpeechRecognition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SpeechRecognition.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.tub.oks.infotainment.geo.gps;

import java.util.StringTokenizer;

public class NMEAStringParser {

	public static Coordinate parseNMEA(String nmeaString) {

		// ==================================

		String record_latitude;

		StringTokenizer tokenizer1 = new StringTokenizer(new String(nmeaString));
		String token, token1;
		Coordinate coordinates = null;
		while (tokenizer1.hasMoreTokens()) {

			token1 = tokenizer1.nextToken();
			StringTokenizer tokenizer = new StringTokenizer(token1);
			token = tokenizer.nextToken();
			StringTokenizer tokenizer2 = new StringTokenizer(token, ",");

			if (tokenizer2.hasMoreTokens() && token.startsWith("$GPGGA")) {
				tokenizer2.nextToken();
				tokenizer2.nextToken();

				// Latitude
				String raw_lat = tokenizer2.nextToken();
				String lat_deg = raw_lat.substring(0, 2);
				String lat_min1 = raw_lat.substring(2, 4);
				String lat_min2 = raw_lat.substring(5);
				String lat_min3 = "0." + lat_min1 + lat_min2;
				float lat_dec = Float.parseFloat(lat_min3) / .6f;
				float lat_val = Float.parseFloat(lat_deg) + lat_dec;

				// Latitude direction
				String lat_direction = tokenizer2.nextToken();
				if (lat_direction.equals("N")) {
					// do nothing
				}
				else {
					lat_val = lat_val * -1;
				}

				record_latitude = lat_val + "";

				// Longitude
				String raw_lon = tokenizer2.nextToken();
				String lon_deg = raw_lon.substring(0, 3);
				String lon_min1 = raw_lon.substring(3, 5);
				String lon_min2 = raw_lon.substring(6);
				String lon_min3 = "0." + lon_min1 + lon_min2;
				float lon_dec = Float.parseFloat(lon_min3) / .6f;
				float lon_val = Float.parseFloat(lon_deg) + lon_dec;

				// Longitude direction
				String lon_direction = tokenizer2.nextToken();
				if (lon_direction.equals("E")) {
					// do nothing
				}
				else {
					lon_val = lon_val * -1;
				}
				String record_longitude = lon_val + "";

				// if(Integer.valueOf(Misc.getAppProperty("debug.level"))>0)
				// System.out.println(">>>>>>>>" +record_latitude + "," +
				// record_longitude);

				coordinates = new Coordinate(record_longitude, record_latitude, null, null);

			}

		}
		return coordinates;

	}

}
