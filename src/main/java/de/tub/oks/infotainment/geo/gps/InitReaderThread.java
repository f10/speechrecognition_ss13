/* 
 * Copyright 2013 Berlin Institute of Technology (TU Berlin), 
 * Faculty IV (CS), Department for Open Communication Systems (OKS).
 * 
 * This file is part of SpeechRecognition.
 * SpeechRecognition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SpeechRecognition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SpeechRecognition.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.tub.oks.infotainment.geo.gps;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

public class InitReaderThread implements Runnable {

	/**
	 * Logger for this class
	 */
	private static final Logger logger = LoggerFactory.getLogger(InitReaderThread.class);

	private GPSReaderClient client;

	private String gpsFile;

	public InitReaderThread(String gpsFile) {
		this.gpsFile = gpsFile;
	}

	@Override
	public void run() {
		logger.debug("run() - {}", "Starting Thread..");
		File f;

		f = new File(gpsFile);
		if (f.exists())
			f.delete();
		logger.debug("run() - {}", ">>>>>>>" + gpsFile);
		client = startReader();
		try {
			Thread.sleep(10000);
		}
		catch (InterruptedException e) {
			logger.error("run()", e);
		}

		stopReader(client);
	}

	private GPSReaderClient startReader() {
		String prefix = "GP";
		// String[] array = {"HDM", "GLL", "GGA", "XTE", "MWV", "VHW", "HDG",
		// "BOD"};
		String[] array = { "GGA" };
		GPSReaderClient customClient = new GPSReaderClient(prefix, array, gpsFile);
		customClient.initClient();
		try {
			logger.debug("startReader() - {}", "sleeping for 2 sec");
			Thread.sleep(2000);
		}
		catch (InterruptedException e) {
			// TODO Auto-generated catch block
			logger.error("startReader()", e);
		}
		customClient.setReader(new GPSReader(customClient.getListeners())); // Serial
																			// Port
																			// reader
		customClient.startWorking();

		return customClient;
	}

	private void stopReader(GPSReaderClient client) {
		((GPSReader) client.getReader()).stopReader();
	}

}
