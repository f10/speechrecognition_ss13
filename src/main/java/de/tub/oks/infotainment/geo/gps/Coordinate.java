/* 
 * Copyright 2013 Berlin Institute of Technology (TU Berlin), 
 * Faculty IV (CS), Department for Open Communication Systems (OKS).
 * 
 * This file is part of SpeechRecognition.
 * SpeechRecognition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SpeechRecognition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SpeechRecognition.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.tub.oks.infotainment.geo.gps;

/**
 * The Class Coordinate.
 */
public class Coordinate {
	private String longtitude;
	private String latitude;
	private String longDirection;
	private String latDirection;

	/**
	 * Instantiates a new coordinate.
	 * 
	 * @param longtitude
	 *            the longtitude
	 * @param latitude
	 *            the latitude
	 * @param longDirection
	 *            the long direction
	 * @param latDirection
	 *            the lat direction
	 */
	public Coordinate(String longtitude, String latitude, String longDirection, String latDirection) {
		super();
		this.longtitude = longtitude;
		this.latitude = latitude;
		this.longDirection = longDirection;
		this.latDirection = latDirection;
	}

	/**
	 * Gets the longtitude.
	 * 
	 * @return the longtitude
	 */
	public String getLongtitude() {
		return longtitude;
	}

	/**
	 * Gets the latitude.
	 * 
	 * @return the latitude
	 */
	public String getLatitude() {
		return latitude;
	}

	/**
	 * Gets the long direction.
	 * 
	 * @return the long direction
	 */
	public String getLongDirection() {
		return longDirection;
	}

	/**
	 * Gets the lat direction.
	 * 
	 * @return the lat direction
	 */
	public String getLatDirection() {
		return latDirection;
	}

}
