/* 
 * Copyright 2013 Berlin Institute of Technology (TU Berlin), 
 * Faculty IV (CS), Department for Open Communication Systems (OKS).
 * 
 * This file is part of SpeechRecognition.
 * SpeechRecognition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SpeechRecognition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SpeechRecognition.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.tub.oks.infotainment.geo.gps;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gnu.io.CommPort;
import gnu.io.CommPortIdentifier;
import gnu.io.NoSuchPortException;
import gnu.io.PortInUseException;
import gnu.io.SerialPort;
import gnu.io.UnsupportedCommOperationException;

import java.io.InputStream;
import java.util.ArrayList;

import de.tub.oks.infotainment.main.SettingsAndEnvironment;

import ocss.nmea.api.NMEAEvent;
import ocss.nmea.api.NMEAReader;

public class GPSReader extends NMEAReader implements Runnable {

	/**
	 * Logger for this class
	 */
	private static final Logger logger = LoggerFactory.getLogger(GPSReader.class);

	public GPSReader(ArrayList al) {
		super(al);
	}

	private boolean shouldStop = false;
	private CommPort thePort;

	public void read() {
		super.enableReading();
		// Opening Serial port COM1
		CommPortIdentifier com = null;
		try {

			com = CommPortIdentifier.getPortIdentifier(SettingsAndEnvironment.getInstance().comport);//

		}
		catch (NoSuchPortException nspe) {
			return;
		}
		thePort = null;
		try {
			thePort = com.open("PortOpener", 10);
		}
		catch (PortInUseException piue) {
			logger.error("read() - Port In Use", piue);
			return;
		}
		int portType = com.getPortType();
		if (portType == CommPortIdentifier.PORT_PARALLEL)
			if (SettingsAndEnvironment.getInstance().debug > 0)
				logger.debug("read() - {}", "This is a parallel port");
			else if (portType == CommPortIdentifier.PORT_SERIAL)
				if (SettingsAndEnvironment.getInstance().debug > 0)
					logger.debug("read() - {}", "This is a serial port");
				else
					logger.debug("read() - {}", "This is an unknown port:" + portType);
		if (portType == CommPortIdentifier.PORT_SERIAL) {
			SerialPort sp = (SerialPort) thePort;
			try {
				// Settings for B&G Hydra
				sp.setSerialPortParams(4800, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);
			}
			catch (UnsupportedCommOperationException ucoe) {
				logger.error("read() - Unsupported Comm Operation", ucoe);
				return;
			}
		}
		// Reading on Serial Port
		try {
			byte[] buffer = new byte[4096];
			InputStream theInput = thePort.getInputStream();
			if (SettingsAndEnvironment.getInstance().debug > 0)
				logger.debug("read() - {}", "Reading serial port...");
			while (canRead() && !shouldStop) // Loop
			// for(int zz = 0; zz<100; zz++)
			{
				int bytesRead = theInput.read(buffer);
				if (bytesRead == -1)
					break;
				// System.out.println("Read " + bytesRead + " characters");
				// Count up to the first not null
				int nn = bytesRead;
				for (int i = 0; i < Math.min(buffer.length, bytesRead); i++) {
					if (buffer[i] == 0) {
						nn = i;
						break;
					}
				}
				byte[] toPrint = new byte[nn];
				for (int i = 0; i < nn; i++)
					toPrint[i] = buffer[i];
				// Broadcast event
				super.fireDataRead(new NMEAEvent(this, new String(toPrint))); // Broadcast
																				// the
																				// event

			}

			if (SettingsAndEnvironment.getInstance().debug > 0)
				logger.debug("read() - {}", "Stop Reading serial port.");

			thePort.close();
		}
		catch (Exception e) {
			logger.error("read()", e);
			thePort.close();
		}
	}

	public void stopReader() {
		this.shouldStop = true;
		thePort.close();
	}

}
