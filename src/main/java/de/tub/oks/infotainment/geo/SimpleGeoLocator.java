/* 
 * Copyright 2013 Berlin Institute of Technology (TU Berlin), 
 * Faculty IV (CS), Department for Open Communication Systems (OKS).
 * 
 * This file is part of SpeechRecognition.
 * SpeechRecognition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SpeechRecognition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SpeechRecognition.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.tub.oks.infotainment.geo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.tub.oks.infotainment.geo.gps.Coordinate;
import de.tub.oks.infotainment.geo.gps.GPSReader;
import de.tub.oks.infotainment.geo.gps.GPSReaderClient;
import de.tub.oks.infotainment.main.SettingsAndEnvironment;

public class SimpleGeoLocator implements GeoLocator {

	/**
	 * Logger for this class
	 */
	private static final Logger logger = LoggerFactory.getLogger(SimpleGeoLocator.class);

	@Override
	public Coordinate getCoordinates(String gpsFile) {

		String prefix = "GP";
		// String[] array = {"HDM", "GLL", "GGA", "XTE", "MWV", "VHW", "HDG",
		// "BOD"};
		String[] array = { "GGA" };
		GPSReaderClient customClient = new GPSReaderClient(prefix, array, gpsFile);
		customClient.initClient();
		customClient.setReader(new GPSReader(customClient.getListeners())); // Serial
																			// Port
																			// reader
		customClient.startWorking();

		while (customClient.getCoordinates() == null)
			; // wait
		if ((Integer.valueOf(SettingsAndEnvironment.getInstance().debug) > 0))
			logger.debug("getCoordinates(String) - {}", customClient.getCoordinates().getLatitude() + "," + customClient.getCoordinates().getLongtitude());
		((GPSReader) customClient.getReader()).stopReader();
		return customClient.getCoordinates();
	}

}
