/* 
 * Copyright 2013 Berlin Institute of Technology (TU Berlin), 
 * Faculty IV (CS), Department for Open Communication Systems (OKS).
 * 
 * This file is part of SpeechRecognition.
 * SpeechRecognition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SpeechRecognition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SpeechRecognition.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.tub.oks.infotainment.audio.cloudgarden;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.speech.recognition.*;

import de.tub.oks.infotainment.main.SettingsAndEnvironment;

/**
 * Prints audio events to System.out
 * 
 * @see AudioEvent
 */
public class AudioListener extends RecognizerAudioAdapter {

  /**
   * Logger for this class
   */
  private static final Logger logger = LoggerFactory.getLogger(AudioListener.class);

  /**
   * Instantiates a new audio listener.
   */
  public AudioListener() {
  }

  /**
   * (non-Javadoc)
   * 
   * @see javax.speech.recognition.RecognizerAudioAdapter#speechStarted(javax.speech.recognition.RecognizerAudioEvent)
   */
  public void speechStarted(RecognizerAudioEvent e) {
    logger.debug("speechStarted(RecognizerAudioEvent) - Speech started");
  }

  /**
   * (non-Javadoc)
   * 
   * @see javax.speech.recognition.RecognizerAudioAdapter#speechStopped(javax.speech.recognition.RecognizerAudioEvent)
   */
  public void speechStopped(RecognizerAudioEvent e) {
    logger.debug("speechStopped(RecognizerAudioEvent) - Speech stopped");
  }

  /**
   * (non-Javadoc)
   * 
   * @see javax.speech.recognition.RecognizerAudioAdapter#audioLevel(javax.speech.recognition.RecognizerAudioEvent)
   */
  public void audioLevel(RecognizerAudioEvent e) {

  }

}
