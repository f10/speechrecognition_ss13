/* 
 * Copyright 2013 Berlin Institute of Technology (TU Berlin), 
 * Faculty IV (CS), Department for Open Communication Systems (OKS).
 * 
 * This file is part of SpeechRecognition.
 * SpeechRecognition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SpeechRecognition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SpeechRecognition.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.tub.oks.infotainment.audio.cloudgarden;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.speech.AudioException;
import javax.speech.Central;
import javax.speech.EngineException;
import javax.speech.EngineStateError;
import javax.speech.recognition.FinalResult;
import javax.speech.recognition.GrammarException;
import javax.speech.recognition.Recognizer;
import javax.speech.recognition.RecognizerAudioAdapter;
import javax.speech.recognition.RecognizerModeDesc;
import javax.speech.recognition.ResultAdapter;
import javax.speech.recognition.ResultEvent;
import javax.speech.recognition.ResultToken;
import javax.speech.recognition.RuleGrammar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.tub.oks.infotainment.audio.RecognizedSpeechEvent;
import de.tub.oks.infotainment.audio.SpeechToText;
import de.tub.oks.infotainment.audio.SpeechToTextException;
import de.tub.oks.infotainment.audio.SpeechToTextListener;

/**
 * The Class SAPISpeechToText.
 */
public class SAPISpeechToText extends ResultAdapter implements SpeechToText {

	/** Logger for this class. */
	private static final Logger logger = LoggerFactory.getLogger(SAPISpeechToText.class);

	/** The Constant GRAMMAR_PATH. */
	private final static String GRAMMAR_PATH = "/de/tub/oks/infotainment/res/lang/grammar/";

	/** The rec. */
	private Recognizer recognizer;

	/** The recognizeAudioAdapter. */
	private RecognizerAudioAdapter recognizeAudioAdapter;

	/** The speechToTextListeners. */
	private List<SpeechToTextListener> speechToTextListeners;

	/** The locale. */
	private Locale locale;

	/** enable additional debug output. */
	private boolean debug = false;

	/** saves current pause/running state. */
	private boolean pause = false;

	/** The finished. */
	private boolean finished = false;

	/** The current grammar. */
	private String currentGrammar;

	/**
	 * Instantiates a new sAPI speech to text.
	 * 
	 * @throws SpeechToTextException
	 * 
	 */
	public SAPISpeechToText() throws SpeechToTextException {
		this(Locale.US);
	}

	/**
	 * Instantiates a new sAPI speech to text.
	 * 
	 * @param local
	 *            the local
	 * @throws SpeechToTextException
	 */
	public SAPISpeechToText(Locale local) throws SpeechToTextException {
		this.locale = local;
		speechToTextListeners = new ArrayList<SpeechToTextListener>();

		initRecognitionEngine(local);

	}

	/**
	 * Inits the recognition engine.
	 * 
	 * @param local
	 *            the local
	 * @throws SpeechToTextException
	 */
	private void initRecognitionEngine(Locale local) throws SpeechToTextException {
		finished = false;
		RecognizerModeDesc desc = new RecognizerModeDesc(local, Boolean.TRUE);

		// Create a recognizer that supports US English.
		try {
			recognizer = Central.createRecognizer(desc);
		}
		catch (EngineException e) {

		}
		catch (SecurityException e) {

		}

		if (recognizer == null) {
			logger.debug("initRecognitionEngine(Locale) - {}", "Choosen locale " + local.toString() + " is not available. OS default will be used");
			try {
				recognizer = Central.createRecognizer(null);
			}
			catch (IllegalArgumentException e) {
				throw new SpeechToTextException(e);
			}
			catch (EngineException e) {
				throw new SpeechToTextException(e);
			}
			catch (SecurityException e) {
				throw new SpeechToTextException(e);
			}
		}
		if (debug) {
			recognizer.addEngineListener(new EngineListener());
		}

		recognizer.addResultListener(this);

		recognizeAudioAdapter = new AudioListener();
		recognizer.getAudioManager().addAudioListener(recognizeAudioAdapter);

		try {
			recognizer.allocate();
		}
		catch (EngineException e) {
			throw new SpeechToTextException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.tub.oks.infotainment.audio.SpeechToText#setGrammar(java.lang.String)
	 */
	public void setGrammar(String grammar) throws SpeechToTextException {

		currentGrammar = grammar;
		// Load the grammar from a file, and enable it
		// RuleGrammar is more complex and allows to create a sentence with
		// alternative words
		InputStreamReader reader = new InputStreamReader(getClass().getResourceAsStream(GRAMMAR_PATH + grammar + "_" + locale.getLanguage() + ".gram"));

		RuleGrammar gram;
		try {
			gram = recognizer.loadJSGF(reader);
			if (!gram.isEnabled())
				gram.setEnabled(true);

			recognizer.commitChanges();
		}
		catch (GrammarException grammarException) {
			throw new SpeechToTextException(grammarException);
		}

		catch (IOException ioException) {
			// TODO Auto-generated catch block
			throw new SpeechToTextException(ioException);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.tub.oks.infotainment.audio.SpeechToText#addSpeechToTextListener(de
	 * .tub.oks.infotainment.audio.SpeechToTextListener)
	 */
	@Override
	public void addSpeechToTextListener(SpeechToTextListener listener) {
		speechToTextListeners.add(listener);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.tub.oks.infotainment.audio.SpeechToText#removeSpeechToTextListener
	 * (de.tub.oks.infotainment.audio.SpeechToTextListener)
	 */
	@Override
	public void removeSpeechToTextListener(SpeechToTextListener listener) {
		speechToTextListeners.remove(listener);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#finalize()
	 */
	@Override
	protected void finalize() throws Throwable {
		if (!finished)
			this.close();
		super.finalize();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.tub.oks.infotainment.audio.SpeechToText#close()
	 */
	@Override
	public void close() {
		finished = true;
		try {
			recognizer.suspend();
			recognizer.waitEngineState(Recognizer.SUSPENDED);

			recognizer.getAudioManager().removeAudioListener(recognizeAudioAdapter);
			recognizer.removeResultListener(this);
			recognizer.deallocate();

		}
		catch (Exception e) {
			logger.error("close()", e);
		}
	}

	/**
	 * Gets the grammar.
	 * 
	 * @return the currentGrammar
	 */
	public String getGrammar() {
		return currentGrammar;
	}

	/**
	 * Event handler which gets detected results.
	 * 
	 * @param e
	 *            the e
	 */
	public void resultAccepted(ResultEvent e) {

		if (pause) {
			return;
		}
		try {
			FinalResult r = (FinalResult) (e.getSource());

			// Recognize the words
			ResultToken tokens[] = r.getBestTokens();

			recognizer.suspend();

			// put the recognized words in "result"
			final RecognizedSpeechEvent event = new RecognizedSpeechEvent();
			int i = 0;
			while (i < tokens.length) {
				event.addWord(tokens[i].getWrittenText());
				i++;
			}
			recognizer.waitEngineState(Recognizer.SUSPENDED);

			for (final SpeechToTextListener listener : speechToTextListeners) {
				// start each handler in a new thread
				Runnable run = new Runnable() {
					@Override
					public void run() {
						listener.handleRecognizedSpeech(event);
					}
				};
				Thread handlerThread = new Thread(run);
				handlerThread.start();
			}

		}
		catch (Exception e1) {
			logger.error("resultAccepted()", e1);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.tub.oks.infotainment.audio.SpeechToText#listen()
	 */
	@Override
	public void listen() throws SpeechToTextException {
		pause = false;
		try {
			internalListining();
		}
		catch (AudioException e) {
			throw new SpeechToTextException(e);
		}
		catch (InterruptedException e) {
			throw new SpeechToTextException(e);
		}
	}

	/**
	 * Internal listining.
	 * 
	 * @throws EngineStateError
	 * @throws AudioException
	 * @throws InterruptedException
	 * @throws IllegalArgumentException
	 * 
	 */
	private void internalListining() throws AudioException, InterruptedException {

		recognizer.requestFocus();
		recognizer.resume();
		recognizer.waitEngineState(Recognizer.LISTENING);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.tub.oks.infotainment.audio.SpeechToText#pause()
	 */
	@Override
	public void pause() throws SpeechToTextException {
		pause = true;
		// this is a workaround for avoiding hang up of cloudgarde SAPI
		// recognition will run in background but
		// all results will be skipped during pause, so we could use
		// other recognition engine in between
		try {
			internalListining();
		}
		catch (AudioException e) {
			throw new SpeechToTextException(e);
		}
		catch (InterruptedException e) {
			throw new SpeechToTextException(e);
		}
	}

}