/* 
 * Copyright 2013 Berlin Institute Of Technology (TU Berlin), 
 * Faculty IV (CS), Department for Open Communication Systems.
 * 
 * This file is part of SpeechRecognition.
 * SpeechRecognition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SpeechRecognition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SpeechRecognition.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.tub.oks.infotainment.audio;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;
import javax.speech.AudioException;

/**
 * 
 * Thread subclass media player which enables playback of mp3 files, streams and
 * urls
 * 
 */
public class Mp3Player extends Thread {

	/**
	 * Logger for this class
	 */
	private static final Logger logger = LoggerFactory.getLogger(Mp3Player.class);

	/**
	 * flag to indicate the playloop that the thread should stop
	 */
	private boolean stop = false;

	/**
	 * input stream which is used for playback will be created by constructor
	 */
	private AudioInputStream in;

	/**
	 * Constructor for streams
	 * 
	 * @param stream
	 */
	public Mp3Player(InputStream stream) {
		try {
			in = AudioSystem.getAudioInputStream(stream);

		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("Mp3Player()", e);
		}
	}

	/**
	 * Constructor for url --> could play webstreams
	 * 
	 * @param url
	 */
	public Mp3Player(URL url) {
		try {
			in = AudioSystem.getAudioInputStream(url);
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("Mp3Player()", e);
		}
	}

	/**
	 * Constructor for ordinary mp3 files
	 * 
	 * @param file
	 */
	public Mp3Player(File file) {
		try {
			in = AudioSystem.getAudioInputStream(file);
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("Mp3Player()", e);
		}
	}

	@Override
	public void run() {
		try {
			if (in != null)
				play(in);
		}
		catch (Exception e) {
			logger.error("run()", e);
		}
	}

	/**
	 * starts the playback thread
	 */
	public void play() {
		this.start();
	}

	/**
	 * safe playback thread exit method
	 */
	public void stopPlay() {
		this.stop = true;
	}

	/**
	 * Play a mp3 based audio stream
	 * 
	 * @param in
	 * @throws IOException
	 * @throws LineUnavailableException
	 * @throws AudioException
	 */
	private void play(AudioInputStream in) throws IOException, LineUnavailableException, AudioException {
		AudioInputStream din = null;
		AudioFormat baseFormat = in.getFormat();
		AudioFormat decodedFormat = new AudioFormat(AudioFormat.Encoding.PCM_SIGNED, baseFormat.getSampleRate(), 16, baseFormat.getChannels(),
				baseFormat.getChannels() * 2, baseFormat.getSampleRate(), false);
		// convert data
		din = AudioSystem.getAudioInputStream(decodedFormat, in);
		// Play now.
		rawplay(decodedFormat, din);
		in.close();
	}

	/**
	 * gets data from input audio stream and writes it to output line
	 * 
	 * @param targetFormat
	 * @param din
	 * @throws IOException
	 * @throws LineUnavailableException
	 * @throws AudioException
	 */
	private void rawplay(AudioFormat targetFormat, AudioInputStream din) throws IOException, LineUnavailableException, AudioException {
		byte[] data = new byte[4096];
		SourceDataLine line = getLine(targetFormat);
		if (line != null) {
			// Start
			line.start();
			int nBytesRead = 0, nBytesWritten = 0;
			while (nBytesRead != -1 && !stop) {
				nBytesRead = din.read(data, 0, data.length);
				if (nBytesRead != -1)
					nBytesWritten = line.write(data, 0, nBytesRead);
				// if (nBytesRead != nBytesWritten) {
				// throw new AudioException(
				// "Error during writing to line interface!");
				// }
			}
			// Stop
			line.drain();
			line.stop();
			line.close();
			din.close();
		}
	}

	/**
	 * get the output line
	 * 
	 * @param audioFormat
	 * @return
	 * @throws LineUnavailableException
	 */
	private SourceDataLine getLine(AudioFormat audioFormat) throws LineUnavailableException {
		SourceDataLine res = null;
		DataLine.Info info = new DataLine.Info(SourceDataLine.class, audioFormat);
		res = (SourceDataLine) AudioSystem.getLine(info);
		res.open(audioFormat);
		return res;
	}
}
