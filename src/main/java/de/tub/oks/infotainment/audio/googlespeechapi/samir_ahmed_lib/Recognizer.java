/* 
 * Copyright 2011 Samir Ahmed.
 * 
 * Copyright 2013 Berlin Institute of Technology (TU Berlin), 
 * Faculty IV (CS), Department for Open Communication Systems (OKS).
 * 
 * This file is part of SpeechRecognition.
 * SpeechRecognition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SpeechRecognition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SpeechRecognition.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.tub.oks.infotainment.audio.googlespeechapi.samir_ahmed_lib;

import java.io.File;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.tub.oks.infotainment.audio.SpeechToTextListener;

/**
 * Class Recognizer
 * 
 * This is a threadSafe Callable class that will return a future String array
 * Currently the class will call an external process for audio recording
 * HttpPost requests
 * 
 * Time : 2 Seconds response time from google;
 * 
 * @author Rayjan Wilson - https://github.com/rayjanwilson
 * @author Samir Ahmed - https://github.com/samirahmed
 * @see http://www.samir-ahmed.com/iris.html
 * @author Florian Beckmann - florianbeckmann@gmx.net
 * */

public class Recognizer implements Callable<String> {

	/**
	 * Logger for this class
	 */
	private static final Logger logger = LoggerFactory.getLogger(Recognizer.class);

	private String command;

	private Double confidence;
	private ExecutorService exs;
	/**
	 * Recognizer's Data Fields Confidence: String: indicating Google Confidence
	 * of the result wGetResponse: String: Raw response from Google Speech API
	 * String command: String: Voice Command Confidence: Double: Represents
	 * Google Speech API Confidence in results recordShell : String: Name of
	 * shell file used for getting the
	 */

	private String googleSpeechAPIResponse;

	private Recorder recorder;
	private HttpPostWrapper SendFile;
	private List<SpeechToTextListener> speechToTextListeners;

	/** Default Constructor **/
	public Recognizer(Locale locale, ExecutorService ExServ, List<SpeechToTextListener> listener) {
		exs = ExServ;
		speechToTextListeners = listener;
		SendFile = new HttpPostWrapper(locale);
		recorder = new Recorder(exs);
		googleSpeechAPIResponse = null;
		command = null;
		confidence = 0.0;
	}

	@Override
	public String call() {
		try {
			String text = capture();

			return text;
		}
		catch (Exception ee) {
			return "";
		}
	}

	/** capture : Will record voice, send to google and parse the jSON results. **/
	public String capture() {
		// Send file to Google's Speech Servers using the Record Command

		try {
			if (recorder.isActive()) {
				if (speechToTextListeners != null) {
					for (SpeechToTextListener l : speechToTextListeners) {
						l.informRecognized();
					}
				}
				recorder.record();
				logger.debug("capture() - {}", "recorder trying to record");
			}
			else {
				return "";
			}
			if (speechToTextListeners != null) {
				for (SpeechToTextListener l : speechToTextListeners) {
					l.informProcessing();
				}
			}
			googleSpeechAPIResponse = SendFile.postFile(System.getProperty("java.io.tmpdir") + File.separator + "recording.flac");
		}
		catch (Exception EE) {
			logger.error("capture()", EE);
			return "";
		}

		// Parse Response by finding
		try {
			if (!googleSpeechAPIResponse.contains("\"utterance\":")) {
				logger.debug("capture() - Recognizer: No Command Could Be Captured");
				if (speechToTextListeners != null) {
					for (SpeechToTextListener l : speechToTextListeners) {
						l.informBadResult();
					}
				}
				return "";
			}
			else {
				// Include -> System.out.println(wGetResponse); // to view the
				// Raw output
				int startIndex = googleSpeechAPIResponse.indexOf("\"utterance\":") + 13; // Account for term "utterance":"<TARGET>","confidence"
				int stopIndex = googleSpeechAPIResponse.indexOf(",\"confidence\":") - 1; // End position
				command = googleSpeechAPIResponse.substring(startIndex, stopIndex);

				// Determine Confidence
				startIndex = stopIndex + 15;
				stopIndex = googleSpeechAPIResponse.indexOf("}]}") - 1;
				confidence = Double.parseDouble(googleSpeechAPIResponse.substring(startIndex, stopIndex));
				logger.debug("capture() - \nUtterance : " + command.toUpperCase() + "\nConfidence Level: " + (confidence * 100) + "\n");
			}
		}
		catch (NullPointerException npE) {
			// Event that no speech is captured
			return "";
		}

		return command;
	}

	@Override
	protected void finalize() throws Throwable {
		exs.shutdownNow();
		super.finalize();
	}

	public static void main(String[] args) throws InterruptedException, ExecutionException {

		// An executable method that will create a Recognizer Object and Run
		// it, It gives the time take for this process too.
		// ExecutorService exs = Executors.newFixedThreadPool(20);
		// Recognizer recognizer = new Recognizer(exs);
		// Future <String> futureresults = exs.submit(recognizer);
		// String results = futureresults.get();
		// System.out.println(results);
		// exs.shutdownNow();

		String results = "";
		// note by florian beckmann: Why the hell do you compare strings by
		// references?
		while (results != "Exit") {
			ExecutorService exs = Executors.newFixedThreadPool(20);
			Recognizer recognizer = new Recognizer(Locale.ENGLISH, exs, null);
			Future<String> futureresults = exs.submit(recognizer);
			results = futureresults.get();
			logger.debug("main(String[]) - {}", results);
			exs.shutdownNow();
		}
	}

}
