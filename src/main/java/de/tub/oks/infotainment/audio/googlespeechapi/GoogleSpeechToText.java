/* 
 * Copyright 2011 Samir Ahmed.
 * 
 * Copyright 2013 Berlin Institute of Technology (TU Berlin), 
 * Faculty IV (CS), Department for Open Communication Systems (OKS).
 * 
 * This file is part of SpeechRecognition.
 * SpeechRecognition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SpeechRecognition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SpeechRecognition.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.tub.oks.infotainment.audio.googlespeechapi;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.tub.oks.infotainment.audio.RecognizedSpeechEvent;
import de.tub.oks.infotainment.audio.SpeechToText;
import de.tub.oks.infotainment.audio.SpeechToTextListener;
import de.tub.oks.infotainment.audio.googlespeechapi.samir_ahmed_lib.Recognizer;

/**
 * Proxy between the google speech to text provider implementation from Samir
 * Ahmed and the used interface in this application.
 * 
 * @see Recognizer
 */
public class GoogleSpeechToText implements SpeechToText, Runnable {

	/**
	 * Logger for this class
	 */
	private static final Logger logger = LoggerFactory.getLogger(GoogleSpeechToText.class);

	private ExecutorService exs;
	private List<SpeechToTextListener> listeners;
	private Locale locale;
	private boolean pause = false;

	/**
	 * Instantiates a new google speech to text wrapper.
	 * 
	 * @param locale
	 *            the locale
	 */
	public GoogleSpeechToText(Locale locale) {
		this.locale = locale;
		listeners = new ArrayList<SpeechToTextListener>();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.tub.oks.infotainment.audio.SpeechToText#addSpeechToTextListener(de
	 * .tub.oks.infotainment.audio.ISpeechToTextListener)
	 */
	@Override
	public void addSpeechToTextListener(SpeechToTextListener listener) {
		listeners.add(listener);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.tub.oks.infotainment.audio.SpeechToText#close()
	 */
	@Override
	public void close() {
		pause();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.tub.oks.infotainment.audio.SpeechToText#getGrammar()
	 */
	@Override
	public String getGrammar() {
		// not supported and not necessary
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.tub.oks.infotainment.audio.SpeechToText#listen()
	 */
	@Override
	public void listen() {
		pause = false;
		Thread t = new Thread(this);
		t.setName("Thread-GSPTT-Listen");
		t.start();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.tub.oks.infotainment.audio.SpeechToText#pause()
	 */
	@Override
	public void pause() {
		if (!pause) {
			if (exs != null) {
				exs.shutdownNow();
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.tub.oks.infotainment.audio.SpeechToText#removeSpeechToTextListener
	 * (de.tub.oks.infotainment.audio.SpeechToTextListener)
	 */
	@Override
	public void removeSpeechToTextListener(SpeechToTextListener listener) {
		listeners.remove(listener);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		int counter = 0;
		while (!Thread.interrupted()) {
			logger.info("Creating new ExecutorService with thread pool size {}", 20);
			exs = Executors.newFixedThreadPool(20);
			Recognizer recognizer = new Recognizer(locale, exs, listeners);
			Future<String> futureresults = exs.submit(recognizer);
			try {
				String result = futureresults.get();
				if (result.isEmpty()) { // we receive empty result on timeout
					Thread.sleep(1000);
					if ((counter % 10) == 0) {
						logger.info("Continue listening");
					}
					continue;
				}
				handleEvents(result);
				break;
			}

			catch (Exception e) {
				logger.error("run()", e);
			}
			logger.info("run()- shutting down ExecutorService");
			exs.shutdownNow();
		}
		logger.info("GoogleSpeechToText Thread stopped");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.tub.oks.infotainment.audio.SpeechToText#setGrammar(java.lang.String)
	 */
	@Override
	public void setGrammar(String grammar) /* throws Exception */{
		// not supported! Do nothing
	}

	/**
	 * Call all event listeners with recognized command.
	 * 
	 * @param command
	 *            the command
	 */
	private void handleEvents(String command) {
		if (!pause) {
			StringTokenizer tokenizer = new StringTokenizer(command);

			RecognizedSpeechEvent event = new RecognizedSpeechEvent();

			while (tokenizer.hasMoreTokens()) {
				event.addWord(tokenizer.nextToken());

			}

			for (SpeechToTextListener listener : listeners) {
				listener.handleRecognizedSpeech(event);
			}
		}
	}

}
