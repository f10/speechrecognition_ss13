/* 
 * Copyright 2013 Berlin Institute of Technology (TU Berlin), 
 * Faculty IV (CS), Department for Open Communication Systems (OKS).
 * 
 * This file is part of SpeechRecognition.
 * SpeechRecognition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SpeechRecognition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SpeechRecognition.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.tub.oks.infotainment.audio.cloudgarden;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ConcurrentLinkedQueue;

import javax.speech.Central;
import javax.speech.Engine;
import javax.speech.EngineException;
import javax.speech.EngineStateError;
import javax.speech.synthesis.JSMLException;
import javax.speech.synthesis.Synthesizer;
import javax.speech.synthesis.SynthesizerModeDesc;
import javax.speech.synthesis.SynthesizerProperties;
import javax.speech.synthesis.Voice;

import de.tub.oks.infotainment.audio.TextToSpeech;

/**
 * The Class SAPITextToSpeech: Generate Voice Output from Text, implemented as
 * Singleton
 */
public class SAPITextToSpeech implements TextToSpeech {

	/** Logger for this class. */
	private static final Logger logger = LoggerFactory.getLogger(SAPITextToSpeech.class);

	private Synthesizer synth = null;

	private String voice;

	private float volume;

	private float speed;

	private boolean debug;

	// store current speech threads
	ConcurrentLinkedQueue<Thread> speechThreads = new ConcurrentLinkedQueue<Thread>();

	/**
	 * Instantiates a new sAPI text to speech.
	 * 
	 * @param voice
	 *            the voice
	 * @param volume
	 *            the volume
	 * @param speed
	 *            the speed
	 */
	public SAPITextToSpeech(String voice, float volume, float speed) {
		this(voice, volume, speed, Locale.ENGLISH, false);
	}

	/**
	 * Instantiates a new sAPI text to speech.
	 * 
	 * @param voice
	 *            the voice
	 * @param volume
	 *            the volume
	 * @param speed
	 *            the speed
	 * @param locale
	 *            the locale
	 * @param debug
	 *            the debug
	 */
	public SAPITextToSpeech(String voice, float volume, float speed, Locale locale, boolean debug) {
		this.voice = voice;
		this.volume = volume;
		this.speed = speed;
		this.debug = debug;

		synth = createSynthesizer(locale);
		refreshSettings();
	}

	/**
	 * Creates the synthesizer.
	 * 
	 * @param locale
	 *            the locale
	 * @return the synthesizer
	 */
	private Synthesizer createSynthesizer(Locale locale) {

		try {
			SynthesizerModeDesc desc = new SynthesizerModeDesc(locale);

			synth = Central.createSynthesizer(desc);
			if (synth == null) {
				logger.debug("createSynthesizer(Locale) - {}", "Selected language not available using default language!");
				synth = Central.createSynthesizer(null);
			}

			desc = (SynthesizerModeDesc) synth.getEngineModeDesc();

			/* Listet Ereignisse in der Konsole auf */
			if (debug)
				synth.addEngineListener(new EngineListener());

			/* Nun wird Speicher bereitgestellt */
			synth.allocate();

			/* Weitermachen nicht stoppen */
			synth.resume();

			/* Status des Engines auf ALLOCATED setzen */
			synth.waitEngineState(Synthesizer.ALLOCATED);

		}
		catch (Exception e1) {
			logger.debug("createSynthesizer(Locale) - {}", e1);
		}
		return synth;
	}

	/**
	 * Refresh settings.
	 */
	protected void refreshSettings() {

		SynthesizerProperties props = synth.getSynthesizerProperties();

		/*
		 * voice = "Microsoft Sam" "Microsoft Mike" "Microsoft Mary"
		 */
		/* Erzeugung der Stimme mit den gegebenen Eigenschaften */
		Voice theVoice = new Voice(voice, Voice.GENDER_DONT_CARE, Voice.GENDER_DONT_CARE, null);

		try {
			props.setVoice(theVoice);
			props.setVolume(volume);
			props.setSpeakingRate(speed);
		}
		catch (Exception prop) {
			logger.warn("refreshSettings() - exception ignored", prop);

		}

	}

	/**
	 * Kill synthesizer.
	 */
	protected void killSynthesizer() {
		// Schlie�e den Synthezizer
		try {
			synth.deallocate();
			synth.waitEngineState(Engine.DEALLOCATED);
		}
		catch (IllegalArgumentException e) {
			logger.error("killSynthesizer()", e);
		}
		catch (InterruptedException e) {
			logger.error("killSynthesizer()", e);
		}
		catch (EngineException e) {
			logger.error("killSynthesizer()", e);
		}
		catch (EngineStateError e) {
			logger.error("killSynthesizer()", e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.tub.oks.infotainment.audio.TextToSpeech#getVoices()
	 */
	public List<String> getVoices() {
		SynthesizerModeDesc desc;
		desc = (SynthesizerModeDesc) synth.getEngineModeDesc();

		List<String> voices = new ArrayList<String>();

		for (Voice voice : desc.getVoices()) {
			voices.add(voice.getName());
		}
		return voices;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#finalize()
	 */
	@Override
	protected void finalize() throws Throwable {
		killSynthesizer();
	};

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.tub.oks.infotainment.audio.TextToSpeech#getVoice()
	 */
	@Override
	public String getVoice() {
		return voice;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.tub.oks.infotainment.audio.TextToSpeech#setVoice(java.lang.String)
	 */
	@Override
	public void setVoice(String voice) {
		this.voice = voice;
		refreshSettings();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.tub.oks.infotainment.audio.TextToSpeech#getVolume()
	 */
	@Override
	public float getVolume() {
		return volume;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.tub.oks.infotainment.audio.TextToSpeech#setVolume(float)
	 */
	@Override
	public void setVolume(float volume) {
		this.volume = volume;
		refreshSettings();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.tub.oks.infotainment.audio.TextToSpeech#getSpeed()
	 */
	@Override
	public float getSpeed() {
		return speed;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.tub.oks.infotainment.audio.TextToSpeech#setSpeed(float)
	 */
	@Override
	public void setSpeed(float speed) {
		this.speed = speed;
		refreshSettings();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.tub.oks.infotainment.audio.TextToSpeech#speak(java.lang.String)
	 */
	@Override
	public void speak(final String text) {
		Runnable run = new Runnable() {

			@Override
			public void run() {
				// Speche mit dem erzeugten synthie
				try {
					synth.speak(text, null);

				}
				catch (JSMLException e) {
					logger.error("$Runnable.run()", e);
				}
				catch (EngineStateError e) {
					logger.error("$Runnable.run()", e);
				}
				catch (IllegalArgumentException e) {
					logger.error("$Runnable.run()", e);
				}

			}

		};
		Thread t = new Thread(run);
		speechThreads.add(t);
		t.start();
		try {
			cleanSpeechThreads();
		}
		catch (InterruptedException e) {
			logger.error("speak()", e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.tub.oks.infotainment.audio.TextToSpeech#stop()
	 */
	@Override
	public void stop() {
		synth.cancelAll();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.tub.oks.infotainment.audio.TextToSpeech#waitForSpeech()
	 */
	@Override
	public void waitForSpeech() {
		for (Thread t : speechThreads) {
			try {
				t.join();
			}
			catch (InterruptedException e) {
				logger.error("waitForSpeech()", e);
			}
		}

	};

	/**
	 * Clean speech threads.
	 * 
	 * @throws InterruptedException
	 *             the interrupted exception
	 */
	private void cleanSpeechThreads() throws InterruptedException {
		Thread t;
		while ((t = speechThreads.peek()) != null) {
			if (!t.isAlive()) {
				speechThreads.poll();
			}
		}
	}

}
