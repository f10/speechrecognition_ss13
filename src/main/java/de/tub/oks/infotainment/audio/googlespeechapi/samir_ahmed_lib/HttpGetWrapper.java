/* 
 * Copyright 2011 Samir Ahmed.
 * 
 * Copyright 2013 Berlin Institute of Technology (TU Berlin), 
 * Faculty IV (CS), Department for Open Communication Systems (OKS).
 * 
 * This file is part of SpeechRecognition.
 * SpeechRecognition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SpeechRecognition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SpeechRecognition.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.tub.oks.infotainment.audio.googlespeechapi.samir_ahmed_lib;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

import javazoom.jl.decoder.JavaLayerException;

/***
 * A Simple Class with only the method download, for getting http responses with
 * JAVA.net class
 * 
 * Throws IOException in the event that the connection is not established
 * Warning: Do not rely on the IOException to determine whether or not the
 * content is correct
 * 
 * Download is a thread-safe static method
 * 
 * **/

public class HttpGetWrapper {

	/**
	 * Executes an http GET request to the given url, with the specified User
	 * Agent String Returns response as a string
	 **/

	public static final String USER_AGENT = "Mozilla/5.0";//"Mozilla/5.0 (Windows NT 6.1; WOW64; rv:7.0.1) Gecko/20100101 Firefox/7.0.1";

	public static String download(String url2get, Object lock, String UserAgent) throws IOException, InterruptedException {

		// Safe approach to setting http.agent system property
		synchronized (lock) {
			System.setProperty("http.agent", "");
		}
		// Set up Strings
		String buffer;
		String response = "";

		// Set up URL connection
		java.net.URLConnection urlConnect = new URL(url2get).openConnection();
		urlConnect.setRequestProperty("User-Agent", UserAgent);

		// URL connection inputstream is read into response
		BufferedReader br = new BufferedReader(new InputStreamReader(urlConnect.getInputStream()));
		while ((buffer = br.readLine()) != null) {
			response += buffer;
			if (Thread.interrupted()) {
				br.close();
				throw new InterruptedException("Thread Task Cancelled");
			}
		}
		br.close();
		return response;
	}

	/**
	 * Quick Way to call the download method, setting default UserAgent to
	 * Mozilla\5.0
	 **/

	public static String download(String url2get, Object lock) throws IOException, InterruptedException {
		return HttpGetWrapper.download(url2get, lock, USER_AGENT);
	}

	public static byte[] downloadToByteArray(String url2get, Object lock) throws IOException, InterruptedException, JavaLayerException {
		return HttpGetWrapper.downloadToByteArray(url2get, lock, USER_AGENT);
	}

	public static byte[] downloadToByteArray(String url2get, Object lock, String UserAgent) throws IOException, InterruptedException {

		// Safe approach to setting http.agent system property
		synchronized (lock) {
			System.setProperty("http.agent", "");
		}

		// Set up URL connection
		int readLength;
		byte[] buffer = new byte[1024];
		java.net.URLConnection urlConnect = new URL(url2get).openConnection();
		urlConnect.setRequestProperty("User-Agent", UserAgent);

		// URL connection inputstream is read into byte array
		ByteArrayOutputStream byteStream = new ByteArrayOutputStream();

		// Open connection and stream to inputStream
		InputStream iStream = new BufferedInputStream(urlConnect.getInputStream());

		// Read the GET Response into the ByteArray Stream
		while (-1 != (readLength = iStream.read(buffer))) {
			byteStream.write(buffer, 0, readLength);
			if (Thread.interrupted()) {
				byteStream.close();
				iStream.close();
				throw new InterruptedException("Thread Task Cancelled");
			}
		}

		// Close the Streams
		byteStream.close();
		iStream.close();

		// Return byte array with MP3 files
		return byteStream.toByteArray();
	}

	public static InputStream downloadInputStream(String url2get, Object lock) throws IOException, InterruptedException, JavaLayerException {
		return HttpGetWrapper.downloadInputStream(url2get, lock, USER_AGENT);
	}

	public static InputStream downloadInputStream(String url2get, Object lock, String UserAgent) throws IOException {

		// Safe approach to setting http.agent system property
		synchronized (lock) {
			System.setProperty("http.agent", "");
		}

		// Set up URL connection
		java.net.URLConnection urlConnect = new URL(url2get).openConnection();
		urlConnect.setRequestProperty("User-Agent", UserAgent);

		// Open connection and stream to inputStream
		InputStream iStream = new BufferedInputStream(urlConnect.getInputStream());
		return iStream;
	}
}