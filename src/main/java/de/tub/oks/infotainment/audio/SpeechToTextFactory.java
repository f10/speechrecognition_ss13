/* 
 * Copyright 2013 Berlin Institute of Technology (TU Berlin), 
 * Faculty IV (CS), Department for Open Communication Systems (OKS).
 * 
 * This file is part of SpeechRecognition.
 * SpeechRecognition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SpeechRecognition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SpeechRecognition.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.tub.oks.infotainment.audio;

import java.util.Locale;

import de.tub.oks.infotainment.audio.cloudgarden.SAPISpeechToText;
import de.tub.oks.infotainment.audio.googlespeechapi.GoogleSpeechToText;
import de.tub.oks.infotainment.audio.sphinx.SphinxSpeechToText;

/**
 * Factory for speech to text instantiation encapsulation. In general the
 * behavior of online/offline methods should be maintained.
 */
public class SpeechToTextFactory {

	/**
	 * define available engines.
	 */
	public enum Engine {
		GOOGLE, CLOUDGARDEN, SPHINX
	}

	/**
	 * give online speech to text engine.
	 * 
	 * @param locale
	 *            the locale
	 * @param type
	 *            the type
	 * @return the speech recognition engine
	 * @throws SpeechToTextException
	 * @throws Exception
	 *             the exception
	 */
	public static SpeechToText getSpeechRecognitionEngine(Locale locale, Engine type) throws SpeechToTextException {
		SpeechToText engine = null;
		switch (type) {
		case GOOGLE:
			engine = new GoogleSpeechToText(locale);
			break;
		case CLOUDGARDEN:
			engine = new SAPISpeechToText(locale);
			break;
		case SPHINX:
			engine = new SphinxSpeechToText(locale);
			break;

		default:
			break;
		}
		return engine;
	}
}
