/* 
 * Copyright 2013 Berlin Institute of Technology (TU Berlin), 
 * Faculty IV (CS), Department for Open Communication Systems (OKS).
 * 
 * This file is part of SpeechRecognition.
 * SpeechRecognition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SpeechRecognition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SpeechRecognition.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.tub.oks.infotainment.audio;

import java.util.Locale;

import de.tub.oks.infotainment.audio.cloudgarden.SAPITextToSpeech;
import de.tub.oks.infotainment.audio.googlespeechapi.samir_ahmed_lib.GoogleTextToSpeech;

/**
 * Factory for text to speech instianciation encapsulation in general a offline
 * engine is used but this is changeable.
 */
public class TextToSpeechFactory {

	/**
	 * define available engines.
	 */
	public enum Engine {
		GOOGLE, CLOUDGARDEN
	}

	/**
	 * Gets the text to speech engine.
	 * 
	 * @param voice
	 *            the voice
	 * @param volume
	 *            the volume
	 * @param speed
	 *            the speed
	 * @param locale
	 *            the locale
	 * @param type
	 *            the type
	 * @return the text to speech engine
	 */
	public static TextToSpeech getTextToSpeechEngine(String voice, float volume, float speed, Locale locale, Engine type) {

		TextToSpeech engine = null;
		switch (type) {
		case GOOGLE:
			engine = new GoogleTextToSpeech(locale);
			break;
		case CLOUDGARDEN:
			engine = new SAPITextToSpeech(voice, volume, speed, locale, false);
			break;

		default:
			break;
		}
		return engine;

	}
}
