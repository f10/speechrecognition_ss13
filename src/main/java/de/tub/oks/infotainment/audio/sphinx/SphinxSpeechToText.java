/* 
 * Copyright 2013 Berlin Institute of Technology (TU Berlin), 
 * Faculty IV (CS), Department for Open Communication Systems (OKS).
 * 
 * This file is part of SpeechRecognition.
 * SpeechRecognition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SpeechRecognition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SpeechRecognition.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.tub.oks.infotainment.audio.sphinx;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//import sun.org.mozilla.javascript.annotations.JSFunction;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import de.tub.oks.infotainment.audio.NotRecordingException;
import de.tub.oks.infotainment.audio.SpeechToText;
import de.tub.oks.infotainment.audio.SpeechToTextException;
import de.tub.oks.infotainment.audio.SpeechToTextListener;
import de.tub.oks.infotainment.audio.RecognizedSpeechEvent;
//import de.tub.oks.infotainment.audio.SoundAudit;

import edu.cmu.sphinx.frontend.util.Microphone;
import edu.cmu.sphinx.jsgf.JSGFGrammar;
import edu.cmu.sphinx.jsgf.JSGFGrammarException;
import edu.cmu.sphinx.jsgf.JSGFGrammarParseException;
import edu.cmu.sphinx.recognizer.Recognizer;
import edu.cmu.sphinx.result.Result;
import edu.cmu.sphinx.util.props.ConfigurationManager;

/**
 * Implementation of SpeechToText based on Sphinx engine implementation should
 * be ok in general but it is necessary to spend some time on configuration for
 * proper results TODO this engine has a very extensive configuration
 * documentation and a big bunch of parameters --> needs further testing it is
 * also know that current microphone format results into problem on combination
 * with remote google engine implementation
 */
public class SphinxSpeechToText implements SpeechToText {

	/**
	 * Logger for this class
	 */
	private static final Logger logger = LoggerFactory.getLogger(SphinxSpeechToText.class);

	private Recognizer recognizer;
	private JSGFGrammar jsgfGrammar;
	private Microphone microphone;

	private List<SpeechToTextListener> listeners;

	private Locale locale;

	private String currentGrammar;

	private boolean finished = false;

	/**
	 * Instantiates a new sphinx speech to text.
	 * 
	 * @param locale
	 *            the locale
	 * @throws Exception
	 *             the exception
	 */
	public SphinxSpeechToText(Locale locale) {

		this.locale = locale;

		listeners = new ArrayList<SpeechToTextListener>();

		URL url = this.getClass().getResource("/de/tub/oks/infotainment/res/settings/sphinx.config.xml");
		ConfigurationManager cm = new ConfigurationManager(url);

		// Retrieve the recognizer, jsgfGrammar and the microphone from
		// the configuration file.

		recognizer = (Recognizer) cm.lookup("recognizer");
		jsgfGrammar = (JSGFGrammar) cm.lookup("jsgfGrammar");
		microphone = (Microphone) cm.lookup("microphone");
		microphone.initialize();

		logger.debug("SphinxSpeechToText(Locale) - {}", "sphinx allocating recognizer");
		recognizer.allocate();

	}

	@Override
	protected void finalize() throws Throwable {
		close();
		super.finalize();
	}

	@Override
	public void addSpeechToTextListener(SpeechToTextListener listener) {
		listeners.add(listener);
	}

	@Override
	public void removeSpeechToTextListener(SpeechToTextListener listener) {
		listeners.remove(listener);
	}

	private RecognitionThread recognitionThread;

	@Override
	public void listen() throws SpeechToTextException {

		// avoid running two recognition threads at the same time
		if (recognitionThread != null) {
			try {
				recognitionThread.join();
			}
			catch (InterruptedException e) {
				throw new SpeechToTextException(e);
			}
		}
		logger.info("Starting new Recognition Thread.");
		recognitionThread = new RecognitionThread();
		recognitionThread.setName("Thread-Sphinx-Recognition");
		recognitionThread.start();
	}

	@Override
	public void setGrammar(String grammar) throws SpeechToTextException {
		currentGrammar = grammar;
		// the path is specified in sphinx configuration, file extension .gram
		// is automatically added
		logger.debug("setGrammar(String) - Grammar resource to be loaded [{}]", grammar + "_" + locale.getLanguage());
		try {
			jsgfGrammar.loadJSGF(grammar + "_" + locale.getLanguage());
			logger.info("Rule Grammar loaded [{}]", jsgfGrammar.getRuleGrammar().toString());
		}
		catch (IOException e) {
			throw new SpeechToTextException(e);
		}
		catch (JSGFGrammarParseException e) {
			throw new SpeechToTextException(e);
		}
		catch (JSGFGrammarException e) {
			throw new SpeechToTextException(e);
		}

	}

	@Override
	public String getGrammar() {
		return currentGrammar;
	}

	@Override
	public void close() {
		if (!finished) {
			try {
				recognitionThread.join(2000);
			}
			catch (InterruptedException e) {
				logger.warn("close() - exception ignored", e); //$NON-NLS-1$

			}
			recognitionThread.setStopped(true); // it is ok because we give the
												// thread
			// time for closing
			recognizer.deallocate();

			finished = true;
		}
	}

	@Override
	public void pause() {
		microphone.stopRecording();
	}

	public class RecognitionThread extends Thread {
		/**
		 * Logger for this class
		 */
		private final Logger logger = LoggerFactory.getLogger(RecognitionThread.class);

		private boolean stopped;

		public void run() {
			logger.debug("run() - start"); //$NON-NLS-1$

			logger.info("run() - Recognition Thread started. Sphinx is listening.");
			// SoundAudit.printSoundAudit();

			if (!(recognizer.getState() == Recognizer.State.READY)) {
				logger.debug("run() - {}", "sphinx allocating recognizer in recognitionthread");
				recognizer.allocate();
			}

			// when online search (i.e. via google) was performed the microphone needs to be re-initialized,
			// without, even though startRecording() can be invoked without exceptions, nothing seems to be recorded
			// at least that's the case for linux.
			microphone.initialize();
			microphone.startRecording();

			if (!microphone.isRecording()) {
			  microphone.initialize();
			  
				if (!microphone.startRecording()) {
					throw new NotRecordingException("Can't start the microphone!");
				}
			}

			int eventCounter=0;
			while (!isStopped()) { // loop as long we get no proper result
				Result result = recognizer.recognize();

				if (result != null) {

					String bestResult = result.getBestFinalResultNoFiller();

					if (bestResult != null && !bestResult.isEmpty()) {
						logger.debug("run() - {}", "sphinx recognized:" + bestResult);

						final RecognizedSpeechEvent recognizedSpeechEvent = new RecognizedSpeechEvent();
						recognizedSpeechEvent.addWord(bestResult);

						logger.info("Informing {} SpeechToTextListeners about newly added word [{}] in word list [" + recognizedSpeechEvent.toString() + "]",
								listeners.size(), bestResult);
						
						int handlerCounter=0;
						for (SpeechToTextListener speechToTextListener : listeners) {

							Thread handlerThread = new RecognizedSpeechHandler(speechToTextListener, recognizedSpeechEvent);
							
							handlerThread.setName("Thread-Sphinx-SpeechToTextEventHandler-"+eventCounter+"-"+handlerCounter);
							handlerThread.start();
							handlerCounter++;
						}
						eventCounter++;
						break;
					}
				}
			}
			logger.info("RecognitionThread stopped.");
			//microphone.stopRecording();
			
			logger.debug("run() - end"); //$NON-NLS-1$
		}

		public boolean isStopped() {
			return stopped;
		}

		public void setStopped(boolean stop) {
			this.stopped = stop;
		}

	}

	public class RecognizedSpeechHandler extends Thread {

		private SpeechToTextListener speechToTextListener;
		private RecognizedSpeechEvent event;

		public RecognizedSpeechHandler(SpeechToTextListener speechToTextListener, RecognizedSpeechEvent event) {
			this.speechToTextListener = speechToTextListener;
			this.event = event;
		}

		public void run() {
			logger.debug("run() - informing listener"); //$NON-NLS-1$

			speechToTextListener.handleRecognizedSpeech(event);

			logger.debug("run() - Done."); //$NON-NLS-1$
		}

	}
}
