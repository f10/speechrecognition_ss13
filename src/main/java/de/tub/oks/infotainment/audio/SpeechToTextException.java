/* 
 * Copyright 2013 Berlin Institute of Technology (TU Berlin), 
 * Faculty IV (CS), Department for Open Communication Systems (OKS).
 * 
 * This file is part of SpeechRecognition.
 * SpeechRecognition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SpeechRecognition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SpeechRecognition.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.tub.oks.infotainment.audio;

/**
 * @author Florian Beckmann - florianbeckmann@gmx.net
 * 
 */
public class SpeechToTextException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2342799168087097702L;

	public SpeechToTextException(String message) {
		super(message);
	}

	public SpeechToTextException(Throwable cause) {
		super(cause);
	}

	public SpeechToTextException(String message, Throwable cause) {
		super(message, cause);
	}

}
