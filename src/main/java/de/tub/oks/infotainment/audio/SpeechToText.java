/* 
 * Copyright 2013 Berlin Institute of Technology (TU Berlin), 
 * Faculty IV (CS), Department for Open Communication Systems (OKS).
 * 
 * This file is part of SpeechRecognition.
 * SpeechRecognition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SpeechRecognition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SpeechRecognition.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.tub.oks.infotainment.audio;

import java.io.IOException;

import javax.speech.AudioException;
import javax.speech.EngineStateError;
import javax.speech.recognition.GrammarException;

/**
 * General interface for speech to text engines.
 * 
 */
public interface SpeechToText {

	/**
	 * add a object which listens to recognized speech sequences.
	 * 
	 * @param listener
	 *            the listener
	 */
	public void addSpeechToTextListener(SpeechToTextListener listener);

	/**
	 * remove a former added recognized speech sequences listener.
	 * 
	 * @param listener
	 *            the listener
	 */
	public void removeSpeechToTextListener(SpeechToTextListener listener);

	/**
	 * starts listening process.
	 * 
	 * @throws SpeechToTextException
	 */
	public void listen() throws SpeechToTextException;

	/**
	 * change used grammar of speech recognizer.
	 * 
	 * @param grammar
	 *            the new grammar
	 * @throws SpeechToTextException
	 */
	public void setGrammar(String grammar) throws SpeechToTextException;

	/**
	 * Get current grammarfile.
	 * 
	 * @return the grammar
	 */
	public String getGrammar();

	/**
	 * stops speech listening and recognition.
	 * 
	 * @throws SpeechToTextException
	 */
	public void pause() throws SpeechToTextException;

	/**
	 * close pending resources.
	 */
	public void close();

}
