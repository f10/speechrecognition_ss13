/* 
 * Copyright 2013 Berlin Institute of Technology (TU Berlin), 
 * Faculty IV (CS), Department for Open Communication Systems (OKS).
 * 
 * This file is part of SpeechRecognition.
 * SpeechRecognition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SpeechRecognition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SpeechRecognition.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.tub.oks.infotainment.audio;

import java.util.ArrayList;
import java.util.List;

/**
 * This Event object will be returned in speech recognition callback routines
 * 
 */
public class RecognizedSpeechEvent {
	private List<String> words;

	/**
	 * get recognized words
	 * 
	 * @return words
	 */
	public List<String> getWords() {
		return words;
	}

	/**
	 * default constructor
	 */
	public RecognizedSpeechEvent() {
		words = new ArrayList<String>();
	}

	/**
	 * add a recognized word to list
	 * 
	 * @param word
	 */
	public void addWord(String word) {
		words.add(word);
	}

	/**
	 * get number of recognized words in this event
	 * 
	 * @return
	 */
	public int getWordCount() {
		return words.size();
	}

	/**
	 * converts list of recognized words to one concatenated string
	 */
	@Override
	public String toString() {

		StringBuffer all = new StringBuffer();

		for (String word : words) {
			all.append(word);
			all.append(" ");
		}
		if (all.length() > 0)
			all.delete(all.length() - 1, all.length());

		return all.toString();
	}

}
