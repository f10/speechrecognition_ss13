/* 
 * Copyright 2013 Berlin Institute of Technology (TU Berlin), 
 * Faculty IV (CS), Department for Open Communication Systems (OKS).
 * 
 * This file is part of SpeechRecognition.
 * SpeechRecognition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SpeechRecognition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SpeechRecognition.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.tub.oks.infotainment.audio;

/**
 * This is a callback interface which must be implemented by classes which want
 * to be called after speech recognition is occurred
 * 
 */
public interface SpeechToTextListener {
	/**
	 * function is called if speech sequence was detected
	 * 
	 * @param e
	 */
	public void handleRecognizedSpeech(RecognizedSpeechEvent e);

	/**
	 * this method is called when the recognition engine is busy with processing
	 * its also possible that this method will not be called by engine if it has
	 * short processing time
	 */
	public void informProcessing();

	/**
	 * inform that the engine detected recognizeable sound
	 */
	public void informRecognized();

	/**
	 * inform about bad recognition result
	 */
	public void informBadResult();
}
