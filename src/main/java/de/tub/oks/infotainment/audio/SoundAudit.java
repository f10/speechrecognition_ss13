/* 
 * Copyright 2013 Berlin Institute of Technology (TU Berlin), 
 * Faculty IV (CS), Department for Open Communication Systems (OKS).
 * 
 * This file is part of SpeechRecognition.
 * SpeechRecognition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SpeechRecognition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SpeechRecognition.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.tub.oks.infotainment.audio;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sound.sampled.*;

/**
 * The Class SoundAudit.
 */
public class SoundAudit {

	/**
	 * Logger for this class
	 */
	private static final Logger logger = LoggerFactory.getLogger(SoundAudit.class);

	/**
	 * Prints the sound audit.
	 */
	public static void printSoundAudit() {
		logger.debug("start");

		try {
			logger.debug(
					"printSoundAudit() - {}",
					"OS: " + System.getProperty("os.name") + " " + System.getProperty("os.version") + "/" + System.getProperty("os.arch") + "\nJava: "
							+ System.getProperty("java.version") + " (" + System.getProperty("java.vendor") + ")\n");
			for (Mixer.Info thisMixerInfo : AudioSystem.getMixerInfo()) {
				logger.debug("printSoundAudit() - {}", "Mixer: " + thisMixerInfo.getDescription() + " [" + thisMixerInfo.getName() + "]");
				Mixer thisMixer = AudioSystem.getMixer(thisMixerInfo);
				for (Line.Info thisLineInfo : thisMixer.getSourceLineInfo()) {
					if (thisLineInfo.getLineClass().getName().equals("javax.sound.sampled.Port")) {
						Line thisLine = thisMixer.getLine(thisLineInfo);
						thisLine.open();
						logger.debug("printSoundAudit() - {}", "  Source Port: " + thisLineInfo.toString());
						for (Control thisControl : thisLine.getControls()) {
							logger.debug("printSoundAudit() - {}", AnalyzeControl(thisControl));
						}
						thisLine.close();
					}
				}
				for (Line.Info thisLineInfo : thisMixer.getTargetLineInfo()) {
					if (thisLineInfo.getLineClass().getName().equals("javax.sound.sampled.Port")) {
						Line thisLine = thisMixer.getLine(thisLineInfo);
						thisLine.open();
						logger.debug("printSoundAudit() - {}", "  Target Port: " + thisLineInfo.toString());
						for (Control thisControl : thisLine.getControls()) {
							logger.debug("printSoundAudit() - {}", AnalyzeControl(thisControl));
						}
						thisLine.close();
					}
				}
			}
		}
		catch (Exception e) {
			logger.error("printSoundAudit()", e);

			logger.error("printSoundAudit()", e);
		}

		logger.debug("end");
	}

	/**
	 * Analyze control.
	 * 
	 * @param thisControl
	 *            the this control
	 * @return the string
	 */
	public static String AnalyzeControl(Control thisControl) {
		logger.debug("start");

		String type = thisControl.getType().toString();
		if (thisControl instanceof BooleanControl) {
			String returnString = "    Control: " + type + " (boolean)";
			logger.debug("end");
			return returnString;
		}
		if (thisControl instanceof CompoundControl) {
			logger.debug("AnalyzeControl(Control) - {}", "    Control: " + type + " (compound - values below)");
			String toReturn = "";
			for (Control children : ((CompoundControl) thisControl).getMemberControls()) {
				toReturn += "  " + AnalyzeControl(children) + "\n";
			}
			String returnString = toReturn.substring(0, toReturn.length() - 1);
			logger.debug("end");
			return returnString;
		}
		if (thisControl instanceof EnumControl) {
			String returnString = "    Control:" + type + " (enum: " + thisControl.toString() + ")";
			logger.debug("end");
			return returnString;
		}
		if (thisControl instanceof FloatControl) {
			String returnString = "    Control: " + type + " (float: from " + ((FloatControl) thisControl).getMinimum() + " to "
					+ ((FloatControl) thisControl).getMaximum() + ")";
			logger.debug("end");
			return returnString;
		}

		logger.debug("end");
		return "    Control: unknown type";
	}
}