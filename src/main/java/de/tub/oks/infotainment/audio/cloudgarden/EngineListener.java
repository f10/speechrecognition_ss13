/* 
 * Copyright 2013 Berlin Institute of Technology (TU Berlin), 
 * Faculty IV (CS), Department for Open Communication Systems (OKS).
 * 
 * This file is part of SpeechRecognition.
 * SpeechRecognition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SpeechRecognition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SpeechRecognition.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.tub.oks.infotainment.audio.cloudgarden;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.speech.EngineErrorEvent;
import javax.speech.EngineEvent;
import javax.speech.recognition.RecognizerEvent;
import javax.speech.recognition.RecognizerListener;
import javax.speech.synthesis.SynthesizerEvent;
import javax.speech.synthesis.SynthesizerListener;

/**
 * Prints engine events (Recognizer and Synthesizer) to System.out
 * 
 * @see EngineEvent
 */

public class EngineListener implements SynthesizerListener, RecognizerListener {

	/**
	 * Logger for this class
	 */
	private static final Logger logger = LoggerFactory.getLogger(EngineListener.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * javax.speech.EngineListener#engineAllocated(javax.speech.EngineEvent)
	 */
	public void engineAllocated(EngineEvent e) {

		logger.debug("engineAllocated(EngineEvent) - {}", e.getSource() + " engineAllocated");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.speech.EngineListener#engineAllocatingResources(javax.speech.
	 * EngineEvent)
	 */
	public void engineAllocatingResources(EngineEvent e) {

		logger.debug("engineAllocatingResources(EngineEvent) - {}", e.getSource() + " engineAllocatingResources");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * javax.speech.EngineListener#engineDeallocated(javax.speech.EngineEvent)
	 */
	public void engineDeallocated(EngineEvent e) {

		logger.debug("engineDeallocated(EngineEvent) - {}", e.getSource() + " engineDeallocated");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * javax.speech.EngineListener#engineDeallocatingResources(javax.speech.
	 * EngineEvent)
	 */
	public void engineDeallocatingResources(EngineEvent e) {

		logger.debug("engineDeallocatingResources(EngineEvent) - {}", e.getSource() + " engineDeallocatingResources");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * javax.speech.EngineListener#engineError(javax.speech.EngineErrorEvent)
	 */
	public void engineError(EngineErrorEvent e) {

		logger.debug("engineError(EngineErrorEvent) - {}", e.getSource() + " engineError");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.speech.EngineListener#enginePaused(javax.speech.EngineEvent)
	 */
	public void enginePaused(EngineEvent e) {

		logger.debug("enginePaused(EngineEvent) - {}", e.getSource() + " enginePaused");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.speech.EngineListener#engineResumed(javax.speech.EngineEvent)
	 */
	public void engineResumed(EngineEvent e) {

		logger.debug("engineResumed(EngineEvent) - {}", e.getSource() + " engineResumed...");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * javax.speech.recognition.RecognizerListener#recognizerProcessing(javax
	 * .speech.recognition.RecognizerEvent)
	 */
	public void recognizerProcessing(RecognizerEvent e) {

		logger.debug("recognizerProcessing(RecognizerEvent) - {}", e.getSource() + " recognizerProcessing");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * javax.speech.recognition.RecognizerListener#recognizerListening(javax
	 * .speech.recognition.RecognizerEvent)
	 */
	public void recognizerListening(RecognizerEvent e) {

		logger.debug("recognizerListening(RecognizerEvent) - {}", e.getSource() + " recognizerListening");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * javax.speech.recognition.RecognizerListener#recognizerSuspended(javax
	 * .speech.recognition.RecognizerEvent)
	 */
	public void recognizerSuspended(RecognizerEvent e) {

		logger.debug("recognizerSuspended(RecognizerEvent) - {}", e.getSource() + " recognizerSuspended");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * javax.speech.recognition.RecognizerListener#changesCommitted(javax.speech
	 * .recognition.RecognizerEvent)
	 */
	public void changesCommitted(RecognizerEvent e) {

		logger.debug("changesCommitted(RecognizerEvent) - {}", e.getSource() + " changesCommitted");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * javax.speech.recognition.RecognizerListener#focusGained(javax.speech.
	 * recognition.RecognizerEvent)
	 */
	public void focusGained(RecognizerEvent e) {

		logger.debug("focusGained(RecognizerEvent) - {}", e.getSource() + " focusGained");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.speech.recognition.RecognizerListener#focusLost(javax.speech.
	 * recognition.RecognizerEvent)
	 */
	public void focusLost(RecognizerEvent e) {

		logger.debug("focusLost(RecognizerEvent) - {}", e.getSource() + " focusLost");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * javax.speech.synthesis.SynthesizerListener#queueEmptied(javax.speech.
	 * synthesis.SynthesizerEvent)
	 */
	public void queueEmptied(SynthesizerEvent e) {

		logger.debug("queueEmptied(SynthesizerEvent) - {}", e.getSource() + " queueEmptied");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * javax.speech.synthesis.SynthesizerListener#queueUpdated(javax.speech.
	 * synthesis.SynthesizerEvent)
	 */
	public void queueUpdated(SynthesizerEvent e) {

		logger.debug("queueUpdated(SynthesizerEvent) - {}", e.getSource() + " queueUpdated");
	}
}
