/* 
 * Copyright 2011 Samir Ahmed.
 * 
 * Copyright 2013 Berlin Institute of Technology (TU Berlin), 
 * Faculty IV (CS), Department for Open Communication Systems (OKS).
 * 
 * This file is part of SpeechRecognition.
 * SpeechRecognition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SpeechRecognition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SpeechRecognition.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.tub.oks.infotainment.audio.googlespeechapi.samir_ahmed_lib;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * SYNTHESIZER MODULE -- 
 * LAST EDITED BY SAMIR AHMED - JUNE 20 2011
 * 
 * OUTLINE: Synthesizer Used for Generating Speech Via Google Translate Text-to-Speech API
 * This Class uses a CachedThreadPool To Increase Increase Efficiency 
 * Speed Test: Single Thread 4.3 seconds vs Multi-Threaded 1.2 Seconds
 * 
 **/

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.Normalizer;
import java.text.Normalizer.Form;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.tub.oks.infotainment.audio.TextToSpeech;

import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.AudioDevice;
import javazoom.jl.player.FactoryRegistry;
import javazoom.jl.player.Player;

public class GoogleTextToSpeech implements Runnable, TextToSpeech {

	/**
	 * Logger for this class
	 */
	private static final Logger logger = LoggerFactory.getLogger(GoogleTextToSpeech.class);

	// Testing Synthesizer Directly
	public static void main(String[] args) throws InterruptedException, ExecutionException {
		ExecutorService exs = Executors.newFixedThreadPool(10);
		GoogleTextToSpeech outVoice = new GoogleTextToSpeech(Locale.GERMAN, exs);
		outVoice.setAnswer("Han� M�ller �lpipeline");
		Future<?> playFuture = exs.submit(outVoice);
		playFuture.get();

	}

	/*
	 * Synthesizer Data Fields
	 * 
	 * UserText : LinkedList: String Container with Tokenized Stringfs Files :
	 * Integer : The number of output files that will be generated isUnassigned
	 * : Boolean : True/False holds status of the AudioDevice object
	 * 
	 * AudioLine and FutureAudioLine are AudioDevice objects for the JLayer
	 * Player
	 */

	private LinkedList<String> UserText;
	private Integer Files;

	private AudioDevice AudioLine;

	private final ExecutorService exs;
	private final String GOOGLE_TRANSLATE_TTS_URL = "http://translate.google.com/translate_tts?ie=UTF-8&tl=";
	private final String GOOGLE_TRANSLATE_TTS_URL_2 = "&q=";
	FactoryRegistry factoryregistry = FactoryRegistry.systemRegistry();

	ConcurrentLinkedQueue<Player> currentSpeech = new ConcurrentLinkedQueue<Player>();

	Locale locale;

	/** Constructor: Will submit the AudioDeviceLoader to the executor */

	/** Constructor for Synthesizer Object **/
	public GoogleTextToSpeech(Locale locale, ExecutorService executor) {
		this.locale = locale;
		//Assign executor
		this.exs = executor;
	}

	public GoogleTextToSpeech(Locale locale) {
		this(locale, Executors.newFixedThreadPool(10));
	}

	/** Assigns the audioline if unassigned */

	private void AssignAudioLine() {
		try {
			AudioLine = factoryregistry.createAudioDevice();
		}
		catch (JavaLayerException e) {
			logger.error("AssignAudioLine() - Could not load audio driver", e);
			logger.error("AssignAudioLine()", e);
		}
	}

	/** Public method to set desire text for Text to Speech Synthesis */

	public void setAnswer(String rawText) {

		// Assign the audiodevice from the future it is stored in
		AssignAudioLine();

		//keep everthing like it is and replace only whitespaces
		rawText.replaceAll(" ", "%20");

		//above conversation works better as original implementation below
		//also better as rawText=URLEncoder.encode(rawText,"UTF-8");

		//Convert to good ole' plain english ascii characters
		//		rawText = Normalizer.normalize(rawText, Form.NFD)
		////		.replaceAll("\\p{InCombiningDiacriticalMarks}+", "");
		//		rawText= EncodingUtil.encodeURIComponent(rawText);

		//		// Ensure we have only whitespaces that are single spaces characters
		//		rawText = rawText.replaceAll("%"," percent ");
		//		rawText = rawText.replaceAll("\\$"," Dollars ");
		//		rawText = rawText.replaceAll("&"," and ");
		//		rawText = rawText.replaceAll("\\+"," plus ");
		//
		//		rawText = rawText.replaceAll("\\s"," ");
		//		rawText = rawText.replaceAll("\\s{2,}"," ");
		//		rawText = rawText.replaceAll("\"|\'|\\(|\\)|\\]|\\[","");
		//
		//
		//		Pattern allcapsPattern = Pattern.compile("([A-Z]){2,}[^A-Z]");
		//		Matcher allcapsMatch = allcapsPattern.matcher(rawText);
		//		while(allcapsMatch.find()){
		//			allcapsMatch.group().toCharArray();
		//			String replacement="";
		//			for (char cc: allcapsMatch.group().toCharArray())
		//			{
		//				replacement=replacement+cc+"-";
		//			}
		//			rawText = rawText.replace(allcapsMatch.group(),replacement);
		//		}

		// Tokenize userText and Retreive number of Files; 
		UserText = smartTokenize(rawText);
		Files = UserText.size();
	}

	/**
	 * SmartTokenize : Function for parsing by grammar and restructuring output:
	 * Average Run Time ~ 12 ms
	 **/
	private LinkedList<String> smartTokenize(String oneString) {
		// Check if we end on a period. If not Add one
		if (!oneString.endsWith(".")) {
			oneString = oneString + ".";
		}

		// Create a regex pattern parse by punctuation;
		Pattern tokenizerPattern = Pattern.compile("([^\\.,;:!]*[\\.,;:!])+?");
		Matcher tokenizerMatcher = tokenizerPattern.matcher(oneString);

		// Declare String variables.
		String buffer;
		String sentence = "";
		int count = 0;
		int length = 0;

		// Create Linked list for putting in strings
		LinkedList<String> answerList = new LinkedList<String>();

		// For every punctuation separated value
		while (tokenizerMatcher.find()) {

			// Instantiate Buffer count and length;
			buffer = tokenizerMatcher.group();
			count = sentence.length();
			length = buffer.length();

			// CASE 1: We can easily add buffer to our sentence because we are under the 100 char limit
			if ((count + length) <= 100) {
				sentence += buffer;
			}

			// CASE 2: If buffer length is greater than 100...
			else if (length > 100) {

				// Add tack on however much of buffer is required to get a 100 character string
				int breakpoint = buffer.lastIndexOf(' ', 100 - sentence.length());
				answerList.addLast(sentence + buffer.subSequence(0, breakpoint));
				length -= breakpoint;

				// If we still have more than 100 characters we cut off the largest chunks until we are under one hundred
				if (length > 100) {
					while (length > 100) {
						int newbreakpoint = buffer.lastIndexOf(' ', breakpoint + 100);
						answerList.addLast(buffer.substring(breakpoint, newbreakpoint));
						length -= (newbreakpoint - breakpoint);
						breakpoint = newbreakpoint;
					}
				}

				// In either case, we just tack on the remainder, assuming it is not zero
				if (length > 0) {
					answerList.addLast(buffer.substring(breakpoint, buffer.length()));
					sentence = "";
				}
			}

			// CASE 3: Buffer is less than 100 but added to sentence is greater
			// We add sentence too list, we remake sentence as buffer
			else {
				answerList.addLast(sentence);
				sentence = buffer;
			}
		}
		// Last case in the event we missed something
		if (sentence != "") {
			answerList.addLast(sentence);
		}

		return answerList;
	}

	/** getSpeech **/

	/**
	 * Speak will create multiple SpeechFileGenerator Objects, which return mp3
	 * chunks the method will concatenate all the files and then play them with
	 * the JLayer Mp3 Player Object.
	 */
	private synchronized void Speak(LinkedList<String> UserText, Integer Files) {

		// Use the executor service to submit all the TTS strings simultaneously
		try {
			// For every file in UserText LinkedList; Generate a textToSpeech Object and execute it
			ArrayList<Future<byte[]>> mp3_filelist = new ArrayList<Future<byte[]>>(Files);
			for (int ii = 1; ii <= Files; ii++) {
				SpeechFileGenerator tts = new SpeechFileGenerator(UserText.pop());
				mp3_filelist.add(exs.submit(tts));
			}

			/* Create a new mp3 file byte[] of size zero */
			byte[] mp3_file = new byte[0];
			int size = mp3_file.length;

			/*
			 * For every future in the arrayList, Append it to mp3_file
			 * variable;
			 */
			for (Future<byte[]> mp3_piece_Future : mp3_filelist) {

				/* Load the future into mp3_piece */
				byte[] mp3_piece = mp3_piece_Future.get();

				/* Calculate piece length */
				int pieceLength = mp3_piece.length;

				/*
				 * Create a new temporary byte array with size large enough to
				 * accommodate the new piece
				 */
				byte[] temp = new byte[size + pieceLength];

				/* Copy the original file into the byte array */
				System.arraycopy(mp3_file, 0, temp, 0, size);

				/*
				 * Copy the new piece into the byte array, offset by the old
				 * files length
				 */
				System.arraycopy(mp3_piece, 0, temp, size, pieceLength);

				/* Update the size of the whole file and reassign variables */
				mp3_file = temp;
				size += pieceLength;

			}

			/* Create Input Stream from mp3_file and submit to JLayer MP3 Player */
			InputStream mp3_stream = new ByteArrayInputStream(mp3_file);
			Player mp3_player = new Player(mp3_stream, this.AudioLine);
			currentSpeech.add(mp3_player);
			mp3_player.play();

		}
		catch (Exception e) {
			logger.error("Speak()", e);
		}
	}

	/** Run Interface **/
	public void run() {
		try {
			Speak(UserText, Files);
		}
		catch (Exception ee) {
			logger.error("run()", ee);
		}
	}

	/** Threadable textToSpeech Synthesis Objects **/
	private class SpeechFileGenerator implements Callable<byte[]> {

		/*
		 * textToSpeech Data Fields text : String : String to pass to the
		 * ttsShell mp3_file : byte[] : Byte Array with mp3 file contained in it
		 */

		private String text;
		private byte[] mp3_chunk;

		/**
		 * Constructor - Takes String Under 100 Characters and Encodes to URL
		 * safe Format
		 **/
		public SpeechFileGenerator(String text) {
			try {
				this.text = URLEncoder.encode(text, "UTF-8");
			}
			catch (UnsupportedEncodingException UEex) {
				// If the result is unencodable by the means above, 
				// we replace the spaces with plus and hope for the best
				this.text = text.replaceAll(" ", "+");
			}
		}

		/**
		 * "call" : Callable method, will return future byte [] with the mp3
		 * file containing the give text
		 **/
		public byte[] call() {
			try {
				// 
				mp3_chunk = HttpGetWrapper.downloadToByteArray(GOOGLE_TRANSLATE_TTS_URL + locale.getLanguage() + GOOGLE_TRANSLATE_TTS_URL_2 + text, this);
			}
			catch (Exception ee) {
				logger.error("call()", ee);
				mp3_chunk = new byte[0];
			}

			return mp3_chunk;
		}
	}

	@Override
	public synchronized void speak(final String text) {
		Runnable run = new Runnable() {

			@Override
			public void run() {
				internalSpeak(text);
				updateSpeechQueue();

			}
		};
		Thread t = new Thread(run);
		t.start();
	}

	@Override
	public void stop() {
		//stops all available players
		Player p;
		while (null != (p = currentSpeech.poll())) {
			p.close();
		}
	};

	private void updateSpeechQueue() {
		Player p;
		while (null != (p = currentSpeech.peek())) {
			if (p.isComplete()) {
				currentSpeech.remove(p);
			}
		}
	}

	private synchronized void internalSpeak(final String text) {
		setAnswer(text);
		Speak(UserText, Files);
	}

	@Override
	public String getVoice() {
		//not implemented
		return "No voice selection available";
	}

	@Override
	public void setVoice(String voice) {
		//not implemented		
	}

	@Override
	public float getVolume() {
		//not implemented	
		return 1;
	}

	@Override
	public void setVolume(float volume) {
		//not implemented			
	}

	@Override
	public float getSpeed() {
		//not implemented	
		return 1;
	}

	@Override
	public void setSpeed(float speed) {
		//not implemented			
	}

	@Override
	public List<String> getVoices() {
		//not implemented	
		return new ArrayList<String>();
	}

	@Override
	protected void finalize() throws Throwable {
		exs.shutdownNow();
		super.finalize();
	}

	@Override
	public void waitForSpeech() {
		Player p = currentSpeech.poll();
		while (null != p) {
			while (!p.isComplete()) {
				try {
					Thread.sleep(200);
				}
				catch (InterruptedException e) {
					logger.error("waitForSpeech()", e);
				}
			}
			p = currentSpeech.poll();
		}
	}
}
