/* 
 * Copyright 2013 Berlin Institute of Technology (TU Berlin), 
 * Faculty IV (CS), Department for Open Communication Systems (OKS).
 * 
 * This file is part of SpeechRecognition.
 * SpeechRecognition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SpeechRecognition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SpeechRecognition.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.tub.oks.infotainment.audio;

import java.util.List;

/**
 * general interface for text to speech engines.
 */
public interface TextToSpeech {

	/**
	 * generate speech output.
	 * 
	 * @param text
	 *            the text
	 */
	public void speak(String text);

	/**
	 * Interrupt current speech playback -> stop output.
	 */
	public void stop();

	/**
	 * get current used voice name.
	 * 
	 * @return the voice
	 */
	public String getVoice();

	/**
	 * change current used voice name if available.
	 * 
	 * @param voice
	 *            voice name
	 */
	public void setVoice(String voice);

	/**
	 * get voice volume.
	 * 
	 * @return volume
	 */
	public float getVolume();

	/**
	 * set voice volume if available.
	 * 
	 * @param volume
	 *            the new volume
	 */
	public void setVolume(float volume);

	/**
	 * get voice speed.
	 * 
	 * @return speed
	 */
	public float getSpeed();

	/**
	 * set voice speed if available.
	 * 
	 * @param speed
	 *            the new speed
	 */
	public void setSpeed(float speed);

	/**
	 * get list of available voice names.
	 * 
	 * @return the voices
	 */
	public List<String> getVoices();

	/**
	 * wait until current text is spoken.
	 */
	public void waitForSpeech();
}
