/* 
 * Copyright 2013 Berlin Institute of Technology (TU Berlin), 
 * Faculty IV (CS), Department for Open Communication Systems (OKS).
 * 
 * This file is part of SpeechRecognition.
 * SpeechRecognition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SpeechRecognition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SpeechRecognition.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.tub.oks.infotainment.remoteservice.spotify;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * Test class for Spotify service class SpotifyService.
 */
public class SpotifyServiceTest {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LoggerFactory.getLogger(SpotifyServiceTest.class);

	private static SpotifyService spotifyService;

	@Before
	public void setUp() {
		logger.debug("setUp() - start"); //$NON-NLS-1$

		spotifyService = new SpotifyService();
		spotifyService.initialize();

		logger.debug("setUp() - end"); //$NON-NLS-1$
	}

	@Test
	public void testSearch() {

		try {
			Thread.sleep(3000);
		}
		catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertTrue(spotifyService.loggedIn());
		spotifyService.search("salsa");

		spotifyService.addTracksToPlaylist(spotifyService.loadTracksFound());
		assertTrue(spotifyService.getPlayListQueue().size() > 0);
//		spotifyService.play();
//		while (true) {
//
//			try {
//				Thread.sleep(60000);
//				//				logger.info("60s passed. Skipping Track");
//				//				spotifyService.skipTrack();
//			}
//			catch (InterruptedException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		}

	}
}
