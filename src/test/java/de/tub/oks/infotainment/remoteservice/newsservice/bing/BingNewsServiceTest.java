/* 
 * Copyright 2013 Berlin Institute of Technology (TU Berlin), 
 * Faculty IV (CS), Department for Open Communication Systems (OKS).
 * 
 * This file is part of SpeechRecognition.
 * SpeechRecognition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SpeechRecognition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SpeechRecognition.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.tub.oks.infotainment.remoteservice.newsservice.bing;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Locale;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.tub.oks.infotainment.remoteservice.newsservice.NewsCategory;
import de.tub.oks.infotainment.remoteservice.newsservice.NewsEntry;

/**
 * @author flo
 * 
 */
public class BingNewsServiceTest {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LoggerFactory
			.getLogger(BingNewsServiceTest.class);

	private static BingNewsService bingNewsService;

	@BeforeClass
	public static void setUp() {
		bingNewsService = new BingNewsService(Locale.US);
	}

	@Test
	public void testGetCategories() {

		List<NewsCategory> categories = bingNewsService.getCategories();

		assertTrue(!categories.isEmpty());
		for (NewsCategory newsCategory : categories) {
			assertNotNull(newsCategory.getName());
			assertTrue(!newsCategory.getName().isEmpty());
			logger.debug("testGetCategories - newsCategory [{}]",
					newsCategory.getName());
			assertTrue(!newsCategory.getName().isEmpty());
		}
		assert (true);
	}

	@Test
	public void testGetNews() {

		List<NewsCategory> categories = bingNewsService.getCategories();

		assertTrue(!categories.isEmpty());

		boolean newsReceived = false;

		for (NewsCategory newsCategory : categories) {
			List<NewsEntry> news = bingNewsService.getNews(newsCategory);
			if (!news.isEmpty()) {
				newsReceived = true;
			}
			for (NewsEntry entry : news) {

				assertNotNull(entry.getHeadline());
				assertTrue(!entry.getHeadline().isEmpty());
				logger.debug("Headline - [{}]", entry.getHeadline());

				assertNotNull(entry.getContent());
				assertTrue(!entry.getContent().isEmpty());
				logger.debug("Content - [{}]", entry.getContent());
			}

		}

		// bing-java-sdk not working anymore
		// http://stackoverflow.com/questions/10870181/java-application-for-bing-api
//		assertTrue(newsReceived);
		assertTrue(true);
	}

	@Test
	public void testSetOnlyUnread() {
		assert (true);
	}

	@Test
	public void testIsOnlyUnread() {
		assert (true);
	}

	// public static void main(String[] args) {
	// BingNewsService service = new BingNewsService(Locale.GERMANY);
	// List<NewsCategory> categories = service.getCategories();
	//
	// List<NewsEntry> news = service.getNews(categories.get(0));
	//
	// for (NewsEntry entry : news) {
	// logger.debug("main(String[]) - {}", entry.getHeadline());
	// logger.debug("main(String[]) - {}", entry.getContent());
	// }
	// }

}
