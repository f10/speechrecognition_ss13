<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
	<head>
		<title>Web 2.0 | Speech Recognition</title>
		
		<link rel="stylesheet" type="text/css" href="style.css">
		<link rel="shortcut icon" type="image/x-icon" href="images/favicon.jpg">
	
		<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false" ></script>
		<script type="text/javascript" src="jslib/prototype.js" ></script>
		<script type="text/javascript" src="jslib/slider.js" ></script>
		<script type="text/javascript" src="jslib/livepipe.js" ></script>
		<script type="text/javascript" src="scrollbar.js" ></script>
		<script type="text/javascript" src="function.js" ></script>
		
	</head>
	
<body>
		<!-- Background Image -->
		<div class="background"></div>
		
		<!-- Command Online -->
		<div class="begrenzer">
		
			<!--  Some parameters for internal use (always hidden) -->
			<input id="starthidden" name="starthidden" type="hidden" value="start" >
		
			<!-- Server Status -->
			<div class="status" id="status">Turned Off</div>
			
			<!-- Header Menu -->
			<div class="menu" id="menu">&nbsp;</div>
			
			<!-- Body between the menus -->
			<div class="placeToShow">
			
				<!-- Start Button (Visible only on start, hidden when program is running) -->
				<div class="startstop" id="startstop">Start speech listener:	
					<input class="button" id="startbutton" type="button" value="start" onclick="init();">				
				</div>
				
				<!-- Result list for huge result content with custom scrollbar-->
				<div class="resultbig"  id="scrollbar_container">  
				    <div id="scrollbar_track"><div id="scrollbar_handle"></div></div>  
				    <div id="scrollbar_content">&nbsp;</div>  
				</div> 
				<!-- Result list for smaller result content-->
				<div class="result"  id="result">  
				&nbsp; 
				</div> 
				
				<!-- Menu Images -->
				<div class="canvas" id="canvas">&nbsp;</div>
				<div class="map" id="map">&nbsp;</div>
			</div>
			
			<!-- Footer Menu -->
			<div class="menu" id="footermenu">&nbsp;</div>
			<br><!-- Just for debugging -->
			<a href="http://speechrecognition.oks.de:8080/SpeechRecognition_SS13/">Home</a>		    
		</div>
	</body>
</html>