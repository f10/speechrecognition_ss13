var checkLat;

var directionDisplay;
var directionsService = new google.maps.DirectionsService();

var initurl = "http://speechrecognition.oks.de:8080/SpeechRecognition_SS13/servlett?start=start"; 
var updateurl="http://speechrecognition.oks.de:8080/SpeechRecognition_SS13/updater"; 

var youarehere = "images/google/youarehere.png";
var image1 = "images/google/number1.png";
var image2 = "images/google/number2.png";
var image3 = "images/google/number3.png";
var image4 = "images/google/number4.png";
var yelpmarker = "images/menu/marker_star.png";

var googleDivTagID = "map";

var scrollbar;

function showYelpResults(mapResults, marker) {
	var biz = mapResults[0];
	var latFrom = biz.location.coordinate.latitude;
	var lngFrom = biz.location.coordinate.longitude;
	var pointCenter = new google.maps.LatLng(latFrom, lngFrom);

	var myOptions = {
		zoom : 13,
		center : pointCenter,
		mapTypeId : google.maps.MapTypeId.ROADMAP
	};
	var map = new google.maps.Map(document.getElementById(googleDivTagID),
			myOptions);

	var marker1 = new google.maps.Marker({
		position : pointCenter,
		map : map,
		icon : yelpmarker
	});
	
	

}






function intializeWithMorePoints(mapResults, latFrom, lngFrom, googleDivTagID,
		youarehere, image1, image2, image3, image4, resultsToShow) {
	var latArr = new Array();
	var lngArr = new Array();

	var arr = mapResults.split("#");
	var l = arr.length;
	var i;
	for (i = 0; i < l - 1; i++) {
		var test1 = arr[i].split(":");
		latArr[i] = test1[0];
		lngArr[i] = test1[1];
	}

	// Postion You are Here
	var latlng = new google.maps.LatLng(latFrom, lngFrom);

	// Position 1
	var l0 = latArr[0];
	var l1 = lngArr[0];
	var latlng1 = new google.maps.LatLng(l0, l1);

	// Position 2
	var l2 = latArr[1];
	var l3 = lngArr[1];
	var latlng2 = new google.maps.LatLng(l2, l3);

	// Position 3
	var l4 = latArr[2];
	var l5 = lngArr[2];
	var latlng3 = new google.maps.LatLng(l4, l5);

	// Position 4
	var l6 = latArr[3];
	var l7 = lngArr[3];
	var latlng4 = new google.maps.LatLng(l6, l7);

	var myOptions = {
		zoom : 13,
		center : latlng,
		mapTypeId : google.maps.MapTypeId.ROADMAP
	};

	var bounds = new google.maps.LatLngBounds();

	bounds.extend(latlng);

	if (l0 != null) {
		bounds.extend(latlng1);
	}
	if (l2 != null) {
		bounds.extend(latlng2);
	}
	if (l4 != null) {
		bounds.extend(latlng3);
	}
	if (l6 != null) {
		bounds.extend(latlng4);
	}

	var map = new google.maps.Map(document.getElementById(googleDivTagID),
			myOptions);
	map.fitBounds(bounds);

	var marker = new google.maps.Marker({
		position : latlng,
		map : map,
		icon : youarehere
	});

	if (resultsToShow >= 1) {
		var marker1 = new google.maps.Marker({
			position : latlng1,
			map : map,
			icon : image1
		});
	}
	if (resultsToShow >= 2) {
		var marker2 = new google.maps.Marker({
			position : latlng2,
			map : map,
			icon : image2
		});
	}
	if (resultsToShow >= 3) {
		var marker3 = new google.maps.Marker({
			position : latlng3,
			map : map,
			icon : image3
		});
	}
	if (resultsToShow == 4) {
		var marker4 = new google.maps.Marker({
			position : latlng4,
			map : map,
			icon : image4
		});
	}

}

function showDirection(latFrom, lngFrom, latTo, lngTo, googleDivTagID) {
	// var directionDisplay;
	// var directionsService = new google.maps.DirectionsService();
	directionsDisplay = new google.maps.DirectionsRenderer();

	var latlng = new google.maps.LatLng(latFrom, lngFrom);

	var myOptions = {
		zoom : 13,
		center : latlng,
		mapTypeId : google.maps.MapTypeId.ROADMAP
	};

	var map1 = new google.maps.Map(document.getElementById(googleDivTagID),
			myOptions);
	directionsDisplay.setMap(map1);

	var request = {
		origin : latFrom + ", " + lngFrom,
		destination : latTo + ", " + lngTo,
		travelMode : google.maps.DirectionsTravelMode.DRIVING
	};

	directionsService.route(request, function(result, status) {
		if (status == google.maps.DirectionsStatus.OK) {
			directionsDisplay.setDirections(result);
		}
	});
}

function initialize(latFrom, lngFrom, googleDivTagID, youarehere) {
	var pointCenter = new google.maps.LatLng(latFrom, lngFrom);
	var myOptions = {
		zoom : 13,
		center : pointCenter,
		mapTypeId : google.maps.MapTypeId.ROADMAP
	};

	var map = new google.maps.Map(document.getElementById(googleDivTagID),
			myOptions);

	var marker = new google.maps.Marker({
		position : pointCenter,
		map : map,
		icon : youarehere,
		title : "WEB 2.0 im Auto!"
	});
}

function init(){
		//DEBUG
		//var initurl = "http://speechrecognition.oks.de:8080/SpeechRecognition_SS13/servlett?start="; 

		var url = initurl; // + "" + escape(starthidden.value);

		var request = new XMLHttpRequest();

		request.open("GET", url, true);
		request.setRequestHeader("Content-Type", "application/x-javascript;");
		request.onreadystatechange = function() {
			if (request.readyState == 4) {
				if (request.status == 200) {
					initRefresher();
				}
			}

		};
		request.send(null);
}

function initRefresher(){
	var startstop = document.getElementById("startstop");
	startstop.innerHTML = "";
	setInterval(refresh, 1000);
}

function refresh() {
		//DEBUG
		//var updateurl="http://speechrecognition.oks.de:8080/SpeechRecognition_SS13/updater"; 
		var url = updateurl;

		var request = new XMLHttpRequest();

		request.open("GET", url, true);
		request.setRequestHeader("Content-Type", "application/x-javascript;");
		request.onreadystatechange = function() {
			if (request.readyState == 4) {
				if (request.status == 200) {

					if (request.responseText) {

						var menu = document.getElementById("menu");
						var footermenu = document.getElementById("footermenu");						
						var server_status = document.getElementById("status");
						bigresults = document.getElementById("scrollbar_content");
						bigresultsContainer= document.getElementById("scrollbar_container");
						smallresults = document.getElementById("result");

						var jsonData = request.responseText;
						var myJSONObject = eval('(' + jsonData + ')');

						var menuFromJson = myJSONObject.menu;
						var footermenuFromJson = myJSONObject.footermenu;
						var resultFromJson = myJSONObject.result;
						var server_statusFromJson = myJSONObject.server_status;
						var canvasFromJson = myJSONObject.canvas;
						var changeCanvasFromJson = myJSONObject.changeCanvas;
						var mapResultsFromJson = myJSONObject.mapResults;
						var mode = myJSONObject.mode;
						var increasedRsult = myJSONObject.increasedResult;

						
						// You are here
						var latFrom = myJSONObject.latFrom;
						var lngFrom = myJSONObject.lngFrom;

						// Chosen Result
						var latTo = myJSONObject.latTo;
						var lngTo = myJSONObject.lngTo;
						
						var resultsToShow = myJSONObject.resultsToShow;
						
						var results;
						
						if(increasedRsult == "true"){
							bigresultsContainer.style.visibility="visible";
							results = bigresults;
							//set opposit hidden
							smallresults.style.visibility="hidden";
						}else{
							smallresults.style.visibility="visible";
							results = smallresults;
							//set opposit hidden
							bigresultsContainer.style.visibility="hidden";
						}

					    if(menu.innerHTML != menuFromJson){
					    	menu.innerHTML = menuFromJson;
					    }
					    if(footermenu.innerHTML != footermenuFromJson){
					    	footermenu.innerHTML = footermenuFromJson;
					    }
					    if(results.innerHTML != resultFromJson){
					    	results.innerHTML = resultFromJson;
						    scrollbar.recalculateLayout();  
						    event.stop(); 
					    }
					    
					    if(server_status.innerHTML != server_statusFromJson){
					    	server_status.innerHTML = server_statusFromJson;
					    }

						//handle menu state canvas element
						if (changeCanvasFromJson == "true") {
							if ((canvasFromJson == "1")
									|| (canvasFromJson == "2")
									|| (canvasFromJson == "3")
									|| (canvasFromJson == "4")) {
								// Build and show Google-Maps
								document.getElementById(googleDivTagID).style.visibility = 'visible';
								if (canvasFromJson == "1")
									initialize(latFrom, lngFrom,
											googleDivTagID, youarehere);
								if (canvasFromJson == "2")
									intializeWithMorePoints(mapResultsFromJson,
											latFrom, lngFrom, googleDivTagID,
											youarehere, image1, image2, image3,
											image4, resultsToShow);
								if (canvasFromJson == "3")
									showDirection(latFrom, lngFrom, latTo,
											lngTo, googleDivTagID);
								if (canvasFromJson == "4") {
									var data = eval('(' + mapResultsFromJson + ')');
									//results.innerHTML=data[0].location.coordination.latitude;
									showYelpResults(data);
								}
							} else { // Show only icons, hide Google-Maps
								document.getElementById(googleDivTagID).style.visibility = 'hidden';
								if(canvas.innerHTML != canvasFromJson){
									canvas.innerHTML = canvasFromJson;
								}
							}
						}
					}
				}
			}

		};
		request.send(null);
}

function checkForRestartingUpdater(){
	var qs = (function(a) {
	    if (a == "") return {};
	    var b = {};
	    for (var i = 0; i < a.length; ++i)
	    {
	        var p=a[i].split('=');
	        if (p.length != 2) continue;
	        b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));
	    }
	    return b;
	})(window.location.search.substr(1).split('&'));
	if(qs["updater"]=="restart"){
		initRefresher();
	}
}


function initSite(){
	//Scrollbar for de.tub.oks.infotainment.main result window
	scrollbar = new Control.ScrollBar('scrollbar_content','scrollbar_track'); 
	checkForRestartingUpdater();
}

window.onload=initSite;
